\chapter{The Many Dimensions of Change}

\textsf{Don Edward Beck}\\

\epigraph{The population undergoing drastic change is a population of misfits, and misfits live and breathe in an atmosphere of passion and imbalance, explosive and hungry for action.}{Eric Hoffer in The Ordeal of Change}

This quotation by Eric Hoffer, the insightful American philosopher, seems to have been written for our times. It introduces some reality into our understanding of change itself. We have just been through a presidential campaign where the word has been bashed about by politicians, spin-doctors, and partisans on the street. We have been challenged to ‘be the change,’ as if one could simply turn on a switch or meditate to discover the deeper truths. Let us  all be world-centric, you suggest. Well, maybe. 

Change, for sure, is the topic of the day. That’s easy to say. But change ‘from what and to what?’ I ask. Add \emph{how} and by \emph{whom}, and then perhaps we can begin to talk some sense, even in these days of nonstop news cycles and naïve but well-meaning mantras clamouring for ‘change.’ 

Look at the power within the shift in educational purpose from ‘No child left behind’ to ‘Each child to full potential.’ The first rescues stragglers; the other pushes to greatness. 

Our task is three-fold. We need a Lexicon \emph{on Change} to talk and walk the same language. Second, it would help if we had an understanding of the Components for Change, in order to know when to do what, as acceptance windows open and close. Last, we will explore the \emph{Global Change Game} to gain a better perspective on how to deal with 7.8 billion humans along the change landscape. Harm De Blij’s new book, \emph{The Power of Place}, makes it quite clear that Tom Friedman’s ‘flat earth’ is an oversimplification of the human realities on the ground. Our geographic and memetic landscapes are rough and full of traps and sinkholes.

\section*{Lexicon on Change}

Alfred Korzybski is known in many circles as the father of general semantics. In a major work, Science and Sanity, the Polish Count attributes much of our social insanity to the lack of precision within the structure and meaning of our words. He suggested that we index each word to differentiate it from others which are spelled and pronounced the same. He calls for operational definitions to enhance sanity by at least knowing what we are talking about. 

So, here is ‘change’ in eight different dimensions or variations. Perhaps you can add others. The first five function within the current set of givens, called \emph{Change of the First Order}. The second three operate within a new set of givens, thus \emph{Change of the Second Order}. 
 
We often call it deep change, profound change, or even radical change to signify this category. New wine demands new wineskins. 

\subsection*{First Order Change}

\mypar{Change Variation One (CV\textsubscript{1}):} Fine-tune or trim the tabs. Make minor adjustments to simply tweak the system. Nothing else is needed. 
\mypar{Change Variation Two (CV\textsubscript 2):} Reform or reshuffle the deck. The essential elements within the givens are re-aligned, or reformed while the way of thinking remains constant. 
\mypar{Change Variation Three (CV\textsubscript3):} Upgrade or improve on the gi\-vens. The new or updated version replaces the previous one, but within the present operating code. 
\mypar{Change Variation Four (CV\textsubscript4):} Down-stretch and regress. Adjust to the current situation by returning to an earlier system, hunker down for the moment. Go back to basics. 
\mypar{Change Variation Five (CV\textsubscript5):} Up-stretch to expand. Think outside the box, but return later. Push the envelope. 

\subsection*{Second Order Change}

\mypar{Change Variation Six (CV\textsubscript6):} Break-out. Attack the barriers. Wea\-ken, remove or replace the status quo. Here is revolutionary rather than evolutionary change. 
\mypar{Change Variation Seven (CV\textsubscript7):} Up-shift or morph to the next. Subsume the old into the new. Transform to the next level of complexity. Transcend but include, like the ‘doll within the doll’ structures in the nested Russian toys. Here is systemic change. 
\mypar{Change Variation Eight (CV\textsubscript8):} Quantum change of epochal proportions. Here are shifts from Premodern, to Modern, to Postmodern, and to Integral as all the transitions need to be legitimised and facilitated. These shifts concern major sea changes, massive upheavals, millennial-like turns. Everything is on the table and up for grabs as multiple change dynamics occur across the entire landscape. 

All human groupings appear to be on the move, but towards different futures. The deep tectonic plates of human existence are shifting beneath the surface. All Change Variations are active and often in conflict. Plus ça change, \emph{plus c’est la même chose}. The more things change, this expression claims, the more they remain the same. I firmly believe that the more things change, the more they \emph{don’t} stay the same. 

The whole world is turning over, and in real time, while a new world is emerging. Such change is apparent in the transformation into new Ages: Hunter-gatherer, agricultural, industrial, informational, molecular, cyber-, and whatever will be next.  

Yet, different people are moving through the various Ages simultaneously. There are different futures for different folks ratcheting through different levels of complexity, all at the same time. 

Whenever you use the word change, or hear somebody else do so, pause for a moment and inquire as to which of the Change Variations is involved in both sender and receiver. Otherwise, their conversations are about the chicken and the egg, or comparing apples and oranges, or about even contradictory visions, strategies, and tactics. \emph{Words don’t mean; people mean. }

\section*{Components for Change}

Today many recognise Professor Clare W. Graves as one of the world’s leading developmental psychologists. His powerful ‘emergent, cyclical, double-helix model of biopsycho-social developmental’ framework has been popularized as Spiral Dynamics. Yet few are aware of his profound thinking on the matter of the complexities of change. We worked together on the South African transformational strategy to be able to recognise the essential sequences and timing of various interventions.

Like deep sea divers threatened both by getting the bends when surfacing too rapidly and by suffocating if they run out of air, there is a changeability range between ‘too much too soon’ and ‘too little too late.’ 

We are now witnesses to a multitude of voices about change in its 7th and 8th variations.  

Conferences, summits, and gatherings of many different shades and hues are appearing on a myriad of topics, all of which have a decided ‘global’ dimension to them; books are being published by the droves, and new names are showing up on the Web as new industries are flourishing. 

Yet, unfortunately, too often their views of human nature are trapped either in behaviourism (which claims that anybody can be changed to be anything since we are blank slates) and humanism (which claims that all can have access to and respond to everything because human beings are all the same.)  

For years Graves warned me: ‘Don, you can’t change people. All you can do is relate what you are doing to their natural motivational flows.’ This would usually be followed with the caution, accompanied by a twinkle in his eye: ‘People cannot be until they are.’ At first I didn’t know what that actually meant. 

And then would come the closer: ‘All you can do is help a country (society, culture, etc.) become what is next for it to become.’ My naïve idealism at the time was struck a fatal blow as common sense returned.
 
Consider the following pertinent questions: 

\begin{itemize}
	\item What is it that holds beliefs and behaviours in place, making them resistant to certain change efforts? 
%	\item What are the keys to dealing with fixed '\textsc{\lc{DNA}}'---like codes that lie at the core of entire societies? 
	\item Are there a set of universal dynamics that cut across all of our human experiences and endeavours? 
	\item In a practical sense, how can efforts be enhanced around, for example, preventing the spread of the \textsc{\lc{HIV-AIDS}} pandemic, or reducing global warming threats, or defusing militant ‘isms’ that spread like viruses through true believers? 
	\item If, indeed, there is some kind of evolutionary or emergent flow working within humans, there must be some kind of dynamic process that explains growth, or development. 
	\item Why do some people change but others don’t?  
	\item Why do some cultures appear to continue to prosper yet others may well be on the slippery slope toward total collapse? 
	\item What is behind the rise and fall of nations? What triggers the forces of renewal and resurgence in a human grouping, one that has once tasted of the fruits of stability and affluence? 
\end{itemize}

These are quite complex variables and a fuller discussion is not within the scope of this writing. It is still useful, however, to consider at least three of the components which play major roles in underpinning our journey on the planet. They follow in response to the essential question that we should always ask: ‘What really changes in people, structures, and processes whenever there is change?’ Can we be specific and perhaps even measure it? So much of our speculation regarding change, and especially our forecasts of what will happen when we do X, Y, or Z are based on naïve assumptions at best and unexamined cause and effect linkages at worse. It is far too easy to assume that (1) everybody is like us, so that what motivates us to ‘change,’ will do so to others, and (2) everybody has the same interests and bottom lines, and are therefore amenable to the same change messages.
  
Look, for example, at the power within the shift in educational purpose from ‘No child left behind’ to ‘Each child to full potential.’ The first rescues stragglers; the other pushes to greatness. 

This is especially the case today as many become emotionally committed to more ‘world-centric’ or global perspectives. There are many good reasons why this world view is emerging rapidly on the planet. Many of them have been featured in various issues of Kosmos. But problems stem from the belief that all humans, everywhere, will gladly choose this world-centric view over tribal, ethnic, religious, or national identities if given half a chance. If we are serious about developing more of a sense of the human family, with common interests and collaborative intent, a more comprehensive initiative will need to flow from an understanding of the essential \emph{Components of Change}: Life Conditions, Priority Codes, and Beliefs and Behaviours. 

\begin{figure}\centering
	\includegraphics[width=.8\linewidth]{parta1_1-p1}
	\caption[]{}\label{parta1_1-p1}
\end{figure}

\subsection*{Changes in our Life Conditions}

We are, to a large degree, the product of our times. We have an innate capacity to respond to the challenges within our habitats, to adjust to the problems of existence that confront us. As long as those conditions exist, it makes sense for us to embrace the ’steady as she goes’ mentality. 

Mexico is under serious threat to become a failed state because of the Life Conditions that are brewing the lethal cocktail of drugs, violence, and gangs. Fear has become rampant in the country because of the pattern of kidnapping for ransom and even brutal murder of innocent citizens. The youth are caught up in the lure of excitement and easy money. Until the baseline of stability and security is established, don’t expect Mexico to be able to keep up economically, or the various elements in society to achieve even a modicum of success. We recently met with former President Vicente Fox in León, Mexico, who has now established a Center for Global Leadership. He asked: ‘Why has Latin America lagged behind the rest of the world?

As long as there are pockets of poverty in our inner cities, with fragmented homes and lives coupled with lack of discipline and economic opportunities, expect gang-related behaviours to flourish. And if self-serving adults continue to play the race card only to gain political leverage, don’t be surprised if the amount of racism in our society expands rather than constricts. 

\subsection*{Changes in our Priority Codes for Living}

Spiral Dynamics describes the values systems that are the product of the interactions between Life Conditions and the human capacity to adapt to the problems of existence. 

Thus far, eight Priority Codes have emerged, each one calibrated to solve problems which are unique within its specific context. As invisible fractals and organising principles, they impact the full range of human choice-making capacities. Each opens new doors of reality and enable new thinking and fresh options. Sometimes realistic change involves the further development of a Code. When new Priority Codes appear, new insights must develop, as new bottom lines are embraced.

\begin{figure}\centering
	\includegraphics[width=.8\linewidth]{parta1_1-p2}
	\caption[]{}\label{parta1_1-p2}
\end{figure}

\subsection*{Changes in our Beliefs and Behaviours}

Skilful change agents have learned how to connect their ideas and projects to a wide range of Priority Codes. Many in the environmental movement were antagonistic to what they called ‘right-wing’ religious movements until they noted that these same people, ‘to keep God’s earth clean,’ will support environmental goals. In the same way, \textsc{\lc{HIV-AIDS}} campaigns can be designed, which will carry the critical message into all of the Priority Codes, rather than just focus on one or two. This kind of change can bring a Spiral full of advantages and opportunities to a number of different initiatives. 

\section*{Playing the Global Change Game}

Let us play the Global Change game. Take almost any issue that has a global reach, from hunger to disease to political conflicts to have versus have not gaps to immigration matters to environment threats. How should we decide what to do? Who should rule?  

A voting majority? Financial interests? Military clout? Moral superiority? The societal elders? The spiritual leaders? The Super Organisation? The Wizard of Oz? The Spiral Intelligences? 

May I suggest new decision-making processes. We call them MeshWorks Solutions. Imagine the gathering of the competent wise ones, who are armed with powerful technologies, deep insights, and guided by the drives within the human Spiral itself to stay alive and pass its genes and memes far into the future. These are Integral Design Engineers. What would they do? 

First, they would develop Global Vital Signs Monitors much as Singapore has done, through the efforts of John Petersen of The Arlington Institute, to construct Risk Analysis Horizontal Scanning capacity (\textsc{\lc{RAHS}}). Monitors would track the Priority Codes beneath cultures and groupings. They would read the weak signals of impending change, in its many manifestations. Second, such a MeshWorks effort would create scaffoldings of solutions to serious problems by identifying everything that works, and spreading the information through different cultures and across Priority Codes, but in their respective ‘language.’ Third, these decision processes would be the first to spot and track conflicts in the making, and to design and implement preventative efforts through a distributed intelligence that is circulated across all political structures. 

Through the efforts of Peter Merry and his Dutch colleagues, we have established such a potential function in The Hague Center for Global Governance, Innovation, and Emergence. Roberto Bonilla in Mexico is demonstrating the power in MeshWorks by helping design education for the whole country to pluck the youth from the hands of the gangsters. Lebanese-American Elza Maalouf is doing important work in Palestine and the Middle East to awaken the Arab world to new possibilities, and inspires many in Israel to envision a future 
‘Hong Kong of the Middle East.’ 

It is time to get the entire global system set right by respecting all of us who exist in clans, tribes, empires, holy orders, cultures, enterprises, communes, natural habitats – and are spread along the various trajectories of change. Imagine a global Intelligence that, much like a metaphoric ‘air traffic control’ system, can keep up with and direct our many life forms which are dispersed in different altitudes, moving in different directions, at different speeds, with different capacities, and all with multiple bottom lines and priorities. 

Why not design and build this capacity, now? Our very lives, individually and collectively, might well depend on it. 