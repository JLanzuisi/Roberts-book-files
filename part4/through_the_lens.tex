\chapter[Second tier Impact 4.0]{Through the lens of an envisaged 2\textsuperscript{nd} tier Impact 4.0}

\epigraph{The whole is a necessary to the understanding of its parts, as the parts are necessary to the understanding of the whole}{The Mach principle\cite{jaworski_synchronicity:_2011}}

We now can return to our beginning. In this chapter I will provide some of the additional ingredients necessary for this level. 

This 4.0 Impact level requires us to have already confronted our shadows, and to have reached a stage where we have found our humanity. If we have done so sufficiently well, we will have developed the prerequisites for the state of humanity and the state of our planet to open up our creativity for Impact. 

Using Spiral language, Impact 3.0 and 4.0 cannot evolve unless we move from the current predominant Orange Meme of the financial industry. We are seeing Finance beginning to develop elements of Green, and Impact having tinges of Yellow in several areas. 

If anything these trends are accelerating, but are by no means certain, given the perceived need for the finance industry to retain the post-modern paradigm. As discussed before, much of the Impact development will be based on the increased capacities to value returns from other dimensions, and to disseminate their differences. 

By saying someone is second tier, we are saying that their centre of gravity is in second tier. This means that, when all the lines of development are considered, (cognitive, emotional, social, spiritual, physical) the majority of their lines are in second tier. 

There may still be parts of themselves further down (or up) the developmental ladder. Importantly, remember that higher does \emph{not} mean better per se, just further evolved to include more states and perspectives. 

It is common for previous stages to think they are already 2nd tier if they are close to having one line (e.g., cognitive) at 2nd tier. Leadership in its complexity can actually be seen as a developmental line in itself. 

Also, as higher tiers are by definition hidden from view from lower tiers, and have to be developed to be made visible, they are often dismissed by the ego before they have a chance to emerge. 

Impact investments at 2nd tier have  now evolved to become an art form which transcends and includes the science behind it. 

Art elicits our most basic instinct for aesthetics. So in forms such as the Martial arts, mastery can only be obtained through the development of the whole person, beyond mere skill and technique. The same is true in Impact 4.0, which contains more of the non-dual states. Gestalts, in terms of measurements and calibration methodology. Here we seek to further evolve, and find culturally aligned ways to connect and interpret Impact, using common languages which each of the four worlds can understand and synthesise. 

Below is a non-exhaustive list of some of the key components of Impact 4.0, additional to 3.0. As such our objective is to envisage the evolutionary trajectory of travel, rather than a new destination. 

\begin{itemize}
	\item The deepening of the integral four-worlds and their corresponding dynamic. 
	\item Less prevalent organisational pathology e.g., black/white, digital, mechanistic thinking; and shadow parts from previous levels; are left behind.  
	\item Evolved non-local consciousness and non-dual consciousness with more acceptance of is-ness.  
	\item Removal of many of the power complexes in governance, economic, and social structures and solutions. 
	\item Ego-drops; including fewer differentiations (like ‘expert’), and fewer hierarchies of power, fewer power complexes, and with more understanding of humanity and equality.
	\item Practical outcomes from the removal of power complexes and egoic structures include senior investment executives working/volunteering in arenas including the investee and its community.  
	\item Evolved governance, beyond e.g., consensus, or even consent in conventional holacracy and sociocracy, to include external systemic stakeholders and contain more convergence around our humanness.  
	\item The spiritual dimension is now fully included to inform and guide proceedings. 
	\item Systemic combined aggregate returns, e.g. IIR, is now an acceptable form of measurement.  
	\item Market enterprise valuation methodology transmutes to include Impact, e.g., multiples of IIR.  
	\item Context equally drives co-created Impact outcome objectives, as opposed by owners of capital.  
	\item New states of consciousness (at least mindfulness) are included and valued in all proceedings.  
	\item Outcomes now drive proceedings, rather than policies and procedures driving them.  
	\item Servant leadership no longer is an oxymoron but groun\-ded in the South, where relating is the natural organising principle.   
	\item Knowledge, and its making, is valued as coming from all creation and creation's processes. 
	\item Moving measurement systems towards modelling, and e.g., Interpretative Phenomenology Analysis (\textsc{\lc{IPA}}), and other formats that capture more and deeper content.  
	\item Integral components now can be calibrated using multiple alternatives such as Hawkins’ scales.   
	\item Investment theses and reporting can now take multiple forms, drawing on art, music, poetry, movement, and other forms of expressive transmission of integral information.  
	\item Culturally aligned rituals and ceremonies are used to understand, fuse, and synthesise impact.  
	\item Vision quests are used to diagnose symbolic representations for Impact.  
	\item Much less emphasis on the model of things, and more on the intuitive and collective sensations around an investment proposition. The model may even become a hindrance to see the real impact.  
	\item An increase in creative approaches, with ‘out-of-the box’ solutions, to an investment.  
	\item Openness and acceptance to solutions arriving from alternative states of consciousness e.g., dreams, revelations, etc.  
	\item Processes and forums are designed to look for engagement to maximise collective intelligence towards dialectic ‘one-mind’. Real dialogue trumps processes.  
	\item Increased sense of freedom and space to experiment.  
	\item Increased comfort to handle ambiguity and uncertainty, with fewer ‘elephants in the room’.  
	\item Lessening of simple filtering, such as generalisations, distortions, deletions, or projections; instead there is an opening up to more direct perceptions, true implications, and a new healthy relationship with reality.   
	\item ‘I’ and ‘Thou’ i.e., ‘the other’ merges naturally to become one and now ‘us’. 
\end{itemize} 

Each one of these would need to be evolved by each firm into each organisation as new forms of interpretations of reality.  

Let us now pick one of the above and enter some depth just to illustrate the possibilities to discover here. We will briefly investigate the work on consciousness by David Hawkins. The core idea and application within Impact is that our body doesn’t lie, and we can use kinesiology technology to calibrate and find out the different levels of truth as part of any assessment. This research has received some criticism for being subjective, judgemental, and having issues with the calibration technique, which will require any user to accept or reject questions on its own merits. 

However, this research has profound implications in that it can provide an alternative objective calibration and measurement system for Impact as we move into Impact 3.0 and 4.0. During my own deeper work in Psychology I studied and trained in PsychK®, which works with belief change at a deeper subconscious level using whole-brain activation and muscle testing to calibrate and verify change; so I have some experience and validation for this technology. 

Kinesiology is the scientific study of human body movement. Kinesiology addresses physiological, biomechanical, and psychological dynamic principles and mechanisms of movement. The word comes from the Greek
{\textgreekfont κίνησις kínēsis}, 
movement’ (itself from
{\textgreekfont κινεῖν kineîn},
‘to move’), and 
{\textgreekfont λογία}
logia, ‘study’\cite{noauthor_kinesiology_2020}. 

This reconnects us with the earlier discussion on the mind-body approach, which contrasts with much of the postmodern philosophy that denies an existing objective reality, and denies moral values. Kinesiology seeks to emphasise and find a clear distinction between good and bad, knowledge from ignorance, dominance from submission, and presence from absence. It is a critical theory approach that seeks to prove that there are many universal truths. However, when looking at the behaviour of humans, we can see some universal recurring behavioural patterns, such as in our facial expressions: 

\begin{itemize}
	\item We will always interpret a smile as something positive, even if we’ve lived in isolation for our entire life. 
    \item Likewise, we will subconsciously recognise an angry face as potentially dangerous and possibly something to avoid.
\end{itemize} 

The first work on applied kinesiology was pioneered by George Goodheart. He discovered that the body reacted to subtle physical stimuli in predictable ways. For instance, beneficial nutritional supplements would increase the strength of specific indicator muscles, whereas harmful stimuli would cause those same muscles to weaken. Basically, the body knew at a subconscious level what was good or bad for it. John Diamond subsequently refined the technique into a new discipline called behavioural kinesiology. Using the same testing approach, he confirmed that specific indicator muscles would strengthen or weaken in response to positive or negative emotional and physical stimuli. 

For example, a smile would make us test strong, while the statement ‘I hate you’ would make a muscle weaker. He showed that these results were predictable and universal. Somewhat paradoxical was that this was true even with no clear rational link between stimuli and response. In fact, he showed that certain random abstract symbols caused all subjects to test weak, whereas others made them strong. This indicates the presence of an objective communal consciousness embedded in the mind or \emph{spiritus mundi}. The most renowned psychologist in this field of work is Carl G. Jung, who coined the term collective unconscious. He suggested that certain structures of the subconscious mind are phenomena shared amongst all beings of the same species.  

\begin{itemize}
	\item  A fish swimming at the edge of a school will turn instantly to another direction when its partners spot a predator and start to flee, even if they’re a quarter mile away. 
    \item Birds follow the same flight routes generationally even if they haven’t been taught to. 
    \item Adult salmon swim upstream from the ocean to hatch and die, as did their own parents who couldn’t have passed on this behaviour from direct experience. 
\end{itemize}

So, are we talking about a field of consciousness, in addition to our own internal consciousness, which we can tap into? Two influential fields of study in modern mathematics are dynamical systems theory and chaos theory, which describe complex dynamical systems and their motions. Attractor patterns are particular kinds of recognizable patterns, or ‘traces,’ left in the system by the movements of their agents. Hawkins suggested that:  

\begin{quote}
    The brain’s neural networks work like a system of attractor patterns that represent energy fields with qualities of consciousness itself and not the individual	\cite{hawkins_power_2002}.
\end{quote}

According to Hawkins humans live and operate at vastly different ‘levels’ of consciousness. Each level, along with the ‘truth level’ of \textsc{\lc{ANY}} true/false style inquiry, can be tested for truth and numerically ‘calibrated’. It is as if we unconsciously know what’s right and wrong, good and bad, true and false. But it seems we are already in possession of this knowledge which can be accessed at all times. In quantum physics, the question of consciousness is taking the form of a unified field theory, or that of a quantum mind. The unified field of human consciousness is not just experienced by the mind and accessed by the body. The kinesiology test does not show a local reaction to the body but is a general response of consciousness to the energy of a substance or a statement 	\cite{hawkins_power_2002}.

A truthful answer will yield a positive response, indicated by the muscles strengthening, as a result from the impersonal field of consciousness that exists in all things living because the brain receives energy from the patterns that exist a priori in the mind. This led to Hawkins’ work on kinesiological measuring of consciousness in 1975 and over the following 20 years, he built a scale of relative truth, by using mathematical terms of nonlinear dynamics, which ranges from 1 to 1000. 

\begin{figure}\centering
	\includegraphics[width=1\linewidth]{part4_1-p2}
	\caption{Power vs Force Scale of Truth}\label{part4_1-p2}
\end{figure}

The scale indicates the quantum of truth of any expression, such as people, events, and objects, from the energy they emit and imprint into the quantum field. Those vibrations of energy get perceived by attractor patterns of the brain at the subconscious level. According to Hawkins, any person, concept, thought or object that calibrates at 200 (The level of Integrity) or above is positive (‘power’); anything below 200 is negative (‘force’).  

His work also relates strongly with the biologist’s Rupert Sheldrake’s ‘morphogenetic fields’ hypothesis\cite{sheldrake_morphic_2009}. Essentially, morphogenetic fields are groups of cells that respond to biochemical signals, which leads to the development of organs, limbs, muscle tissue, etc. Our body knows how to grow a foot because of our cells are receiving information both from the \textsc{\lc{DNA}} and from the field. 

These are also related to the emergent area of Epigenetics showing that our cell biology is also directed by signals from our thoughts and feelings as outlined in Bruce Lipton’s book ‘The Biology of Belief’\cite{lipton_biology_2016}. The Morpfield findings suggest that every event in history, every person who has ever lived, every action we will take, and every thought we have, influences the state of this quantum field of consciousness. 

It is a system that’s in constant motion, and the traces of which are hidden from the rational mind, so can be accessed only by the body. If so, Hawkins’ quote in \emph{Power versus Force} gives us all something to consider: ‘That one’s every thought and action leaves an indelible trace forever in the universe can be an unsettling thought’\cite{hawkins_power_2002}.

Hawkins found that specific levels have emotions attached; ranging from fear, anger, and anxiety, scoring on the low end; and joy, love, compassion on the high end. For instance, Mother Teresa calibrated at 500, whereas Adolf Hitler was under 150, whilst according to Mark Anielski, the banking industry calibrates in general below 100 with outliers such as Goldman Sachs at 25. Hawkins concluded that the ‘\ldots\, quantum field of consciousness is a powerful attractor which organises human behaviour into what is innate to humanness’\cite{hawkins_power_2002}.

\begin{figure}\centering
	\includegraphics[width=1\linewidth]{part4_1-p3}
	\caption{Power vs Force Map of Consciousness }\label{part4_1-p3}
\end{figure}


Our deep consciousness operates from the perspective that it can do us no harm, that only truths have actual existence, and that the difference between these vibrational levels is a matter of degree. The amount of energy we can attach to anything depends on how truthful it is, which in turn is driven by the degree of its life-enhancement. Does it create duality, or does it seek unity, which as we discussed is one of the prime self-organising principles of life. 

\begin{itemize}
	\item For instance, all lower level vibrations are based on ego, duality, and negativity. Shame: 20, Guilt: 30, Fear: 100, Anger: 150 and Pride: 175 are all low-energy emotions. These are all life-destructive emotions with negative energies. 
\end{itemize}

Only at Courage: 200 can we get access to any positive states. It is a transitory point, in which we go from negativity to affirmation and empowerment. We’ve managed to gain enough bravery to realize the dual nature of the ego and have decided to strive for improvement. Although it is only the first step towards the right direction, it doesn’t mean we’ve made it by any means. Neutrality is at 250, which indicates trust in oneself and a release from the negativity.

\begin{itemize}
	\item Emotions above 200 are life-enhancing emotions with positive energies. Willingness: 310; Acceptance: 350; and Reason: 400 are about raising levels of self-awareness and becoming a better person. 
	\item At Love: 500, we hit another revelation and become more egoless. The idea is that we increase our consciousness to a point in which we become more selfless and empathic. What ensues is Joy: 540 and Peace: 600. 
	\item At 700 we reach enlightenment, which makes us overcome all dualities and attain this total unity with all things living – pure consciousness. This is the point of complete self-transcendence characterised by the Buddha, Jesus, and Krishna. 
	How do we measure the amount of truth? We will need two people to do a kinesiology test. 
	\item The subject has to hold their arm laterally, parallel to the ground, while the tester proposes a yes or no statement. 
	\item While they hold the statement in mind, their arm’s strength is tested by applying pressure to it. 
	\item If the answer to the question is correct, then the subject would find strength to resist; If wrong, they will give in. 
\end{itemize} 

Because consciousness is infinite in capacity, the statement needs to be relative to the reference scale created by Hawkins. Otherwise, the numbers would be arbitrary. For instance, ‘On a scale of human consciousness from 1 to 1,000, where 700 indicates Enlightenment, this \underline{\phantom{motive}}(item, motive, emotion, person, idea, event etc.) calibrates over\underline{\phantom{motive}}(a number).’ 

The map of consciousness differentiates between the two sides of the same coin --- action and inaction and the changing and changeless. 

Force is a push of something that changes the momentum of something. It is something we do and apply to try and get certain results or project onto others or even ourselves. 

\begin{itemize}
	\item  Power, on the other hand in a pull, is internal, existing and omnipotent so no need to apply force as it transfers energy by attraction. 
\end{itemize}

Once Force comes into contact with Power, it dissipates, e.g., from independence movements to the abolition of slavery and apartheid. In organisational settings this can become a cultural Achilles heel that lead to disengagement from mission and vision.  

However, force isn’t necessarily a bad thing. We have to keep in mind that the difference between states of consciousness is still only a matter of degree. Some are simply brighter expressions of truth, whereas others are dimmer. Of course, we should remember that Power is still constructive. Higher vibrations are also more positive and life-enhancing, but there is a place and time for force.

Force is necessary for Power to be manifested. If there weren’t any objects for gravitational force to affect, then it wouldn’t be powerful. It is the notion of being powerless that allows us to become powerful. To have courage, we need also to feel fear. We don’t want to be using Force indiscriminately or indefinitely as it is still motivated by a dual nature between us and our cause. But don’t be fooled into thinking that we can simply ignore it either. It takes Force to cause a change and create momentum in anything. However, once momentum has been achieved, we need less force and to replace it with accessing our Power. Then, our consciousness expands and instead of pushing things we start to pull things towards ourselves almost like gravity.  
 
The life force Power is self-organising and assimilating as it seeks growth, expansion and nourishment. Negative Force is the opposite, it tears down, fragments, breaks away and generates negative outcomes. However, this dynamic also burns the old to create fertile ground for new existence to take root. As we know, personally, death is needed for the ego to be reborn into a more powerful version of ourselves. 
Some interesting facts concerning consciousness calibration: 

\begin{itemize}
	\item Powerful patterns are associated with health; weak patterns are associated with sickness. Every thought, emotion, word and action have one pattern or the other. Every moment of our day we are either moving towards health or sickness. 
	\item Everything calibrates at certain levels from weak to strong including books, food, water, clothes, people, animals, buildings, cars, movies, sports, music, etc. 
	\item Most music produced today calibrates at levels below 200. Hence it leads to behaviour associated with lower energy levels of consciousness. 
	Most movies will weaken people who watch them by bringing their energy levels down below 200 level.  
	\item 85\% of the human race calibrates below the critical level of 200. 
	\item The power of the few individuals at the top counterbalances the weakness of the masses. 
	\begin{itemize}
		\item 1 individual at level 300 counterbalances 90,000 individuals below level 200 
		\item 1 individual at level 400 counterbalances 400,000 individuals below level 200 
		\item 1 individual at level 500 counterbalances 750,000 individuals below level 200 
		\item 1 individual at level 600 counterbalances 10 million individuals below level 200 
		\item 1 individual at level 700 counterbalances 70 million individuals below level 200 
		\item 12 individuals at level 700 equal one Avatar (e.g. Buddha, Jesus, Krishna) at level 1,000 
		\item Any meaningful human satisfaction cannot even commence until the level of 250 where some degree of self-confidence begins to emerge. 
		\item When one’s consciousness falls below 200 at any given moment, we start to lose power and thus grow weaker and more prone to be manipulated by one's surroundings.
		\item Parts of one’s life will calibrate at a higher level of consciousness while other parts will calibrate at lower levels. It is the overall average that determines one’s consciousness.  
	\end{itemize}
\end{itemize}

Universal principles calibrate at over 700 and affect mankind over long periods of time. Power originates from the unlimited mind whilst force is only rooted in the limited material world. Power as such will always defeat force but force is seductive and can often masquerade as a facile solution, serendipitously gaining the upper hand. We often see this in politics where a true statesmen/woman sacrifices him/ herself to serve the people versus the politician who sacrifices people to serve themselves. Colonialism of the type which we saw in India calibrates at 175 whilst Gandhi calibrated at 700 (along with Mandela in South Africa). 

Real truths however need no defence, as they are self-evident and based on inviolate principles; e.g., the US declaration of independence which states ‘that all men are created equal’ (unless you consider the term men to be gender exclusive) calibrates at over 700. Power is associated with things that are unifying, life supporting and enhancing, whilst force is divisive, polarising and life diminishing. Power attracts whereas force repels.  

Business practises, people, and products can be calibrated to ascertain whe\-ther the ingredients are more or less aligned with power or force. This is how one would expand and develop I\textsu{3} to understand its further application. Regardless, we now know that companies operating according to such principles outperform their peers, as the culture the principles are embodied in is non-replicable. This is exemplified in many successful businesses, e.g. Walmart and \textsc{\lc{IKEA}}. 

The classic book ‘In search of Excellence’ found that that successful companies had heart, rather than being only left-brained and scientifically managed. Whilst this type of calibration methodology may be a step too far for many left-brain investment practitioners, for Impact 3.0 and 4.0 we can see how working with kinesiology may become part of a protocol and measurement system which can capture many of the subjective/more qualitative aspects. 

However, nothing will be more important than building your capacity to love.  


\section{Integral Love}

Following on the path of fourfoldness, we can see that since ancient times, the noun and verb love has been considered in its four parts. The Greeks have 4 words for love: Storge, Philia, Eros, and Agape. The British writer and lay theologian, C.S. Lewis, took this concept into what became his 1960 book “The Four Loves” based on thought experiments around the concepts of love following on the same lines as the Greek philosophers.  

\subsection{Storge/Affection Love}

The first Greek love is storge which can be thought of as an affectionate, familial quality of love. It’s the feeling you have for your closest relations. It can even be the affection you feel towards for your pet animal. In relational terms, it’s what is rooted and created in our Integral South.  

To Lewis, storge is the most organic form of love. It’s something anyone can feel. Something as natural as falling asleep at night. He says it’s the most comfortable and least ecstatic of all loves. It’s not the rapturous feeling you have when falling in love with a partner. Lewis describes it as soft slippers, or what an old, worn out chair might feel to your body.  Storge wraps you around like a blanket, almost like sleep itself. 

\subsection{Philia/Friendship Love}

The second type of Greek love is philia which is the reciprocal quality of love you may feel for a friend. Lewis describes Philia as having a dispassionate quality.  It was to him the least natural of all loves. 

He says “There’s nothing that quickens the pulse, or makes you red or pale.” If you compare it with the romantic type of love that makes your heart race and palms sweat, philia is much more dispassionate. In relational terms is the rational kind of love we find in our Integral North. 
 
However, Lewis doesn’t discount it. He says philia love is very necessary. It produces the fewest pains and most pleasures. 

He described philia as the “crown of life.” The Greek philosopher Aristotle described three levels of philia. 

The first level of philia is friendship of utility. These are your acquaintances, work friends, or other relationships where some practical benefit is received by both parties. These types of relationships come and go throughout your life. 

The second level of philia are friendships of pleasure. These are your hang out friends, or even a friend who might be going through a similar hardship you’re experiencing. Just like the first level – these types of friends come and go. 

The third and highest level of philia that Aristotle described are friendships of the good. It’s these rare individuals you can trust and call your true friend. These are people where you can go without speaking for years, yet see each other and feel like you hung out just the other week.

These relationships are rare and shouldn’t be taken for granted. They say, if you have even three of these friendships, you’re living a rich life.

\subsection{Eros/Romantic Love}

Eros is the romantic flavour of love. It’s passionate, it’s rapturous and euphoric. It’s for what some of the greatest songs and poems have been written about. Being the expression and projection of our deepest inner self, it’s what many consider the greatest feeling of the human experience; yet thousands throughout history have died over this feeling. It’s where we create the deepest relevance with our beloved, and is found mainly in our Integral West. 
 
It’s through romance that we can project all our hopes and expectations, and which can truly feel like heaven on earth for a period of time\textemdash until reality meets up again with expectations. Dr. Seuss once said. “You know you’re in love when you don’t want to fall asleep because reality is finally better than your dreams.” 

Eros love can be volatile, the greatest source of pleasure, yet at the same time can bury you into some of the darkest, most painful periods when it ends.  

\subsection{Agape/Charitable Love}

Agape, in its root form, means “wide open”, in a state of wonder. It’s a spiritual portal which creates your awareness and consciousness. It is the love that consumes the person who experiences it. Whoever knows and experiences agape learns that nothing else in the world is important – just love. 

When you connect with awareness itself, that’s exactly the wide open state of mind you’re in. Not focusing on any one thing. Not clinging onto anything. Constantly letting go. Constantly opening up to what’s in front of you. Agape is also directionless, i.e. it’s not focused on anything else apart from what’s inside you. Agape is also conditionless, it needs neither any special knowledge nor expertise. The only ability required is the ability to let go. Agape is what we mainly find in our Integral East.  

Paulo Coelho describes Agape Love in his book 'The Pilgrimage':

\begin{quote}
Agape is total love, the love that devours those that experience it. Whoever knows and experiences Agape sees that nothing else in this world is of any importance, only loving. This was the love that Jesus felt for humanity, and it was so great that it shook the stars and changed the course of man’s history.
\end{quote}

It’s a type of love few people have heard of, let alone understood, let alone experienced. Here is an excerpt from Eckhart Tolle’s 'A New Earth'\cite{tolle_new_2005}: 

\begin{quote}
A beggar had been sitting by the side of a road for over thirty years. One day a stranger walked by. “Spare some change?” mumbled the beggar, mechanically holding out his old baseball cap. “I have nothing to give you,” said the stranger. Then he asked: “What’s that you are sitting on?” “Nothing,” replied the beggar. “Just an old box. I have been sitting on it for as long as I can remember.” “Ever looked inside?” asked the stranger. “No,” said the beggar. “What’s the point? There’s nothing in there.” “Have a look inside,” insisted the stranger. The beggar managed to pry open the lid. With astonishment, disbelief, and elation, he saw that the box was filled with gold.	
\end{quote} 

This is what the fourth and final love is all about. There’s something within you containing all the love you could ever ask for. You don’t really need anything which is not inside you already.  

Lewis\footnote{\url{https://www.cslewis.com/love-love-love-love/}} reminds us: 

\begin{quote}
There is no safe investment. To love at all is to be vulnerable. Love anything, and your heart will certainly be wrung and possibly be broken. If you want to make sure of keeping it intact, you must give your heart to no one, not even to an animal. Wrap it carefully round with hobbies and little luxuries; avoid all entanglements; lock it up safe in the casket or coffin of your selfishness. But in that casket\textemdash safe, dark, motionless, airless\textemdash it will change. It will not be broken; it will become unbreakable, impenetrable, irredeemable. The alternative to tragedy, or at least to the risk of tragedy, is damnation. The only place outside Heaven where you can be perfectly safe from all the dangers and perturbations of love is Hell.
\end{quote}