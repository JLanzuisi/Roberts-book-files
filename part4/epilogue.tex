\chapter{Epilogue}

As we reach the end of this book, I want to sincerely thank you for taking the time to read it, and to consider its content. We have covered much territory. I know it is not a complete treatise, nor can it always lay out succinctly the component parts you most need. 

I hope, though, that you have found your own golden thread, to help define and design your own thinking, to take forth your own, co-created Impact approach, now in a more ‘integral’ guise. The Impact path, as we know, is an artful and creative path for us all to journey along, as we shape what kind of world we want, and what kind of legacy we want to leave. 

If we can fill our Impact journey artfully with beauty, we create an opening for others through which they may wish to walk. Beauty as such is a doorway to our calling.   

\epigraph{The time will come when, with elation you will greet yourself arriving at your own door, your own mirror and each will smile at the other's welcome, and say, sit here. Eat. You will love again the stranger who was yourself. 
	
Give wine. Give bread. Give back your heart to itself, to the stranger who has loved you all your life, whom you ignored for another, who knows you by 
heart. Take down the love letters from the bookshelf, the photographs, the desperate notes, peel your own image from the mirror. Sit. Feast on your life}{Derek Walcott\cite{derek_love_love}}

As long as we don’t shy away from the challenge, as it is through how we conduct our Impact lives that we will find the energy and courage to fully engage with the true impact spectrum, in order to seek the diamond which is already within ourselves. We can’t be everything to everyone, but we can always be something to someone. 

I\textsu{3} may seem to some to be an overly ambitious attempt to create an over engineered and complex approach; and yet, like most things in life, success is only possible if the approach we use is complex enough for the challenge we face; and achieving that success depends on how far we are prepared to go down the rabbit hole to find the truths that are relevant for us. 

However, I trust we have also realised that how far each of us goes is completely up to us, and our organisation, and our direction of travel. 

As mentioned in the beginning, this book was intentionally construed as a personal journey. Whilst many of the arguments may seem to some as sometimes un-balanced, overly critical, or pessimistic, the intention was to provide a challenging narrative to the often conventional, more abstract, and mechanistic modes of thinking. 

My default position is mostly as an optimist, but to challenge myself, to end up closer to reality, I need to counterbalance optimistic with more pessimistic views. Maybe as Antonio Gramsci once suggested, we need to be pessimists of the intellect and optimists of the will.  

Equally important, however, is to ensure we work with and/or for an organisation that shares our values. Challenge your organization’s integrity, dig deep into their track record, values, and beliefs to fully understand how they actually operate regardless of the rhetoric. 

Talk to investees; look at how exits and divestments were handled, and at how governance and ownership is aligned with rhetoric, values and beliefs, both inside and outside the firm. 

How does the organisation co-create value with investors and investees? To what extent do they allow us to contribute co-created value and knowledge creation? How does this inform and help us to navigate our own calling inside Impact? Having crafted and mastered our calling, context, and co-creation, in which venue and under which auspices do we generate the intended effects? Is there alignment and harmony, and a cardiant dialectic that truly resonates? It is through our inner liberation that we create outer transformation, and can journey into impact. Like all endeavours, it is an opportunity, not only to grow, but also to find one’s true calling. 

If we take a true and integral approach, it offers each of us areas of significance and complexity to deepen our understanding about ourselves and the bigger ‘other’. Our calling as rooted in our values, talents and capacities. What we find beautiful is the foundation stone on which we lay our blueprints and envisaged architectures for our lives. It is only through our courage and integrity that we clarify and have the wherewithal to stick to the course. 

\epigraph{There is a vitality, a life force, an energy, a quickening that is translated through you into action, and because there is only one you in all of time, this expression is unique. And if you block it, it will never exist through any other medium, it will be lost. The world will not have it.}{Martha Graham\cite{reeve_grace_2001}}

According to an old Hindu legend there was a time when all men were gods, but they so abused their divinity that Brahma, the chief god, decided to take it away from men and hide it where they would never again find it. Where to hide it became the big question. When the lesser gods were called in council to consider this question, they said, ‘We will bury man’s divinity deep in the earth.’ But Brahma said, ‘No that will not do, for man will dig deep down in the earth and find it.’ 

Then they said, ‘Well, we will sink his divinity into the deepest ocean.’ But again Brahma replied, ‘No, not there, for man will learn to dive into the deepest waters, will search out the ocean bed, and will find it.’ Then the lesser gods said, ‘We will take it to the top of the highest mountain and there hide it.’ But again Brahma replied, ‘No, for man will eventually climb every high mountain on earth. He will be sure some day to find it and take it up again for himself.’ Then the lesser gods gave up and concluded, ‘We do not know where to hide it, for it seems there is no place on earth or in the sea that man will not eventually reach.’ Then Brahma said, ‘Here is what we will do with man’s divinity. 

We will hide it deep down in man himself, for he will never think to look for it there.’ Ever since then, the legend concludes, man has been going up and down the earth, climbing, digging, diving, exploring, searching for something that is already in himself.\cite{collin_answer:_2015} 

We have not elaborated much on the spiritual dimension to investments, since for many it is highly controversial. However, given the direction of travel for impact, it is likely that we will see more investment firms deploying a full-spectrum impact approach as guided by their spiritual principles.  

I wish you luck on your journey, my fellow traveller, and until we meet again, in peace and love. Love is a personal understanding; and is also a verb, which is how it must be activated within Impact. 

\begin{center}
	\emph{‘We are not human beings having a spiritual experience.\\ 
	We are spiritual beings having a human experience.’} \\
	Pierre Teilhard de Chardin\cite{pierre_wikiquote}
\end{center}
\vskip1em
\begin{center}
	{\large Ode to Impact Investments\footnote{Ode: from the Greek oide, meaning song, or as a suffix meaning way or path, from the Greek hodos}} \\
	Oh friend, no more these sounds! \\
	Let us sing more cheerful songs, more full of joy! \\
	Joy, Daughter of Elysium\ldots\, thy magi power re-unites\\ all that custom has divided all men become brothers\ldots\, 
\end{center}

How many recognise these words as those of Schiller, the great German poet, which Beethoven used to conclude his magnificent 9th Symphony? Many have been touched by the powerful strains of Beethoven's symphony. Even though we are unlikely to hear this at our next meeting, we are responsible; so ‘Let it begin with me’\ldots\, Let us have ‘High hopes but low expectations’ in all our endeavours. Whatever we do and however we do it, let’s do it in love for ourselves and ‘the other’.  

\epigraph{Nothing that is worth doing can be achieved in our lifetime; therefore we must be saved by hope. Nothing which is true or beautiful or good makes complete sense in any immediate context of history; therefore we must be saved by faith. Nothing we do, however virtuous, can be accomplished alone; therefore we must be saved by love.}{Niebuhr. 2008. Conclusion. Ch. III. \cite{niebuhr_irony_2008}}

We live in an era of the individualism cult: an I, Me, More, world of only feel good; seeking excitements, and quick fixes, and ‘updates/downloads’ for everything, including our own growth. Nothing of real or authentic value can happen that way; quite the opposite, it is based on Grace and Humility. Learning and knowledge can only move into wisdom through acceptance and integration of one’s own shadow, so as to become fully authentic as human beings. To be authentic also means looking at, and understanding, our own shadows, with all their cracks of openings. 

Impact can potentially form the wedge to open the current financial and economic dogma. As Leonard Cohen sang ‘There is a crack, a crack in everything. That's how the light gets in.’ If Impact is to be the ‘light’, then we definitely have work to do\ldots\, however, nothing of value is created without coming from love. Immerse yourself, and you will find love in abundance, and all over Impact. Maybe just around the corner, but it is there, first in you, and so within and so without\ldots\, 
 
The late Dr Martin Luther King Jr once said: 'We must discover the power of love, the redemptive power of love. And when we do that, we will make of this old world a new world, for love is the only way\footnote{M.L. King from a 1957 sermon on the subject of “Loving Your Enemies,” delivered at the Dexter Avenue Baptist Church in Montgomery}.' 

Love is not a word we hear often in any office. Nevertheless, it operates in all of our domains in life, sometimes contrary to our take on the presenting evidence.
  
Full spectrum impact requires love to be present, as the oil that reduces the friction between the competing claims inside any investment. Love (as per the 5 As) is also the core ingredient needed for a governance approach that can hold and include the real impact sphere. Let us end with one of the best know verses from the Bible, one we have often heard as part of a marriage ceremony\textemdash but when we read it, think of our own relationship (and marriage) to Impact.   

\begin{quote}
\begin{center}
	{\large Corinthians 13\footnote{The Bible, 1. Corinthians. 13. NIV}}
\end{center}
If I speak in the tongues of men or of angels, but do not have love, I am only a resounding gong or a clanging cymbal. If I have the gift of prophecy and can fathom all mysteries and all knowledge, and if I have a faith that can move mountains, but do not have love, I am nothing. If I give all I possess to the poor and give over my body to hardship that I may boast, but do not have love, I gain nothing. Love is patient, love is kind. It does not envy, it does not boast, it is not proud. It does not dishonour others, it is not self-seeking, it is not easily angered, it keeps no record of wrongs. Love does not delight in evil but rejoices with the truth. It always protects, always trusts, always hopes, always perseveres. Love never fails. But where there are prophecies, they will cease; where there are tongues, they will be stilled; where there is knowledge, it will pass away.  

For we know in part and we prophesy in part, but when completeness comes, what is in part disappears. When I was a child, I talked like a child, I thought like a child, I reasoned like a child. When I became a man, I put the ways of childhood behind me. For now we see only a reflection as in a mirror; then we shall see face to face. Now I know in part; then I shall know fully, even as I am fully known. And now these three remain: faith, hope and love. 
But the greatest of these is love.
\end{quote}

So if love is the foundation upon which we stand, then it must become part of our work within impact; part of our Impact journey, part of what it means for us and how we now contribute towards our own story and ‘Ode to Impact’\ldots.  

When we work on Impact, we cannot be indifferent; we need to create differences, contrast, and clear maps, to understand the shadows and angles within each investment context. We need to use our capacity for love to understand what will bring composite returns and value across the capital spectrum. If love is the real Impact driver then let’s remind ourselves the words of Holocaust survivor, Elie Wiesel: 

\begin{quote}
The opposite of love is not hate, it is indifference. The opposite of art is not ugliness, it is indifference. The opposite of faith is not heresy, it is indifference. And the opposite of life is not death, it is indifference.\cite{elie_oxford}
\end{quote} 

You may be familiar with the ancient saying that ’All roads lead to Rome’, implying that the same outcome can be reached by many methods or ideas. So not only can large goals be reached along many possible routes, but also, more subtly, each route will give the traveller a different experience. I therefore often add my own little ending, saying that ‘All roads may lead to Rome but if you want to get there, you must pick one’. It is all about the journey, not the destination. So pick and choose your route well, its the experience of the journey that matters in how you end it.   

\begin{center}
\textsc{\large\lc{The Tree of Dreams}}\\
	Many had gathered under the tree of dreams. \\
	All but one stood shaking its branches for dreams to fall.  \\
	Dreams that had been whispered to them by the voices of others.  \\
	Dreams that would fade with time.  \\
	But one sat quietly, waiting for a dream to recognise his soul.  \\
	And to consume him with no doubt. \\
\end{center}

We have travelled through deep and wide territory that has helped set the tone for our Impact practise and our relation to capital therein. If you are part of a family with managed investments, you may have new challenges to build healthy meanings and purposes for yourself and your descendants. Consider these carefully, with Integral Impact in mind. 

Investment organisations likewise will need to frame Impact carefully and deliberately, as part of their strategy, as well as their capital retention and attraction. So is some form of Integral Impact going to the golden key to unlock your Impact perspective and do some serious work on your own shadows? To what degree will your work contribute to the world? Will such a process help alleviate its many imbalances and help ensure our very survival? If you will fully take up this challenge, this becomes your calling and your context. It will need co-creation to find your real, fully integrated contribution. In this endeavour, and on your travels, I wish you a safe and fruitful journey. 

I would like to end with an excerpt from an important speech made by the former US President, Theodore Roosevelt, which was entitled “Citizenship in a Republic”, given at the Sorbonne in Paris, France on April 23, 1910, which encapsulates the critical path for us all. The passage on page seven of the 35-page speech is referred to as "The Man in the Arena"\footnote{ “The Man in the Arena". Theodore Roosevelt Center at Dickinson State University. Retrieved 2019-12-14}: 

\begin{quote}
It is not the critic who counts; not the man who points out how the strong man stumbles, or where the doer of deeds could have done them better. The credit belongs to the man who is actually in the arena, whose face is marred by dust and sweat and blood; who strives valiantly; who errs, who comes short again and again, because there is no effort without error and shortcoming; but who does actually strive to do the deeds; who knows great enthusiasms, the great devotions; who spends himself in a worthy cause; who at the best knows in the end the triumph of high achievement, and who at the worst, if he fails, at least fails while daring greatly, so that his place shall never be with those cold and timid souls who neither know victory nor defeat.	\cite{brown_dare_2018}
\end{quote}
%\clearpage

\section*{An Ode to Impact}

\begin{flushleft}\itshape
	Allow yourself to dream the dreams of what is to become of this visitation, give yourself this gift without hesitation. Garner your valiant contemplations and let the 
	true calling the heart be the architect and the beautification of your foundations. Seek and ground the wants of the soul to marry with love the needs of your role for its formation. Rejoice in its unfolding as part of life’s greater mystery, goal and for your own impact transformation. 
\end{flushleft}

\section*{The End}

For possible additional reading, this book was essentially intended to be a follow on to such authors who have managed to comprehensively synthesise the current state of our economic paradigm and limitations with data, constructive solutions and ways forward for the economy to evolve. These are not limited to and include:  

\begin{itemize}
	\item The Doughnut Economy by Kate Raworth.\cite{raworth_doughnut_2017}  
	\item Reinventing Organisations by Frederic Laloux \cite{laloux_reinventing_2014} 
	\item The Purpose of Capital, Jed Emerson \cite{emerson_purpose_2018} 
	\item Real Impact by Simon Morgan \cite{simon_real_2017} 	
	\item Making Money Matter by Benjamin Bingham \cite{bingham_making_2015} 
	\item The Impact Investor by Clark, Emerson and Thornley \cite{clark_impact_2015}
	\item The Clean Money Revolution by Joel Solomon  \cite{solomon_clean_2017}
	\item Principles and practise of Impact Investing, by Vecchi, Balbo, Brusoni and Caselli.  \cite{vecchi_principles_2016}
	\item The Economics of Happiness and An Economy of Well-being by Mark Anielski \cite{anielski_economics_2007} 
	\item Synchronicity by Joseph Jaworski \cite{jaworski_synchronicity:_2011} 
	\item Maps of Meaning by Jordan Peterson \cite{peterson_maps_1999}
	\item Designing Regenerative Cultures by Daniel Christian Wahl \cite{wahl_designing_2016}
	\item From Good to Great and How the Mighty Fall by Jim Collins \cite{collins_good_2001} 
	\item Profit Beyond Measure, by Johnson and Broms \cite{johnson_profit_2000} 
	\item Small is Beautiful by E.F. Schumacher \cite{schumacher_small_1993} 
	\item Thinking in Systems, by Donella Meadows \cite{meadows_thinking_2008}
	\item Ethical Markets, by Hazel Henderson \cite{henderson_ethical_2006}
	\item Capital in the Twenty-First Century by Thomas Piketty  \cite{piketty_capital_2017}
	\item The Great Divide by Joseph Stiglitz  \cite{stiglitz_great_2016}
	\item The Spirit Level by Pickett and Wilkinson \cite{wilkinson_spirit_2010}
\end{itemize}

I also have to recognise authors and psychologists such as the great Peter Drucker, Clayton Christiansen, Peter Senge, Christian Felber, Gunter Pauli, Charles Eisenstein, Jane Gleeson-White, Edward Deming, Ichak Adizes, Stephen. R. Covey, Daniel Kahneman, C.G. Jung, Joseph Campbell, Wayne Dyer, James Hollis, David Whyte, Parker J. Palmer, Eckhart Tolle etc. etc. and last but not least, my Professors on this Integral journey and my Doctorate, Drs Ronnie Lessem and Alexander Schieffer, all who have been individually been and remain invaluable teachers to me throughout my own travels. And the list of course could go on and on including Mervyn King, Yanis Varoufakis, Norman Chomsky, Naomi Klein, Robert Schiller etc. etc. What some of these authors have in common is with great courage to provide multiple perspectives that challenges the thinking around the current dogmas rather than just staying on course and sailing on with the prevailing winds. This gift to us sometimes comes a great personal cost and sacrifice as one may be seen to ‘step out of the club’ rather than continue to play the game according to the prevailing rules. We owe a big ‘thank you’ to those who courageously followed their inner callings and provided us with their insights. 