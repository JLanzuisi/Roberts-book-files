\begin{figure}\centering
	\includegraphics[width=1\linewidth]{part2_7-p4}\\
	{\small Image courtesy of Kay Jackson \cite{kay_jackson_webpage}}
\end{figure}

\chapter{Impact 1.0}

\epigraph{Long-term commitment to new learning and new philosophy is required of any management that seeks transformation. The timid and the fainthearted, and the people that expect quick results, are doomed to disappointment}{William Edwards Deming\cite{frazier_roadmap_1997}}

Let us clarify: when we simplify by denominating things into a hierarchy (e.g., a 1.0 to gain structure, simplification and order), the risk is always to assume that one thing is better or worse than another rather than digging deeper into our understanding. Hierarchies based on natural order, however, ‘transcend and include.’ One serves as a foundation for the next, without which it could not exist or come into being.  

Calling anything ‘Impact 1.0, 2.0 and 3.0’, as in this book on impact, grossly oversimplifies the rich field of the impact industry with its nuanced variations and overlaps, so my apologies as we illustrate what is required for moving towards a transformative development at each stage.  
Today we see a continued distinction between a world dominated by conventional investments on the one hand and philanthropy and gifting on the other. Impact currently aims to fit inside this paradigm, and each of us must find our home. This depends on context, culture, and possibly religion, for example with the use of Islamic finance. However, Impact 1.0 has evolved historically out of corporate social responsibility (\textsc{\lc{CSR}}), social auditing and reporting, and well-being economics. It is now embedding \textsc{\lc{ESG}} as its next evolutionary stage.  

We have now evolved from the mindset and culture of ‘doing less bad is not good enough’ into trying to do more good. The history of \textsc{\lc{CSR}} can be traced back to Ancient Mesopotamia (1700 \textsc{\lc{BC}}) when King Hammurabi introduced a code in which builders and innkeepers were put to death if their negligence caused the deaths of others, or major inconvenience to local citizens. In Ancient Rome senators grumbled about the failure of businesses to contribute sufficient taxes to fund their military campaigns, while in 1622 disgruntled shareholders in the Dutch East India Company issued pamphlets complaining about management secrecy and ‘self-enrichment’.  

With industrialisation, the impacts of business on society and the environment assumed an entirely new dimension. The ‘corporate paternalists’ of the late 19th and early 20th centuries used some of their wealth to support philanthropic ventures. By the 1920s, discussions about the social responsibilities of business had evolved into the beginnings of the ‘modern’ \textsc{\lc{CSR}} movement. As the current format of \textsc{\lc{CSR}} emerged out of the social consciousness soup of the 1950s, the primary focus was to add business responsibilities prevalent at the time. ‘The phrase Corporate Social Responsibility was coined in 1953 with the publication of Bowen’s Social Responsibility of Businessmen’ (Corporate Watch Report, 2006).  

In the 1960s and 1970s managers began implementing \textsc{\lc{CSR}} alongside traditional management functions. However, in the 1980s business and social interest grew closer and firms became more responsive to their stakeholders. New inclusions into annual reports evidenced that \textsc{\lc{CSR}} added value to a firm. In 1990 \textsc{\lc{CSR}} became standard in the industry with companies like Price Waterhouse Cooper and \textsc{\lc{KPMG}} becoming consultants. \textsc{\lc{CSR}} has now evolved beyond just code of conduct and reporting, creating initiatives in \textsc{\lc{NGO}}s, multi- stakeholder, and ethical trading. (Corporate Watch Report, 2006)\footnote{Essays, UK. (November 2018). Corporate Social Responsibility: History, Benefits and Types. Retrieved from }%\url{https://www.ukessays.com/essays/management/a-brief-history-of-corporatesocial-responsibility-management-essay.php?vref=1}}.  

By the late 1990s, \textsc{\lc{CSR}} became a part of boardroom discussions, with Shell Oil, the first company to implement \textsc{\lc{CSR}}, in 1998. (Corporate Watch Report, 2006)\cite{corporate_watch_2006}. With more generally well-informed citizens, social awareness was increasing. With tensions resulting in social unrest, \textsc{\lc{CSR}} potentially threatened firms if not managed properly. During the 1990s, \textsc{\lc{CSR}} became almost universally approved, and coupled with strategy literature. During the 2000s, \textsc{\lc{CSR}} became an important strategic issue, but today the \textsc{\lc{CSR}} has taken more of a backseat vis-à-vis \textsc{\lc{ESG}}.  

It is also important to note that, whilst \textsc{\lc{CSR}} was in essence the first introduction within firms of a structured social dimension impacting the firm, it has for many firms become more of a box-ticking exercise. This caused Unilever to introduce their ‘beyond \textsc{\lc{CSR}}’ program. This arrested development in social responsibility has many reasons, one being that \textsc{\lc{CSR}} has been driven mainly by commercial interests, and thus has never exited the narrow Western and Northern dimensions.  As such, as the early social drivers diminished, \textsc{\lc{CSR}} eventually detached itself from its roots never to be renewed in its originating social grounds.  

As history risks repeating, particularly when the paradigm that created the initial problem has not changed, implications for impact are important. As \textsc{\lc{CSR}} did not have a \textsc{\lc{GENE}} to continuously ground itself back into the roots of the social dimension, it died, which impact will repeat during the coming decades unless we work integrally and with the \textsc{\lc{GENE}}.  

Another reason for its demise is that, like impact, \textsc{\lc{CSR}} straddles the often-perceived conflict between financial return and social responsibility.  The spectrum of capital is mainly a construct by financiers to segregate and understand different buckets or risk/return. Most firms don’t necessarily care about what we call our ‘capital’ or product, as long as it fits with their needs and serves the purpose of financing the firm. However, if we are to influence the firms in which we invest for impact, this spectrum is mission critical for reasons previously discussed. Our impact approach must be framed accordingly, as this positions the nature and scope of our entry, and sets the tone for any ongoing relationship with a firm.  Such a spectrum can take the following shape of figure~\ref{part3_1-p1}  

\begin{figure}\centering
	\includegraphics[width=.95\linewidth]{part3_1-p1}
	\caption[Impact Investment Spectrum]{Impact Investment Spectrum\footnote{The Global Impact Investing Network (\textsc{\lc{GIIN}})\cite{impact_investment_nodate}}}\label{part3_1-p1}
\end{figure}

Following the drive over the past fifty years to develop full-cost accounting, out of which the recycling movement emerged, some key concepts were further developed which have also been historically foundational for Impact. First the triple-bottom-line (\textsc{\lc{TBL}}/\textsc{\lc{3BL}}), introduced by John Elkington\cite{elkington_enter_2004} in the mid-1990s, accounting for: 

\begin{itemize}
	\item People, the social equity bottom line 
\item Planet, the environmental bottom line 
\item Profit, the economic bottom line
\end{itemize} 

The \textsc{\lc{TBL}} has been widely explored and adopted. The Anglo-Dutch oil company Shell’s first incorporated it in their 1997 sustainability report. These pillars have been further developed: 

\begin{itemize}
	\item People can be viewed in three dimensions --- organisational needs, individual needs, and community issues.
	\item Profit is a function of both the sales stream, with a focus on customer service, and the adoption of a retention and new client strategy to reduce and replace churn and leavers. 
	\item The planet can be divided into a multitude of subdivisions, although reduce, reuse, and recycle is a start. 
\end{itemize} 

We then evolved into the Integrated Bottom Line (\textsc{\lc{IBL}}) which takes the accounting methodology further by suggesting that firms integrate their financial, economic, and social performance reporting into one integrated balance sheet, thereby providing a more holistic and ‘integral’ lens into the firm’s performance, enabling strategies for long-term value creation. A weakness is the assumed adoption of consensual accounting principles which has limited its methodology. As we will discover, accounting principles and standards are the Achilles heel of any Impact end process.

Here we need also to mention the important work of Jed Emerson who coined the term ‘blended value’ in which he makes it clear that: 

\begin{quote}
In truth, the core nature of investment and return is not a trade-off between social and financial interest but rather the pursuit of an embedded value proposition composed of both\cite[Page 37]{emerson_blended_nodate}
\end{quote}

His proposal comports with the integral approach, since blended value assumes that the nature of value is whole and indivisible; yet is fundamental natural drivers of financial, social, and environmental value. 

The value itself is then pursued, mined, and expressed through the different types of organisational forms, capital structures and investment instruments in which we seek to embed them. It is from this basis and foundation that we see the emergence of the \textsc{\lc{SDG}}s and today’s ‘impact’ investment frameworks.  

If we return to our basic premise that all investments are (and actually have) ‘impact’, and should therefore be viewed through the same basic lens, clearly impact needs to develop further to become the new global market convention that many now predict. This may be done by blending returns from multiple capital perspectives and sources, each investment firm creating their own formula seeking to attract outside capital, positioning themselves deliberately, with clarity and communication, by dialing up and down each dimension, which accounting methodology speaks to both the impact and return that comports with their vision and mission.

Also, creating social and environmental impact through more traditional investment vehicles has the potential to migrate and crowd-in significant amounts of capital to work towards creating positive impact, and thus increase the appeal to mainly philanthropically motivated investors.  
These aspirations and expectations are depicted in the following matrix, along the spectrum and dimensions of financial return and social impact. 

\begin{figure}\centering
	\includegraphics[width=.8\linewidth]{part3_1-p2}
	\caption{Capital Intentions}\label{part3_1-p2}
\end{figure}

Whilst traditional investments are typically made without regarding social impact, and philanthropic contributions are made without any real consideration of financial returns, impact investing incorporates both. This sometimes creates a schism equivalent to a psychosis; and as C.G. Jung once defined insanity as a ‘separation from self’ we need to integrate the splinter parts of ourselves. 

Don’t get me wrong, philanthropy has a significant role to play within the capital spectrum but should not substitute for alleviating a guilty conscience for those with ‘excess’ capital, made from the conventional economic and financial model of organisational management. As Aristotle understood: 

\begin{quote}
To give away money is an easy matter, and in any man’s power. But to decide to whom to give it, and how large and when, and for what purpose, and how, is neither in every man’s power, nor an easy matter. Hence it is that such excellence is rare, praiseworthy, and noble.\cite{effective_what_nodate-1}
\end{quote}
 
This reverts to the notion that if all investments are impact investments, and when looking through a lens such as I\textsu{3}, we also need to include industries that are considered ‘bad’ (e.g., oil and gas, mining, etc.), and how responsible investors can influence them to improve and develop more positively. Whilst sometimes the good obstruct the better, as investors we possess stakeholder influence to work effectively with companies across the good/bad spectrum. In addition, many financially motivated investors now see impact as a smart ‘lens’ through which to view their investments. Here are a few reasons: 

\begin{enumerate}
	\item Investors are connecting their financial portfolio and their philanthropic giving more as a system. For example, if we make a pure investment in fossil fuels, while simultaneously donating to environmental causes, it means that the capital in the financial portfolio may contravene the capital in the philanthropic allocation. 
	\item  More investors understand that it is `good business' for companies to incorporate positive social and environmental practises into their operations, and that they are more likely to reduce associated risks and create more value for investors over time. 
	\item Undoubtedly there are also some big financial opportunities connected to impact (e.g., in renewables). PWC’s megatrend analysis points to several areas, suggesting that health care, education, urbanization, the rise of emerging economies, and climate change will be amongst the biggest challenges and opportunities. 
	\item Last, but not least, as discussed, we are moving into an ‘integral age’ as more investors are aligning their financial portfolios with their clarified personal values.
\end{enumerate}

As these trends accelerate, the aim is for impact to continue to develop accounting systems that measure financial capital returns, and for other capitals such as social, natural, and cultural to become imperative. Unless achieved, at the collective level most of the current and traditional investment philosophy will continue to relate to pure and simple operational profit maximisation and continuation of the status quo, trying to maintain the industrial and investment landscape without attempting to change its models, structures or axioms. 

If financial capital is to assume its rightful, equal position in any organisational capital hierarchy, owners of such capital will need to value capital in other forms and understand that the purely rent-seeking approach must be complemented or the rent will be adjusted downwards over time. 

It seems that the core of the emergent impact investment philosophy stems from the awareness that we must collectively find a better approach that benefits both people and the planet. Our economy is to a large degree intrinsically destructive of everything non-financial and of investment in the future due to short-termism, which means that a large amount of global capital is diverted into speculation.  

At the individual level Impact investment, even in its current infancy, could become the catalyst to re-introduce a portion of the human spirit back into capitalism, thereby reestablishing a developmental and personal individuation process which is currently lacking in most organisations. If anything, the current operating model requires dis-individuation, a disconnection between task and true meaning and the individual. Also, individual investment thinking processes need to shift from conventional linear digitisation and abstract rationalisation toward systems-based thinking. Only thus can we re-build the lost linkages and connectivity which lie deep inside our consciousness and operational thinking.  

As former US President, Bill Clinton suggested: 

\begin{quote}
	If ordinary people don’t perceive that our grand ideas are working in their lives then they can’t develop the higher level of consciousness, to use a term that American philosopher Ken Wilber wrote a whole book about\ldots\, He said, ‘You know, the problem is the world needs to be more integrated, but it requires a consciousness that’s way up here, and an ability to see beyond the differences amongst us’\cite{who_wilber_who_2014}
\end{quote}

Firms with an impact methodology need to address and re-balance this aspect as part of the investment process, otherwise results will be devoid of the all-important ethic of the moral code and risk being executed without any real integrity for the ultimate outcomes. Impact can potentially turn the traditional transactional and resource-competitive relationship with an investee into more of a partnership. Therefore, firms seeking impact investment frameworks must recognise the psychological processes that are evoked, and the need for these processes to become sustainable in themselves.  

Whilst individual members of the investment community contemplate and deliberate this ‘new’ approach and how to position themselves on ‘the spectrum of capital’, it is also imperative to recognise that, from a historical and cultural perspective, impact investing is nothing new; rather, we are rediscovering some of the core elements of finance that were lost during the industrial revolution and over the past decades of asset monetisation and globalisation.  

In essence, impact investing will heal the wounded planet and its people. But as we know, healing requires the patient to come out of denial and into awareness and truth, which is extraordinarily difficult. As Naguib Mahfouz points out in his book Palace of Desire: 

\begin{quote}
The problem is not that the truth is harsh but that liberation from ignorance is as painful as being born. Run after truth until you’re breathless. Accept the pain involved in re-creating yourself afresh. These ideas will take a lifetime to comprehend, a hard one interspersed with drunken moments.	\cite{mahfuz_palace_2011}
\end{quote}

The question therefore, is through what lens are we looking and what map are we using to diagnose, understand and address the issues at hand? Another integral question is how we define and value ‘success’ in our organisations. 
