\chapter{SEKEM a case study}

The organisation \textsc{\lc{SEKEM}} (from the ancient Egyptian, meaning vitality from the sun) was founded in 1977 by the Egyptian pharmacologist and social entrepreneur Dr. Ibrahim Abouleish in order to bring about cultural renewal in Egypt on a sustainable basis. \textsc{\lc{SEKEM}} has been built on his foundational vision rooted in nature and community:
  
\begin{quote}
I carry a vision deep within myself: in the midst of sand and desert I see myself standing as a well drawing water. Carefully I plant trees, herbs and flowers and wet their roots with the precious drops. The cool well water attracts human beings and animals to refresh and quicken themselves. Trees give shade, the land turns green, fragrant flowers bloom, insects, birds and butterflies show their devotion to God, the creator, as if they were citing the first Sura of the Koran. The human, perceiving the hidden praise of God, care for and see all that is created as a reflection of paradise on earth. For me this idea of an oasis in the middle of a hostile environment is like an image of the resurrection at dawn, after a long journey through the nightly desert. I saw it in front of me like a model before the actual work in the desert started. And yet in reality I desired even more : I wanted the whole world to develop.	\cite{lessem_integral_2015}
\end{quote}

The \textsc{\lc{SEKEM}} Initiative\cite{sekem_sustainable_nodate} was founded with the vision of sustainable development and giving back to the community. A main goal is to ‘restore and maintain the vitality of the soil and food as well as the biodiversity of nature’ through sustainable, organic agriculture and to support social and cultural development. It aims to develop the individual, society, and the environment through a holistic approach which integrates ecology, economy, societal and cultural life. Located NorthEast of Cairo, the organisation now has several commercial enterprises and now includes:
 
\begin{itemize}
	\item a series of biodynamic farms; 
\item trading companies for produce and processed foods (Hator and Libra), herbal teas and beauty products (\textsc{\lc{ISIS}} Organic), medicinal herbs and medicines (\textsc{\lc{ATOS}} Pharma), and organic cotton products (NatureTex); 
\item a medical centre;
    \item a school based on the principles of Waldorf pedagogy open to pupils from any religious or ethnic background; 
\item a community school catering specifically to the needs of children from disadvantaged groups; 
\item a nursery 
\item a vocational training centre; 
\item a college (Mahad Adult Education Training Institute) and research centre (\textsc{\lc{SEKEM}} Academy for Applied Art and Sciences); 
\item Heliopolis University for Sustainable Development  
\end{itemize}

Here we can see that by taking the Arts centre inside the first realm, the interconnectedness of the four-worlds in terms of being, becoming, thinking and doing, reinforcing each other. 

\begin{figure}\centering
	\includegraphics[width=.8\linewidth]{part3_9-p1}
	\caption[Source Maximilian Abouleish]{Source Maximilian Abouleish \cite{abouleish_sekemsophia}}\label{part3_9-p1}
\end{figure}

As an integral enterprise, \textsc{\lc{SEKEM}} integrates the 4 elements from cultural, societal, economic and ecological life, and represents an internationally recognised role model for sustainable corporate development. 

From what was based initially on a social innovation, a central question for \textsc{\lc{SEKEM}} is how it can be functionally and structurally understood, designed and institutionalised in order to move towards the integral phase of the organisation as a living organism. 

We know that this transition requires that every part contributes intelligently to the overall organisation, serving the purpose of corporate sustainable development in its four independent, yet interrelated dimensions of societal, cultural, economic and ecological life. 

For \textsc{\lc{SEKEM}}, quite naturally rooted and grounded in Nature, they start in the integral South.  

\begin{figure}\centering
	\includegraphics[width=.8\linewidth]{part3_9-p2}
	\caption[Source Maximilian Abouleish]{Source Maximilian Abouleish (Ibid)}\label{part3_9-p2}
\end{figure}

\textsc{\lc{SEKEM}} is based on a renewed approach to Integral Human Development which incorporates individual storytelling (grounding), consciousness level and complexity handling capacity evaluation (emerging), competence level mapping (navigating) and self-management (effecting). 

The corresponding rhythm of \textsc{\lc{SEKEM}}’s Integral Organisational Development includes the dimensions of collective storytelling (grounding), arts and rituals (Cultura Activa) for stimulating consciousness development (emerging), knowledge creation via the integral Project Management framework (navigating), and the application of Holacracy as a complete system for self-organisation (effecting). 

All elements on different levels mutually reinforce each other and help \textsc{\lc{SEKEM}} to actualise its potential to become a fully functional Integral Enterprise. 

\begin{figure}\centering
	\includegraphics[width=.8\linewidth]{part3_9-p3}
	\caption[SEKEM as a Fourfold Commonwealth]{SEKEM as a Fourfold Commonwealth (Source: Adapted from Schieffer and Lessem \cite[p. 523]{schieffer_integral_2014})}\label{part3_9-p3}
\end{figure}

\textsc{\lc{Sekem}} has taken further development of the integral model to heart and has cocreated as suggested a unique version 3.0 which incorporates the wholeness of the \textsc{\lc{SEKEM}} ethos. The below image is the \textsc{\lc{SEKEM}} Sustainability Flower, a symbol for the underlying vision and approach to \textsc{\lc{SEKEM}}’s holistic development. Next to the inner social body with its three constituting elements we find ecology as the fourth surrounding element. The outer circle represents the cosmos as an omnipresent force that is highly relevant. 

The central perspective for \textsc{\lc{SEKEM}} is the ‘Economics of Love’ rendering our conventional, self-oriented and ego-centric concept of homo-economicus obsolete. Their underlying paradigm is based upon a living way of thinking ecologically, linked with an objective sense of economic community and the will to recognise social necessities. Such an organic character, which is expressed by \textsc{\lc{SEKEM}}’s inter-institutional ecology, is founded on inner organisational and personal integrity, its concern for humanitarian justice, and, above all, by its determination to realise the material existence of the whole social and ecological community. 

As a predominantly Muslim organisation, stewardship of the earth (khalifa), (South), consciousness development (ishtehad), (East), Justice (fadl), (North) and beauty 
(ihsan), (West) and are all core principles of Islam that Odeh Rashed Al-Jayyousi in his book 
‘Islam and Sustainable Development’ (2012) related to the concept of sustainable development, and hence in turn are related to \textsc{\lc{SEKEM}}’s Economics of Love. 

\begin{figure}\centering
	\includegraphics[width=.8\linewidth]{part3_9-p4}
	\caption[Source Maximilian Abouleish]{Source Maximilian Abouleish \cite{abouleish_sekemsophia}}\label{part3_9-p4}
\end{figure}

While \textsc{\lc{SEKEM}} is a fully commercial profit-making enterprise, it does not consider its prime aim profit maximisation. Through an integrated profit-sharing methodology, it shares its returns with the smallholder farmers in its supplier network called the Egyptian Biodynamic Association (\textsc{\lc{EBDA}}), which has around 600 small-scale farmers who represent SEKEM’s ecological branch. \textsc{\lc{SEKEM}} for Land Reclamation (\textsc{\lc{SLR}}), for farming, organic seed\-lings, fertilisation and pest control, also belongs to this agricultural sphere. It is important for \textsc{\lc{SEKEM}} to distinguish its sustainable agriculture business from its manufacturing and sales businesses, because the real added value is created in the 
Economic Life, which generates profit to be used in its broader agricultural cultural life. 

As an example, ten percent of \textsc{\lc{SEKEM}}’s profits go to the \textsc{\lc{SEKEM}} Development Foundation (\textsc{\lc{SDF}}), which represents the cultural branch of the initiative. It has launched many community development projects, including establishing schools and a medical centre, celebrating culture and diversity, and promoting peace, cooperation and understanding between all human beings. Since 2012, The Heliopolis University for Sustainable Development has become a leading player in cultural development under the umbrella of the \textsc{\lc{SEKEM}} initiative. 

Last but not least, the Cooperative for \textsc{\lc{SEKEM}} Employees (\textsc{\lc{CSE}}) represents the societal branch of the \textsc{\lc{SEKEM}} initiative, with its relationship to and between people respecting human rights. The huge national and international network of \textsc{\lc{SEKEM}}, such as the International Association for Partnership (\textsc{\lc{IAP}}), also belongs here as an institution of economic partners working together as pioneers in the organic movement. 

\textsc{\lc{SEKEM}} works with what they call ‘The Integral Project Management impact matrix’ (\textsc{\lc{IPM}}) which visualises the contribution of each project according to the four dimensions of the Sustainability Flower. It works as an assessment tool for each team and thus the actual contribution is evaluated at the end of a project. Project teams and their roles are also structured according to integral principles and natural allocation and selection.

\begin{figure}\centering
	\includegraphics[width=1\linewidth]{part3_9-p5}
	\caption[Source Maximilian Abouleish]{Source Maximilian Abouleish (Ibid)}\label{part3_9-p5}
\end{figure}  

The \textsc{\lc{IPM}} process describes (depicted below) a four-phase process that moves through the different phases in several rounds following the integral \textsc{\lc{GENE}}-rhythm. The different phases can be executed in parallel or sequentially depending on the type and needs of a project.  

\begin{figure}\centering
	\includegraphics[width=.8\linewidth]{part3_9-p6}
	\caption[Source Maximilian Abouleish]{Source Maximilian Abouleish (Ibid)}\label{part3_9-p6}
\end{figure}  

This is very similar to the I\textsu{3} version 3.0 from the organisational level. The indicators of the \textsc{\lc{IPM}} contribution questionnaire are collected from various sources that \textsc{\lc{SEKEM}} agrees are important to measure sustainable development. The questionnaire requests six to seven indicators for contributions in the four dimensions. Based on this matrix, each project group can develop a project-specific grant review beyond this general scheme. Initially, each project team ensures that the indicators and dimensions are individually adapted to the specific project.

\begin{figure}\centering
	\includegraphics[width=.8\linewidth]{part3_9-p7}
	\caption[The Integral Project Management impact matrix.]{The Integral Project Management impact matrix. Source Maximilian Abouleish (Ibid)}\label{part3_9-p7}
\end{figure}   

Once the project is completed, external beneficiaries can use the questionnaire as a tool to provide detailed comments on the results of the project. This is similar to having the I\textsu{3}OO as the driver of an iterative process that capitalises and resources each dimension. A specific \textsc{\lc{IMP}} project flower itself is depicted in figure~\ref{part3_9-p8}.

\begin{figure}\centering
	\includegraphics[width=.8\linewidth]{part3_9-p8}
	\caption[Source Maximilian Abouleish]{Source Maximilian Abouleish (Ibid)}\label{part3_9-p8}
\end{figure}  

Integral contains significant complexity. Maximilian Abouleish – Boes has produced a four-worlds map that illustrates the component parts of his own integral journey inside \textsc{\lc{SEKEM}} (figure~\ref{part3_9-p9}).

\begin{figure}\centering
	\includegraphics[width=1\linewidth]{part3_9-p9}
	\caption[\textsc{sekem}’s approach to Integral Human, Organisational and Community Development]{Overview of the function and structure of SEKEM’s approach to Integral Human, Organisational and Community Development. Source: Based on Schieffer and Lessem \cite{schieffer_integral_2014}, Lessem \cite{lessem_innovation_2017,lessem_awakening_2017,lessem2019idea}, and Lessem and Schieffer \cite{lessem_integral_2010}, adapted by Maximilian Abouleish – Boes}\label{part3_9-p9}
\end{figure}  

Having travelled through this third part of the book where we looked at ‘changing the firms in which we invest’ you may rightly wonder why we are still looking through the lens of the self. The answer may sound slightly simplistic and by now may sound obvious but nevertheless contains a profound truth. You can only change another or anything outside of yourself as a reflection of a change in yourself and your firm. We return to the adage of ‘be the change you want to see’ and ‘let it begin with me’. As an extension, we have created the conditions to influence the investee without voluntarily creating the Impact conditions as agreed through working through the Impact outcome objectives (e.g., I\textsu{3}OO). 