This directory is supposed to be the root directory for the LaTeX
files of the book. The toplevel directory (that is, the one above
this one) could be used for all the other required files that
are not TeX related.

We also plan on using this file to log important changes to the file
structure, with the format listed below. This way all changes are
clear and transparent, as well as the time and date of the commit.

Other changes that are smaller or specific to a part of the document
should be logged in the corresponding file of that part (e.g. if
something gets changed in Section 1 of Chapter 1 it should be stated
as a comment at the top of the .tex of section 1. Of course, once the 
work is done all these comments can be deleted from the files).

#######################################

03/02/2020 17:52:31 GTM-04. Intial commit. Added Bibliography-File.bib
and this readme file.

05/02/2020 08:28:51 GTM-04. Added the Main.tex file.

05/02/2020 20:46:47 GTM-04. Added Preamble.tex and sample.pdf. Check
the sample and let me know what you think.

07/02/2020 21:49:31 GTM-04. Sample moved to toplevel directory, it has
been review and there's no need for it in this folder.

08/02/2020 09:28:56 GTM-04. Updated sample.pdf, Bibliography-File and Main.tex

08/02/2020 17:08:01 GTM-04. Updated Main.tex and Preamble.tex. Added
subfolders for Chapter 1 and the frontmatters.

10/02/2020 14:43:08 GTM-04. Finished forewords.tex, halfway trough the
introduction

11/02/2020 21:07:04 GTM-04 Updated Main.tex, Preamble.tex, and the required
subfiles

12/02/2020 16:14:13 GTM-04 Created folder for pictures and populated it.
Finished 'Part 0'.

14/02/2020 20:13:58 GTM-04 Finished part 0. Added subfiles for part 1,
a quarter of part 1 done.

16/02/2020 19:24:44 GTM-04 More than half of part 1 done. Added some
pictures.

24/02/2020 13:02:44 GTM-04 Part 1 done. Almost done with the first 3 docx's
of part 2.

NOTE: It is becoming more difficult to keep the time stamps organized,
so it may be that some updates will be lacking one. In any case, I'll
keep updating this file as much as possible.

03/2020 Part 2 done, except for a single table that needs rewriting.
Commencing Part 3.

Still on Part 3, some of it has been done.

#######################################

Roberts-Book-LaTeX-Files/
├── Bibliography-File.bib
├── frontmatter
│   ├── About-Author
│   │   ├── about-the-author.aux
│   │   └── about-the-author.tex
│   ├── Dedication
│   │   ├── dedication.aux
│   │   └── dedication.tex
│   ├── Forewords
│   │   ├── forewords.aux
│   │   └── forewords.tex
│   ├── Introduction
│   │   ├── introduction.aux
│   │   ├── introduction.tex
│   │   ├── post-modern-econom.aux
│   │   ├── post-modern-econom.tex
│   │   ├── short-overview.aux
│   │   ├── short-overview.tex
│   │   ├── short-reminder.aux
│   │   ├── short-reminder.tex
│   │   ├── the-need.aux
│   │   ├── the-need.tex
│   │   ├── what-are-i3.aux
│   │   └── what-are-i3.tex
│   └── Quotes
│       ├── quotes.aux
│       └── quotes.tex
├── Main.pdf
├── Main.tex
├── part1
│   ├── all_idiots.aux
│   ├── all_idiots.tex
│   ├── areas_knowledge.aux
│   ├── areas_knowledge.tex
│   ├── basis_relationship.aux
│   ├── basis_relationship.tex
│   ├── digitalisation_minds.aux
│   ├── digitalisation_minds.tex
│   ├── five_relationship.aux
│   ├── five_relationship.tex
│   ├── impact_journey.aux
│   ├── impact_journey.tex
│   ├── nature_money.aux
│   ├── nature_money.tex
│   ├── ontology.aux
│   ├── ontology.tex
│   ├── organic_growth.aux
│   ├── organic_growth.tex
│   ├── redefining.aux
│   ├── redefining.tex
│   ├── value_questions.aux
│   ├── value_questions.tex
│   ├── ways_knowing.aux
│   └── ways_knowing.tex
├── part2
│   ├── enterprise_theory.aux
│   ├── enterprise_theory.tex
│   ├── family_office.aux
│   ├── family_office.tex
│   ├── from_cio.aux
│   ├── from_cio.tex
│   ├── integral_design.aux
│   ├── integral_design.tex
│   ├── intro.aux
│   ├── intro.tex
│   ├── intro_worlds.aux
│   ├── intro_worlds.tex
│   ├── ownership.aux
│   └── ownership.tex
├── part3
│   ├── background_impact_3.aux
│   ├── background_impact_3.tex
│   ├── deeper_culture.aux
│   ├── deeper_culture.tex
│   ├── digging_deeper.aux
│   ├── digging_deeper.tex
│   ├── from_irr.aux
│   ├── from_irr.tex
│   ├── impact_1.aux
│   ├── impact_1.tex
│   ├── impact_2.aux
│   ├── impact_2.tex
│   ├── impact_3.aux
│   ├── impact_3.tex
│   ├── integral_ownership.aux
│   ├── integral_ownership.tex
│   ├── sekem.aux
│   └── sekem.tex
├── pictures
│   ├── all_idiots-p1.jpg
│   ├── areas_knowledge-p1.jpg
│   ├── areas_knowledge-p2.jpg
│   ├── basis_relationship-p2.jpg
│   ├── basis_relationship-p3.png
│   ├── defining_success-p1.png
│   ├── digitalisation_minds-p1.jpg
│   ├── digitalisation_minds-p2.jpg
│   ├── digitalisation_minds-p3.png
│   ├── digitalisation_minds-p4.jpg
│   ├── digitalisation_minds-p5.jpg
│   ├── digitalisation_minds-p6.jpg
│   ├── nature_money-p1.jpg
│   ├── nature_money-p2.jpg
│   ├── ontology-p1.jpg
│   ├── p1-memenomics.jpg
│   ├── p2-lessem_schieffer(2).jpg
│   ├── p2-lessem_schieffer(3).jpg
│   ├── p2-lessem_schieffer.jpg
│   ├── p3-impact_journey(2).png
│   ├── p3-impact_journey.png
│   ├── part2_2-p1.jpg
│   ├── part2_4-p1.jpg
│   ├── part2_5-p10.jpg
│   ├── part2_5-p11.png
│   ├── part2_5-p12.jpg
│   ├── part2_5-p13.jpg
│   ├── part2_5-p14.jpg
│   ├── part2_5-p1.jpg
│   ├── part2_5-p2.jpg
│   ├── part2_5-p3.jpg
│   ├── part2_5-p4.jpg
│   ├── part2_5-p5.jpg
│   ├── part2_5-p6.jpg
│   ├── part2_5-p7.jpg
│   ├── part2_5-p8.png
│   ├── part2_5-p9.png
│   ├── part2_6-p10.jpg
│   ├── part2_6-p11.jpg
│   ├── part2_6-p12.jpg
│   ├── part2_6-p13.jpg
│   ├── part2_6-p14.jpg
│   ├── part2_6-p15.jpg
│   ├── part2_6-p16.jpg
│   ├── part2_6-p17.png
│   ├── part2_6-p1.jpg
│   ├── part2_6-p2.jpg
│   ├── part2_6-p3.png
│   ├── part2_6-p4.png
│   ├── part2_6-p5.png
│   ├── part2_6-p6.jpg
│   ├── part2_6-p7.jpg
│   ├── part2_6-p8.jpg
│   ├── part2_6-p9.jpg
│   ├── part2_7-p1.jpg
│   ├── part2_7-p2.jpg
│   ├── part2_7-p3.jpg
│   ├── part2_7-p4.jpg
│   ├── part2_intro-p1.png
│   ├── part3_1-p1.jpg
│   ├── part3_1-p2.png
│   ├── part3_2-p10.jpg
│   ├── part3_2-p11.jpg
│   ├── part3_2-p12.jpg
│   ├── part3_2-p13.jpg
│   ├── part3_2-p1.png
│   ├── part3_2-p2.png
│   ├── part3_2-p3.png
│   ├── part3_2-p4.jpg
│   ├── part3_2-p5.png
│   ├── part3_2-p6.png
│   ├── part3_2-p7.jpg
│   ├── part3_2-p8.jpg
│   ├── part3_2-p9.jpg
│   ├── part3_3-p1.jpg
│   ├── part3_3-p2.jpg
│   ├── part3_3-p3.jpg
│   ├── part3_3-p4.jpg
│   ├── part3_3-p5.png
│   ├── part3_3-p6.jpg
│   ├── part3_3-p7.jpg
│   ├── part3_4-p1.jpg
│   ├── part3_4-p2.jpg
│   ├── part3_4-p3.jpg
│   ├── part3_4-p4.jpg
│   ├── value_questions-p1.png
│   ├── ways_knowing-p1.png
│   ├── ways_knowing-p2.jpg
│   ├── ways_knowing-p3.png
│   ├── what_are_i3(2).jpg
│   └── what_are_i3.jpg
├── Preamble.tex
└── Readme&MainUpdates.txt

10 directories, 172 files
