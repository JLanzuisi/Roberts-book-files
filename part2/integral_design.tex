\chapter{Integral design}

There is no evolution without revolution, and designing for evolution doesn’t mean designing evolution. Natural design is a key component of the integral approach; as such, we need to look briefly at some of its key components.  

Since Charles Darwin published The Origin of Species, generations of evolutionary biologists have been refining, correcting and adding new layers of insight, drawing on a growing body of scientific knowledge. While evolution is generally understood as a movement from simple to more complex, understanding the process through which it occurs is the subject of theory, research and debate. 
 
A particularly relevant school of thought views cooperation (deriving from the mutuality of interest among organisms and ecosystems) rather than competition as evolution’s primary driver. For more than a century, natural selection has been conceptualised as the result of competition over scarce resources. This view dogmatically dominates much of finance. The idea of organisms battling one another for survival still holds sway in popular culture, but science tells us that this isn’t the whole story. 

According to Martin Nowak, Director of the Program for Evolutionary Dynamics at Harvard University:

\begin{quote}
Cooperation is needed for evolution to construct new levels of organisation. The emergence of genomes, cells, multi-cellular organisms, social insects and human society are all based on cooperation.\cite{nowak_five_2006}
\end{quote}

And Darwin himself wrote:

\begin{quote}
The most important of all causes of organic change is . . . the mutual relation of organism to organism—the improvement of one being entailing the improvement or the extermination of others(p.68)\cite{bergeron_animal_2011}
\end{quote}

Many interpretations of Darwin’s work have misleading emphasised extermination rather than improvement of species. Evolutionary biologist Elisabet Sahtouris has asserted that competition is a dominant phase of the juvenile stage in evolution, since a species needs to spread itself over enough territory to get enough resources to multiply. However, at some point as species start to bump into one another, cooperation takes over. She argues  that a tendency toward competition marks an immature level of biological development, occurring when a relatively new species strives to establish itself before it learns to form much more efficient cooperative alliances.3 

\begin{quote}
Young immature species are the ones that grab as much territory and resources as they can, multiplying as fast as they can.  
But the process of negotiations with other species matures them, thus maturing entire ecosystems. Rainforests that have evolved over millions of years are a good example. No species is in charge—the system’s leadership is distributed among all species, all knowing their part in the dance, all cooperating in mutual consistency.	\cite{mang_regenerative_2016}
\end{quote}

Sahtouris also observes that: 

\begin{quote}
Multi-celled creatures are relatively huge cooperative enterprises that could never have evolved if individual cells had been doomed to struggle in scarcity. \cite{sahtouris_earthdance:_2000}
\end{quote}

For her: 

\begin{quote}
The best life insurance for any species in an ecosystem is to contribute usefully to sustaining the lives of other species, a lesson we are only beginning to learn as humans.(Ibid)
\end{quote}

Herein lies the rub: the interdependency is absolute and it is only our lack of consciousness that tricks us into thinking otherwise. We have the false belief that knowledge is intelligence, and that intelligence is sufficient. This is why in the integral four worlds we need each touchstone to inform and navigate the next. Without these, knowledge and intelligence on their own (as we humans have experienced on many occasions) become the disease that kill the host.  

As the German spiritual teacher, Eckhart Tolle, tells us: 

\begin{quote}
In fact, if mental development and increased knowledge are not counterbalanced by a corresponding growth in consciousness, the potential for unhappiness and disaster is very great. (\ldots) The crowning glory of human development rests not in our ability to reason and think, though this is what distinguishes us from animals. Intellect, like instinct, is merely a point along the way. Our ultimate destiny is to re-connect with our essential Being and express from our extraordinary, divine reality in the ordinary physical world, moment-bymoment. Easy to say, yet rare are those who have attained the further reaches of human development.	\cite{tolle_power_2004}
\end{quote} 

Hence our rooting and grounding in the integral South, the home of Being. Evolution, as such, is not guaranteed nor a given. Again, in Sahtouris’s words: 

\begin{quote}
The evolutionary process is an awesome improvisational dance that weaves individual, communal, ecosystemic and planetary interests into a harmonious whole.\cite{mang_regenerative_2016}
\end{quote}

At our stage of human development, we have managed to move this notion of evolution into creative competition in sport, art, design, talent, cooking and other areas. Our competitive roots have evolved into drivers for excellence, e.g., the best behaviour or best product. ‘Best’ is seen as a function of our culture.
  
The implication for impact investing is profound. As we begin to epistemically weave the integral four worlds together, an evolutionary process is being activated by design which cannot be ignored nor pushed aside. This is why the integral design for any organisation is paramount if the pieces are to hold together, and for the sum of the parts to become greater and more transformational than the independent pieces. This is also why we need to travel through the 4Ms and ask (and answer) the ‘Why question’ before we quest to model and create the structures to hold and develop each component. 

One of the practical applications is to look at conventional value chains as virtuous circles. In a process flow format, the familiar corporate value chain highlights where value is added to a set of activities beginning with basic raw material coming from suppliers, moving on to a series of value-added activities involved in producing and marketing a product or service, and ending with distributors getting the final goods into the hands of the ultimate customer. This chain tends to have a beginning and an end, thereby disconnecting and fragmenting any possible integral leverage, unlike a fly-wheel where momentum requires less energy to accelerate as each part assists and supports the next. 

\section{Causal models build virtuous circles  }

It is necessary to establish a causal model, or fly-wheel, of the plan for any system that highlights the drivers of strategic success. In a financial services company, such a model might take the following circular shape, which highlights the learning and self-leveraging effects of the system. 
\clearpage
\begin{scriptsize}
	\begin{longtable}[c]{R{10em} L{10em}}
		\caption{Cycle}\label{cycle}\\
		\toprule
		Selection \& 
		Staffing & New Hires, 
		Education, 
		Work Experience \\
		\addlinespace[4pt]
		Employee 
		Satisfaction & Supervision, 
		Support \& Fairness,
		Compensation \\
		\addlinespace[4pt]
		Employee Added  
		Value & Empowerment, 
		Accountability, 
		Investor Credibility\\
		\addlinespace[4pt]
		Customer 
		Satisfaction & Advice \& Ideas, 
		Execution,
		Price/Value\\
		\addlinespace[4pt]
		Investor Transaction 
		Behaviour & Transaction Frequency,
		\% of Wallet, 
		Margin vs. Risk\\
		\addlinespace[4pt]
		Profitability \&  
		Sustained 
		Revenue & Wallet Migration, Margin Retention,
		Client Survey\\
		\bottomrule
	\end{longtable}
\end{scriptsize}

Here we see that each component feeds the next; thus, becoming necessary to reinforce the prior area so that momentum and harmony can be built up. This, as we know, is rarely seen in organisations where fragmentation and operational silos often prevail due to politics and the ubiquitous power complexes. Our impact design must be based on causal models with no loose ends where things just stop and energy dies.  We now have sufficient basis to look into the integral four-worlds more fully. 

\begin{figure}\centering
	\includegraphics[width=1\linewidth]{part2_4-p1}
	\caption{Virtuous circles  }\label{part2_4-p1}
\end{figure}