\chapter[Ownership and governance]{Ownership and governance as the keystone to culture}

For the purposes of this book, the concept of ownership may extend from equity into other structural forms of capital participation that afford similar privileges. Here we will explore some of the more philosophical aspects of what constitutes ownership, stewardship and governance, and their relation with culture.
  
Ownership, like money, can be completely neutral, e.g., a percentage in a share register which may require no involvement by the investor. However, there comes a point where it is more than just a number and therefore some form of relationship is formed.  

No other aspect of an investment relates more to the concept of the power complex than ownership. Ownership is the foundation of  the nature of our participation and relationship with an investee, as this is where not only our formal and legal agreements are constructed but also informal ones, such as the shape and form of how we see and position ‘the other’ in our affairs.  

Whilst it is easy to grant upfront voting rights and other forms of board participation to gain influence and control, it is only through forming a relationship with our stakeholders that any of our differences can surface. It is in the grounding of the relationship and the quality of our dialectic that any such differences will be resolved without creating conflict and without either participant activating a power complex.
  
Any board or organisational executive committee is bound by mores, rules and requirements for its effective functioning; it is also a social body that needs to develop its own modes of operation. In my experience, boards tend to gravitate in language and thinking towards whoever has power or can dominate the discourse. This is usually how we get listened to. Providing contrast, valuable as this may be, creates dissonance and thus minimised in importance (or rejected outright) to maintain a false sense of group harmony. 
 
Eckhart Tolle tells us that: ‘Power over others is weakness disguised as strength\cite{tolle_power_2004}.’ Throughout my own career, ownership and participation have been a source of attempts of manipulation by smart people who control the levers of equity. Their rationalization, after events unfold, as to why they cannot deliver on previous discussions or commitments fascinate. 

As if by magic, as soon as something has value, history gets reinterpreted; attributable values are re-assigned to those who control the power complex.  

I have personally been involved in several business where much was promised and little delivered by the owners, to the long-term detriment of the firm. In my view, any start-up or early-growth firm needs to have an initial, binding contractual commitment from the initial owners to deliver on shared ownership promises. 

As someone who firmly believes in the ethics and commercial rationale for equity participation across a broad spectrum of a firm’s employee pool, it pains me when this aspect is not considered or delivered reluctantly by incumbent owners. 

Whilst governance is often an extension of ownership, the closer the power distance or reliance on conventional hierarchical structures, the more likely that its governance methods become a result (and reflection) of the same ownership, power and influence structures. 

This leads to the need to consider the governance of a firm as a separate requirement dependent on life-cycle business needs and the necessary stakeholder participation in its governance and social design. Whilst governance has considerable legal and compliance requirements and responsibilities, its effective co-creative function is driven by the level of relationship its members can develop. 

Few boards operate as democracies; whilst there are some extremely important and valid reasons why this may not be appropriate (e.g., in relation to equity capital at risk), firms become more vulnerable through the concentration of power within their control functions. Also, there is a dynamic between the collective and individual intellect that we need to be aware of: 

\begin{quote}
A group experience takes place on a lower level of consciousness than the experience of an individual. This is due to the fact that, when many people gather together to share one common emotion, the total psyche emerging from the group is below the level of the individual psyche. 

If it is a very large group, the collective psyche will be more like the psyche of an animal, which is the reason why the ethical attitude of large organisations is always doubtful. The psychology of a large crowd inevitably sinks to the level of mob psychology. 

If, therefore, I have a so-called collective experience as a member of a group, it takes place on a lower level of consciousness than if I had the experience by myself alone.	
\end{quote}

Many private firms are well-run under strong and concentrated leadership. However, vulnerability comes via group-think, lack of broader and inclusive perspectives, and vital intelligence from others. 

This is always a compromise so should be part of a firm’s design, not just from the accidental outcome from how ownership was created. In the areas of functionality and a firm’s culture, this design is mission critical.
  
We have investigated the Four Rs of Relationships in detail, and we explore them from the governance perspective: 

\begin{enumerate}
	\item \emph{Rooting (South):} How are we connected and in what ways do we belong together? What are our common values for respect, commitment etc.? Are there ways in which we establish our common callings, grounding and world views?    
\item \emph{Resonance (East):} In our communication and ways of being, how do we compliment, expand and enhance each other to make any weaknesses less relevant? Can we openly share, and do we understand each other’s values and belief systems? Why are we together within this context? 
\item \emph{Relevance (North):} How do we operate as a group? Are we co-creating the future and, if so, is this balanced? Are roles clear and lines of authority respected? How are opinions heard and made relevant?  
\item \emph{Rationale (West):} What are the key contributing factors that make our Impact approach work, and are they all present and correct? Are the executive capabilities operational and fully aligned with vision and mission?  
\end{enumerate} 

These are just some examples of questions one can ask in trying to understand the governance culture and Impact dialectic in which all things come together. Can we bring the language of the heart, and impact, into these conversations or will they be ignored, rejected or ridiculed? Can we live by what Emily Dickinson elegantly offers about having our words living on through Impact: ‘A word is dead when it is said, some say. I say it just begins to live that day\footnote{\cite{dickinson_complete_1997}.  A word is dead when it is said.}.’

As we weave impact outcomes into the fabric of our investments, can we have conversations that expand the impact lens and its objectives? We have built the loom on which this fabric will be woven within our governance functions. Will this loom hold under tension? Is it robust enough to hold ambiguity, competing claims and conflicts? Such tensions are the natural consequences of a healthy environment.  It is when things become straightforward and lack a vibrant dialectic that we should worry because this may indicate conformity, fear, and the power complex rearing its head.  

Here we again need to look differently at the political dynamics between larger and smaller organisations and how these affect the possibility of integral impact design. In my own experience when working for large financial institutions, politics was often the modus operandi. Unless we actively participated in these politics and were good at managing the management hierarchy, we were destined to limit our career prospects.

In his 2009 West Point Academy Leadership address, William Deresiewicz said%\footnote{\url{http://www.univforum.org/sites/default/files/789_Deresiewicz_End_of_solitude_ENG_0.pdf}}:  

\begin{quote}
I’m sorry to say this, but like so many people you will meet as you negotiate the bureaucracy of the Army or for that matter of whatever institution you end up giving your talents to after the Army, whether it is Microsoft or the World Bank or whatever—the head of my department had no genius for organising or initiative or even order, no particular learning or intelligence, no distinguishing characteristics at all. Just the ability to keep the routine going, and beyond that, as Marlow says, her position had come to her—why? That’s really the great mystery about bureaucracies. Why is it so often that the best people are stuck in the middle and the people who are running things—the leaders—are the mediocrities? Because excellence isn’t usually what gets you up the greasy pole. What gets you up is a talent for maneuvering. Kissing up to the people above you, kicking down to the people below you.
\end{quote}  

This is another reason why real impact investment is more likely to emanate from smaller firms and family offices where politics do not prevail so extensively. Larger firms often suffer from inertia as their strong processes, capacities and resources have been firmly embedded into their culture and leadership so it will take a greater effort and understanding for any change to be actualised. Deresiewicz makes another important point when he links the cognitive thinking capacities of leaders to the lack of creative vision in many areas of organisational life. In essence, connecting the gap between the Why? and How?: 

\begin{quote}
We have a crisis of leadership in America because our overwhelming power and wealth, earned under earlier generations of leaders, made us complacent, and for too long we have been training leaders who only know how to keep the routine going. Who can answer questions, but don’t know how to ask them. Who can fulfil goals, but don’t know how to set them. Who think about how to get things done, but not whether they’re worth doing in the first place. What we have now are the greatest technocrats the world has ever seen, people who have been trained to be incredibly good at one specific thing, but who have no interest in anything beyond their area of expertise. What we don’t have are leaders. What we don’t have, in other words, are thinkers. People who can think for themselves. People who can formulate a new direction: for the country, for a corporation or a college, for the Army—a new way of doing things, a new way of looking at things. People, in other words, with vision.	\cite{hansen_leadership_2012}
\end{quote}

Surely when we design impact-orientated organisations, we aim higher. We seek to improve ownership structures and governance so they are not just a conventional iven, but an outcome, fully informed and synthesised with the culture, stakeholders, and impact outcome objectives.  

Let us look briefly at some  innovative ways of reinventing ownership. One model at the forefront is the concept of ‘FairShares’ which incorporates six different definitions of ‘wealth’\cite{fairshares_model_nodate}. 
 
\begin{figure}\centering
	\includegraphics[width=.8\linewidth]{part2_2-p1}
	\caption{FairShares}\label{part2_2-p1}
\end{figure}

Similar to I\textsu{3}, the firm needs to travel through six stages of navigation to understand the best fit in terms of outcomes. The FairShares Model is implemented through: 

\begin{enumerate}
	\item  Five values and principles.  
\item Six key questions.  
\item Five learning and development methods.  
\item Four legal identities.  
\item Seven ICT support platforms. 
\item Six forms of wealth contribution.\footnote{For a full description of each, see \cite{fairshares_model_nodate}}
\end{enumerate}

The main point here is that to build resilience and sustainability, the firm needs to become more adaptive and align with its real impact stakeholders. These initiatives, if adopted, should contain the full suite of impact objectives in which ownership and wealth recognition/distribution are key components in building an impactful businesses.  

Another important concept is ‘Equity for Good’ (\textsc{\lc{EFG}}), which originates from the social-enterprise sphere and is based on recent, positive developments in how businesses treat their employees (e.g., ‘Investor in People’); how businesses manage their transparent supply-chain with suppliers (e.g., ‘fair trade’ or ‘anti-slavery’), or what a business chooses to do with its profits (e.g., ‘1\% for the planet’).  

The \textsc{\lc{EFG}} approach gives businesses and investors the chance to clarify their mission and publicly demarcate how created shareholder value will improve practises in the economy. The aim is to provide transparency for social-enterprise stakeholders, reassurance for customers, and brand value for mission-driven companies and investors.  

The \textsc{\lc{EFG}} investment model is rooted in a legally binding pledge made by impact investors. The pledger agrees to reinvest (or donate) a set percentage of the value of any net capital gain in a social-impact investment through any combination of the following: 

\begin{itemize}
	\item  Reinvesting into another social enterprise or social impact investment fund. 
\item Gifting to a registered charity of our choice. 
\item Gifting to a not-for-profit that is not registered as a charity.
\end{itemize} 

The \textsc{\lc{EFG}} concept defines ‘net capital gain’ as the money made in addition to the initial sum invested, net of tax and a nominal inflationary return on the original investment (set at \textsc{\lc{UK GBP}} annual \textsc{\lc{LIBOR}} rate + 2\% per annum). In this way, the \textsc{\lc{EFG}} approach does not cap a return per se, but ensures that a clear proportion of any ‘excess’ return is ploughed back into defined areas of ‘doing good’ as one sees fit.   

In addition, alternative initiatives such as ‘B Corp’ provides a holistic certification that a business has met minimum standards in its relationship with workers, customers, suppliers, the community and the environment. 

This concept of reciprocity also applies to our personal finances. Many people may not have a retirement fund so their family and community becomes a de facto relational insurance. With wealth, we often distance and separate ourselves from such reliance.  If our very lives depend on it, we are more likely to honour and cherish our nearest and dearest, treating them with care and respect, since we not only need these relationships, we have also have built up reciprocal stores of relational capital in combination with our other possible forms of capital. 
   
In her book\cite{twist_soul_2003}, Lynne Twist points out that modern science is discovering a similar truth in nature, 

\begin{quote}
Contrary to those models of Nature as innately, intensely, and almost exclusively competitive, more recent scientific study has illuminated the powerful role of mutuality, synergy, coexistence, and cooperation in the natural world\ldots The idea that scarcity and competition are just the way it is is no longer even viable science. Respected evolutionary biologist Elisabet Sahtouris notes that Nature fosters collaboration and reciprocity. Competition in Nature exists, she says, but it has limits, and the true law of survival is ultimately cooperation\ldots Sahtouris and others note that contrary to the competitive theme that “survival of the fittest” connotes, a more accurate description would be “survival of the cooperative and collaborative.”
\end{quote}

\section{Some early personal experiences}

After a somewhat ‘classical’ background of having studied Economics in Stockholm, followed by Business at the London School of Foreign Trade, I joined the investment banking profession with Paribas Capital Markets. My own style of learning was by ‘doing’; more specifically, I knew that by integrating doing with learning, I accelerated the doing and better embedded the learning. This often confused prospective employers, who seldom look beyond specified degrees and prerequisite knowledge that they believe are key components for a particular job.  

After a few years in the industry, I realised that I lacked the technical insights of my investment management clients so I enrolled in the London Business School’s three-year Corporate Finance and Investment Management evening programme, which had the same curriculum as their MSc in Finance. Whilst this course gave me a deeper appreciation and a technical background to finance, I became aware of its limitations, and that its paradigms resided within the neoclassical ‘Chicago’ style of thinking and doing.  This paradigm was mechanistic and disconnected, ignoring my belief that every human and their creations must function within a living and interconnected system, whether it is a cottage garden powered by soil and sun or a company powered by employees.  

Living systems (sometimes called complex adaptive systems) are ubiquitous: 
hospitals, the human body, the stock market, estuaries, neighbourhoods. Although subject to the law of entropy, living systems are also governed by the countervailing processes of evolution. They don’t just run down, they also somehow grow up. 

For this reason, one of the basic premises of regenerative development is that every living system has the inherent possibility to move to new levels of order, differentiation, and organisation. This capacity to create increased order is the opposite of entropy and, in my view, is one of the core components of integral impact investing. 

Ben Okri encourages us to see impact as a major part of our legacy and our gifts to our families and mankind:  
And just as astonishing is the knowledge That we are, more or less, The makers of the future.

\begin{quote}\raggedright\itshape
And just as astonishing is the\\
knowledge That we are, more or less,\\
The makers of the future. \\
We create what time will frame.\\ 
And a beautiful dream, shaped \\
And realised by a beautiful mind,\\ 
Is one of the greatest gifts\\
We can make to our fellow beings. 	\cite{okri_mental_2000}
\end{quote} 

When working in banking and needing to conform to its culture in order to remain employed and also advance, it became obvious that there was some personal dissonance and differing views around the core operating principles of the industry. I would even argue that a person with strong ethics and morals was incompatible with building a successful banking career. All too often the least trustworthy people, who were personally connected and knew how to manage the firm’s politics, rose in the firm’s hierarchy.  

With my own values and beliefs about how business could be better conducted now being challenged, I became someone who could help to repair such differences. One such driver, as discussed previously, was the universal need to conform to the ‘success principles’ for recognition and advancement. At the organisational level, this becomes a self-selective, iterative, and recursive process.  

Core operating principles, in my experience, were based mainly on transactional quantitative measures relating to money, power, and prestige. This was something we had to live with; given the prevailing culture, there was little alternative but to conform fully, since this was all management could see and value.  
This experience led me to explore what I call the ‘power distance’ between cause and effect. This contrasts what the social psychologist Geert Hofstede terms ‘power distance’, which is a function of how people belonging to a specific culture view a power relationship, e.g., superior-subordinate relationships, authority, and collaborative participation.  

In my work for the Client Solution Group at Fortis, my quest to connect disconnected parts of a business became a key area for my own growth. In my view, moving from our previous Power Distance 1.0, centred on overt power such as position, status and other hierarchies (and including physical distance). A possible shift to Power Distance 2.0 will include a psychodynamic, connective, and spiritual developmental aspect that both includes and transcends the traditional moral development axis. In the banking industry, I witnessed our ability to disconnect from outcomes and not take responsibility for them.  

Similar phenomena have been researched in other areas, such as the military, where following orders freed individuals from guilt and shame, and liberty of consciousness. This confirms the link between our levels of consciousness and how a prevailing culture can influence human behaviour at the cost of awareness and responsibility.  

This link was recognised as an element of the banking crises; I am very interested to see how these power distances can be shortened in the integral economy and, if possible, made whole again. In impact investing, we must be acutely of this power disconnect and work to eliminate it. When designing outcome objectives with organisations, we need to embed and connect fully the individual deliverables with responsibilities. At the same time, we need to take into account the serendipity of life and what C.G. Jung called ‘synchronicity of life’, which at times creates opportunity and adversity often in unequal measures, providing us with our ‘fork in the road’ choices.  

As Alain de Botton reminds us: ‘We should not feel embarrassed by our difficulties, only by our failure to grow anything beautiful from them\cite{quote_alain_nodate}.’

Let us now turn return to the integral approach to see how it should be influenced by natural designs and solutions.   