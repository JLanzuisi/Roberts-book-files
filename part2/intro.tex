\chapter{Introduction}

Our economic system is reaching its limits to perform the basic functions of creating wealth, stability, and well-being. Much has been written from ecological, socio-economic, and post-financial crises perspectives, strongly suggesting that the current paradigms need radical reconstruction.  

As discussed previously, our impact map must include our own perspectives, and to every extent possible those of our investees. The latter being a function of the nature of ourselves and our investment objectives and our capacity to create an investee relationship in which our impact approach can be implemented.  

Here we focus on a full engagement and influence model which we may find in private equity or private debt, in order to focus on our now expanded perspectives without limitations. Whilst we can have the best impact intentions on the planet, they will be impotent unless we can create behavioural and strategic change within the firm(s) in which we invest. This means looking at our investee through our own lens and within a broader economic system. There is a saying that ‘a truth that’s told with bad intent is worse than any lie you can invent.’ Likewise, a well-intended Impact objective based on false intentionality can create worse outcomes than what initially would appear. 

There have been many innovations in post-neoclassical economics, such as evolutionary game theory, complexity economics, behavioural economics, neuroeconomics, and experimental economics. However, these cannot provide the integrality of a full-spectrum approach. In addition, behavioural economics is probably the only genre that includes the psychological dimension in any meaningful way, but current research and writings mainly explain market movements and their decisions, with relatively limited value for our Impact approach.  

The integral four-world theory and praxis has four main research paths: relational (Southern); renewal (Eastern); reason (Northern), and realisation (Western); and on our four-world transformative trajectory: from grounding (origination) and emergence (foundation) to navigation (emancipation) and effect (transformation) (our \textsc{\lc{GENE}}). So our approach is local–global, not just global–local.  

For the development of I\textsu{3}, the implications are profound. As discussed in Part 1, for the impact industry in general, we can no longer look at these component parts in isolation. We need to create frameworks with a deliberate connection and integrally informed outcomes. In essence, we need to take what we call Integral \textsc{\lc{CARE}}: another dynamic that ties together the tasks in each world to build our virtuous circle of deepening understanding:
 
C: Community and Collective Calling and Activation;  

A: Activating Awakening and Awareness / Consciousness;   
    
R: Research driven Innovation and Realised institutionalization;  

E: Embodied and Transformative Education and Evolving Enterprise.

\begin{figure}\centering
	\includegraphics[width=.8\linewidth]{part2_intro-p1}
	\caption{Care}\label{part2_intro-p1}
\end{figure} 

The banking and finance industry not only represents the core mechanism for the transfer, accumulation, and accretion of capital, which corresponds with the relation between stakeholders, but they are also the specific objects of cultural critique serving as society’s custodians for the role of capital in society and business inside our whole historical and social context---a key issue for critical theory. Chomsky said,

\begin{quote}
Before the 1970s, banks were banks. They did what banks were supposed to do in a state capitalist economy: they took unused funds from your bank account, for example, and transferred them to some potentially useful purpose like helping a family buy a home or send a kid to college (p.38).	\cite{chomsky_occupy_2012}
\end{quote}

In the information age, with increased pluralism, flexibility, risk, and uncertainty, positions taken by traditional authorities within the finance industry are being challenged by diverse voices and disruptive technologies. Just like in other areas for example, people living  together in the same home are increasingly challenging and negotiating their relationships, rather than accepting traditionally mandated roles. 

In my own experience, finance was dominated by money and power dictating the entire organisation. Money (and attributable value) was tracked and measured as the life blood of the firm. Other economic systems, (for example, Islamic finance which sees interest as usury, as did Christians, along with the periodic forgiveness of debt through the concept of the jubilee), see the firm’s quintessence in the exchange of value and the commercial transaction rather than its lifeless derivative, money.  

I\textsu{3} re-establishes the lost connections for a firm between its integrative parts, making them visible and manageable as part of the firm’s re-created self. Unlike banking, the investment industry has a unique educational opportunity to reconnect role in work with the work of soul, and find ways to shorten the power distance between money and impact. In addition, much wealth today is accumulated and held by individuals who have varying degrees of consciousness about the nature of money. 

In Pedagogy of the Oppressed\cite{freire_pedagogy_1996}, Freire introduced a radical distinction that has since become an enduring feature of progressive educational thought: the difference between ‘banking’ and ‘problem-posing’ education: 

\begin{quote}
[Banking education] attempts, by mythicizing reality, to conceal certain facts which explain the way men [sic] exist in the world\ldots Banking education resists dialogue; problem-posing education regards dialogue as indispensable to the act of cognition which unveils reality. Banking education treats students as objects of assistance; problem-posing education makes them critical thinkers\ldots Problem-posing education bases itself on creativity and stimulates true reflection and action upon reality, thereby responding to the vocation of men as beings who are authentic only when engaged in inquiry and creative transformation.
\end{quote}

In this sense, I\textsu{3} is the dialectic teacher guiding the ‘student’ (i.e., the firm) through questioning and creating new levels of cognition about their role in life and society. This opens the possibility of the firm becoming the ‘teacher’ towards its environment and relationships.  

 \begin{quote}
 Teachers and students (leadership and people), co-intent on reality, are both Subjects, not only in the task of unveiling that reality, and thereby coming to know it critically, but in the task of re-creating that knowledge. As they attain this knowledge of reality through common reflection and action, they discover themselves as its permanent re-creators.(Ibidem)
 \end{quote}
  
Traditionally, finance education is a relationship of domination in which the teacher has knowledge that s/he deposits in the heads of passive objects –her/his students. Such education maintains students’ immersion in a culture of silence and positions them as objects outside of history, control and agency,   

\begin{quote}
It is not surprising that the banking concept of education regards men as adaptable, manageable beings. The more students work at storing the deposits entrusted to them, the less they develop the critical consciousness which would result from their intervention in the world as transformers of that world. The more completely they accept the passive role imposed on them, the more they tend simply to adapt to the world as it is and to the fragmented view of reality deposited in them.

The capability of banking education to minimise or annul the student's creative power and to stimulate their credulity serves the interests of the oppressors, who care neither to have the world revealed nor to see it transformed. The oppressors use their 
"humanitarianism" to preserve a profitable situation. Thus they react almost instinctively against any experiment in education which stimulates the critical faculties and is not content with a partial view of reality always seeks out the ties which link one point to another and one problem to another.	(Ibidem)
\end{quote}

One of my objectives in writing this book is to provide, an I³ blueprint for each investment institution for their own ‘problem-posing’ questions, namely: 

\begin{itemize}
	\item  What kind of firm are we?
\item  How do we want to invest our capital? and 
\item What sort and level of impact are we looking to achieve?  
\end{itemize}

These questions are mission critical for each investment firm. Asking (and by extension answering) these questions will provide the basis of their identity, and will determine their target market for attracting funding. In my own experience, it became palpably abundantly clear whilst I was working in large financial firms that I was, in effect, prostituting myself into a social system that was existentially toxic, based on manipulative and false premises. These were led by egocentric and narcissistic ‘leaders’. As such, there were no real emancipatory processes in place; rather, most forms of insight into possible positive change were often discarded or disparaged.  

\epigraph{The most potent weapon of the oppressor is the mind of the oppressed}{Steven Biko, (1978)}

The recipe for success, as defined by the culture, requires surface-orien\-ta\-ted measuring sticks to be obtainable so that success can easily be transferred to another institution, since longevity and trust does not exist between the parties. With I³, the reconstruction can start with alternative definitions of success.  

As Parker J. Palmer writes:  

\begin{quote}
In the face of resistance, an ungrounded leader will revert to bureaucratic mode: the teacher will revert to lecturing rather than inviting inquiry, the manager will revert to rule-making rather than inviting creativity. In the face of resistance, leaders will do what they are taught to do: not create space for others, but fill the space themselves—fill it with their own words, their own skills, their own deeds, their own egos. This, of course, is precisely what followers expect from leaders, and that expectation prolongs the period during which leaders of community must hold the space—hold it in trust until people trust the leader, and themselves, enough to enter in.\footnote{ Palmer, P.J. (1998). \emph{Thirteen Ways of Looking at Community}. The Inner Edge.  p.6}
\end{quote} 

As an example, some of the changes in our social impact thinking must include the following:

\begin{scriptsize}
	\begin{longtable}[c]{R{.3\linewidth} L{.3\linewidth}}
		\caption{Changes in our social impact thinking}\label{changes_in_our_social_impact_thinking}\\
		\toprule
		Old Thinking & New Thinking \\
		\midrule
		Community is a goal. & Community is a gift.\\
		\addlinespace[4pt]
		We achieve community through desire, design and determination. & We receive community by cultivating a capacity for connectedness. \\
		\addlinespace[4pt]
		Community requires a feeling of Intimacy. & Community does not depend on intimacy and must expand to embrace strangers, even enemies, as well as friends.\\
		\addlinespace[4pt]
		Community is a romantic Garden of Eden. & Community that can withstand hard times and conflict can help us become not just happy but ‘at home.’\\
		\addlinespace[4pt]
		Leadership is not needed in Communities. & Leadership and the authority to lead toward community can emerge from anyone in an organisation.\\
		\addlinespace[4pt]
		Suffering is bad and should be avoided. & Suffering lets our ‘hearts break open’ enough to hold both vision of hope and the reality of resistance without tightening like a fist.\\
		\bottomrule
	\end{longtable}
\end{scriptsize}

As we know, our own leadership and courage are the only shift changers. I agree with the U.S. Marine Corps Definition of Leadership as ‘The qualities of moral character that enable a person to inspire and influence a group of people successfully’  

However, leading in Impact is much more than that; Impact leadership involves at its core the ability to transform self and others. It means being an awakener, a liberator, bringing new awareness and discoveries through study and experience with a passion for seeing people grow. Impact leadership cannot just be defined by position or authority; it’s a gift granted to you by others as a way of being, becoming, knowing, and doing by its very practise.  

\epigraph{To lead is to learn, to awaken, to grow and to liberate: to honour the past, to dream of the future, to live in the present.}{Colin Reeve}

Having set the scene for our own organisational impact change, let’s investigate what this means specifically for our ‘family office’ and creating the impact frame. 