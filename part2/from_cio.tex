\chapter[New role and meaning of the Chief Impact Officer]{From CIO to CIO, the new role and meaning of the Chief Impact Officer}

\epigraph{A radical inner transformation and rise to a new level of consciousness might be the only real hope we have in the current global crisis brought on by the dominance of the Western mechanistic paradigm.}{Stanislav Grof\cite{challe_manfate_2011}}

Leadership development is psychologically similar to parenting. The neurological set-up and construct of a leader contain the same seeds from our primordial relationships as in other areas of life. Depending on how these primordial relationships were installed and now held, we will have some surplus and deficiencies in the 5 As, (attention, acceptance, appreciation, affection and allowing) which we explored earlier.  

It is quite possible that the cultural leadership norm in finance is based on an egocentric approach to ensuring one’s perceived needs are met first (through money, property and prestige) while the firm’s long-term needs are relegated to a secondary position. It is possible that many senior managers within finance would, from a psychological perspective, be diagnosed with varying degrees of several pathologies and neuroses, such as delusional and extreme self-centeredness, impostor syndrome, heightened entitlement 
expectancy, lack of empathy, and lack of understanding of cause and effects. For example, Kets de Vries writes\cite{vries_dangers_2005}, 

\begin{quote}
My experience has shown that feelings of neurotic imposture profligate in contemporary organisations: I encounter this type of dysfunctional perception and behaviour all the time, particularly when working with executives in high-powered consulting firms and investment banking\ldots\, Power is so intoxicating, so addictive, that only the hardiest individuals can survive it without psychopathology.
\end{quote} 
 
This is why we need to carefully consider the governance design and the management of power complexes within the I³ framework. Paul Babiak and Robert Hare suggest that many boardrooms are riddled with individuals who otherwise would be diagnosed as psychopaths\cite{babiak_snakes_2014}: 

\begin{quote}
Only a fraction of psychopaths are in prison. The rest thrive in the many target-rich environments that make up society, including business, religious, political, and social organisations, and the Internet.	
\end{quote} 

Importantly, egocentric people struggle to see (and value) those who exhibit grace, peace, gentleness and care, as these are often mistaken as weaknesses. Not surprisingly, the latter are often left behind in the climb up the corporate greasy pole and are rarely in positions where the power complex dominates. During my own career it was evident that to progress in the upper echelons of management one needed to assimilate whatever culture was prevalent. In a particular firm, which professed to run a ‘meritocracy’, the reality was opposite, with a highly politicised culture and favouring of conformity. Part of the same firm’s culture was that they only hired PhDs; however, PhD stood for ‘Poor, Hungry and a Desire to become seriously rich’. Ego, money, prestige and power dominated the hierarchical drivers and left the culture undernourished in terms of its additional human needs. 

\begin{quote}
The economic stream, on the other hand, flies in the face of empirical reality: persons’ actions are shaped, redirected, constrained by the social context; norms, interpersonal trust, social networks, and social organisation are important in the functioning not only of the society but also of the economy.%\footnote{\url{https://www.bebr.ufl.edu/sites/default/files/Coleman\%20J.\%20(1988)\%20Social\%20Capital\%20in\%20the\%20Cration\%20of\%20Human\%20Capital.pdf}}
\end{quote}

Our relationships are based on our ability to conceptualise others; we label and add content to another person’s identity upon first meeting. Our own conceptual identity is a source of many dysfunctions as it is always in a state of un-fulfilled identity and, as such, may use others to feel superior. It is always looking to enhance itself if in deficit; if we can’t do it by being better than others, then we can become different, e.g., become a victim.  

Most religions say that parts of our ego must die us to find our true self, as explained by the psychotherapist Kathleen Dowling Singh: 

\begin{quote}
The ordinary mind and its delusions die in the Nearing Death Experience. As death carries us off, it is impossible to any longer pretend that who we are is our ego. The ego is transformed in the very carrying off.	\cite{singh_grace_1998}
\end{quote} 

Although character and virtue have been discussed since Aristotle, organisational scholarship is only just beginning to assess the contribution of character strengths and virtuous behaviours within the realm of leadership as a subset of organisational development. In essence, character strengths represent the way we have operationalised the neurological constructs of virtue.  

A ‘character’ was initially an imprinted symbol used by bricklayers to indicate the source of a brick\cite{calabrese_character_2002}, which indicated the character of the creator and thus the product’s quality and integrity. When the distance and complexity between maker/pro\-ducer and user/ consumer is short and personal, character can be of paramount importance. We know that traditionally in Japan, if a swordsmith’s sword broke during battle, the client could die or be wounded. This was a direct reflection of the quality of the sword itself and its maker, often resulting in the maker taking hara-kiri to create restitution. It is no coincidence that in such traditions quality and attention to detail prevails to this day.  

Character is pervasive and influences a person’s ‘vision, goals, self-concept, strategies, work ethic, attitude, perception, code of ethics, behaviour, and the search for excellence\cite[p.36]{cameron2007governing}’  There is an adage that states that the measure of a person’s character is what he or she does when no one is looking; furthermore, ‘character may not even be relevant unless someone is called on to use it\cite{peterson_character_2004}’. As such, development of character strengths such as humility are fundamental for leadership positions as evidenced by Adizes and Collins.  

Norman Schwarzkopf  suggested:`99\% of leadership failures are failure of character, leadership is a potent combination between strategy and character, and if you have to lose one, lose the strategy.'%\footnote{\url{http://www.artscouncil-ni.org/images/uploads/business-support-documents/ risk_guide_for_board_members.pdf}}

As teachers of ancient wisdom have suggested, we are shaped by our challenges and experiences; if overcome, we embrace new insights as gifts.  The late Steve Jobs said that: ‘The only way to do great work, is to love what you do%\footnote{\url{https://www.forbes.com/sites/amitchowdhry/2013/10/05/lessons-learned-from-4-steve-jobsquotes/\#54455a684f69}}’.  

Impact investment enables us to embark on a new path of individuation. As mentioned, part of our problem in understanding full-spectrum impact lies in our capacity to integrate and find balance in our many intelligences. For most of us, this is the need for the head to descend into the heart or ‘combine soul with role’. The heart is our core, in which all aspects of our ‘how’ wrestle with the questions on which our impact intentionality hinges.  
Can we explore and listen to the views and opinions of ‘the other’? Can we hold ambiguity and difference within our dialectic so that our deeper intelligence can find solutions rather than our ego create conflicts? Do we have enough courage (heart) not to be afraid of things we don’t want to hear? Do we avoid dialogue with those we disagree with ??  

Another personality area is one of deserving and being worthy of creating, giving, and receiving good things. This function is embedded in our sense of self-worth, self-esteem, and identity. Do we see ourselves as the true and honoured recipient, or do we generate negative feelings (including guilt and shame) once we have overreached in our capacity levels to hold these gifts? This often subconsciously invokes self-sabotaging strategies once we have reached a level we consider to be sufficient for our self-worth. We want to remain within our comfort zone. Many ‘achievers’ are not necessarily more proficient or capable but they have not been loaded with as much negative self-talk.  

This is important for impact, since we can only give to another that which we have ourselves. This is why impact is a form of ‘soul work’, for which I³ can provide the dynamic dance and framework in order to expand the capacities of the soul.  

Aristotle writes in his Nicomachean Ethics: 

\begin{quote}
Greatness of soul, as the very name suggests, is concerned with things that are great, and we must first grasp of what sort these are\ldots\, Well, a person is considered to be magnanimous if he thinks that he is worthy of great things, provided he is worthy of them; because anyone who esteems his own worth unduly is foolish, and nobody who acts virtuously is foolish or stupid\cite{kraut_aristotles_2018}.
\end{quote}

The first step to becoming a great soul is to think that one is worthy of great things. The point is to be committed and to serve something bigger than ourselves i.e., Impact.  

For those whose ways of creating reality come from the predominantly tangible (and only from the head) the above will be almost incomprehensible. The soul and heart with their invisible dynamics are more at work here; if not, we risk our core being like an alienated species which belongs to a different world. However, inner and outer realities constantly interact as the source of our creativity, and in co-creating the realities in which we live.  

Head domination may have worked brilliantly with the ‘finance and economics only’ Western dimension and the intellectual North, so we may have not realised that this distorted view does not work when we have to rely on, or integrate other areas. As such, we often see that traditional \textsc{\lc{CIOs}} require significant back-up from other people who may better represent these other dimensions, hence the need in many firms for a ‘Head of Impact.’  

It is not a coincidence that such positions today are filled by evolved females who may carry more capacity than men within the heart area and, more often, can view the world from the inside out. They often have a greater ability to think with the mind descended into the heart, integrating cognition and emotion to create a different type of courage necessary to create new knowledge and intuitive insights. However, let us also carefully consider the ‘Head of Impact’ role within the power complex, as quite often this is the more conventional \textsc{\lc{CIO}} role. 

What tensions are created between the finance dimension and others, and how are they resolved? What levels of influence does the \textsc{\lc{CIO}} have? Do co-creative solutions solve issues, or does a pragmatic \textsc{\lc{CIO}} trump the final decisions as the ‘senior’ committee member by diminishing the relevance of other ideas?  

A \textsc{\lc{CIO}}’s role includes creating spaces where we can go deeper, so that the intellect and a new dialectic can emerge, where the inner and outer realities converge with the core where our capacities and faculties lie. In our meetings, this will undoubtedly create both inner and outer tensions within ourselves and between others. However, if we hold these tensions in a co-creative space where we can leverage our collective intelligences, solutions and outcomes will be better. How we create our forum is an integral part of our governance, and therefore its impact effectiveness. 

In C.G Jung’s ‘Definitions’ he reminds us yet again of the imperative that: 

\begin{quote}
As the individual is not just a single, separate being, but by his[sic] very existence presupposes a collective relationship, it follows that the process of individuation must lead to more intense and broader collective relationships and not to isolation	\cite{jung_essential_1999}
\end{quote}
 
These tensions enable the heart, like any muscle, to exercise and grow; thus, people are less likely to explode in fury and anger. When managed healthily, conflict is an opportunity for a group to expand, build further resilience and share an open heart. Absence of conflict is not necessarily a sign of a healthy group but rather that the power complex is alive and active. 
 
Our awakening conscience is through the lens of difference and experienced perspectives. Integrality carries a psychodynamic and consciousness-based healing perspective that can contribute to any change dynamic. Organisations, in this sense, have a ‘pastoral’ responsibility as a person’s core-holding vessel.  

What people see is far more important than what they hear. People do what they see. As John Maxwell writes\cite{maxwell_developing_2006}:

 \begin{quote}
 Eighty-nine percent of what people learn comes through visual stimulation; ten percent through audible stimulation and one per cent through other senses... What they hear they understand. What they see they believe!’ Such environments, like the family of origin, profoundly influence the developing outlook and values system.	
 \end{quote} 
  
As we move into a more Integral age, firms gain new competitive advantages by moving into deeper and broader integral perspectives. This needs to be translated into an investment methodology that spans the schisms, and heals the divides between financial drivers and their inevitable impacts across the four worlds. The new leadership models now include intuition and foresight as part of the central ethic of the \textsc{\lc{CIO}} whose role, in essence, is to predict the future by knowing the unknowable and seeing the unforeseeable. This requires additional and deeper capacities, many of which will transcend space and time.   

The American parapsychologist and Noetic researcher Dean Radin suggests that the window of consciousness has perceptional capacities on which we can draw\cite{radin_entangled_2006},

\begin{quote}
When you step back from all the research and studies, what you find is a spectacular body of converging evidence indicating on our understanding of time is seriously incomplete. Those studies mean that some aspect of our mind can perceive the future. Not infer the future, or anticipate the future, or figure out the future. But actually perceive it.’ So this is all good news.
\end{quote}

It is part of our nature to explore and investigate. Depending on the outcome of such experiences, we become more (or less) comfortable in experiencing the unknown and unfamiliar. These are just some examples of the territories?? we may need to explore to understand the limitations and barriers we experience as a leader and as part of our own individuation using impact.  

As mentioned, unless we find a way to integrate impact as part of our individuation process, we will not only short-change ourselves and those around us but also not fully reach our potential. C.G. Jung reminds us that, 

\begin{quote}
Individuation has two principle aspects: in the first place it is an internal and subjective process of integration, and in the second it is an equally indispensable process of objective relationship. Neither can exist without the other, although sometimes the one and sometimes the other predominates.	
\end{quote}
 
Life, as we know it, is an extraordinary experience. Whilst pondering its component parts, I constructed the following humouristic representation which attempts to depict how the summary of life is made up from several characteristics and drivers inside and outside of our control.

%\begin{figure}\centering
%	\includegraphics[width=.8\linewidth]{part2_7-p1}
%	\caption{Component parts of life}\label{part2_7-p1}
%\end{figure}

%\begin{gather*}
	\textbf{Component parts of life:}
	\[\sum\infty=\frac{\Delta(q\times\beta)}{\Omega} + \alpha^3(\epsilon+r),\]
	$\sum\infty$ means Sum of Life; $\Delta$ means Decisions,
	$q$ means Choices; $\beta$ means Impact,
	$\alpha$ means Attitude; $\epsilon$ means Experience,
	$r$ means Response; $\Omega$ means Luck
%\end{gather*}

The integral \textsc{\lc{CIO}} now has to embrace systemic complexity and ambiguity to form a broad spectrum of precedents and areas that report into him or her. So what does this mean in terms of requirements and the work ahead? 
 
Joseph Jaworski puts it succinctly\cite{jaworski_synchronicity:_2011},

\begin{quote}
	The capacity to discover and participate in our unfolding future has more to do with our being – our total orientation of character and consciousness – that with what we do. Leadership is about creating, day by day, a domain in which we and others continuously deepen our understanding of reality and are able to participate in shaping the future. This, then, is the deeper territory of leadership – collective ‘listening’ to what is wanting to emerge in the world, and then having the courage to do what is required.
\end{quote}

Whilst this applies to all leadership, the integral \textsc{\lc{CIO}} needs to create an investment environment where this can occur but which may include new roles and areas as integrally outlined. Several organisations such as \textsc{\lc{AP}} Fonden of Sweden, and Robeco of the Netherlands have created the new role of Chief Active Ownership Officer in line with \textsc{\lc{PRI}} guidelines which indicate a shift in thinking and actions.  

Eric Butterworth writes: 

\begin{quote}
You may say, ‘But I am only human.’ This is the understatement of your life. You are not only human—you are also divine in potential. The fulfilment of all your goals and aspirations in life depends upon stirring up and releasing more of that divine potential. And there is really nothing difficult about letting this inner light shine. All we must do is correct the tendency to turn off our light when we face darkness.
\end{quote}

\section{From transformation to contribution}

\epigraph{Individuation does not shut one out from the world, but gathers the world to itself}{C.G. Jung. On the Nature of the Psyche}

This becomes the question of how we, through the medium of impact investments, build ourselves into a person of character and humility in the face of complexity, ambiguity, and contrast, formed through questioning and challenging the conventional wisdom within our firms.  

Paulo Freire writes that our task ahead is crystal clear:
 
\begin{quote}
If I am not in the world simply to adapt to it, but rather transform it, and if it is not possible to change the world without a certain dream or vision for it, I must make use of every possibility there is not only to speak about my utopia, but also to engage in practises consistent with it.	\cite{freire_pedagogy_2004}
\end{quote}

As has been highlighted, our lives are a journey of becoming more fully, so the question is: what are we becoming and do we like what we see? Do we have a mirror, and if not, why not? How do we clean our mirror when it becomes dirty or foggy? The creation of capital by itself is useless, unless it fuels us towards our destination. What level of participation and vulnerability have we managed to create for ourselves? Have we extended the frontier of what defines our reward in work to include defined areas of Impact?  

John Ruskin reminds us that: ‘The highest reward for a person’s toil is not what they get for it, but what they become by it\cite{quotes_john_nodate}.’ As we step into the realities of impact, we realise that there are multiple truths and perspectives. Like with any muscle, we have developed the capacity to hold tensions and ambiguities that come with the territory.  

Within our organisational settings and hierarchy, do we have the courage and imagination to hold this reality as truth, in order to empower all of us? Paulo Freire highlights the challenge but also calls for courage on our impact journey\cite{freire_pedagogy_1996}: 

\begin{quote}
The radical, committed to human liberation, does not become the prisoner of a ‘circle of certainty’ within which reality is also imprisoned. On the contrary, the more radical the person is, the more fully he or she enters into reality so that, knowing it better, he or she can better transform it. This individual is not afraid to confront, to listen, to see the world unveiled. This person is not afraid to meet the people or to enter into dialogue with them. This person does not consider himself or herself the proprietor of history or of all people, or the liberator of the oppressed; but he or she does commit himself or herself, within history, to fight at their side.
\end{quote} 

To what extent have we transcended, and included, a bigger self and understood the limitations of our ego with its made-up beliefs? Can I move into that space where I’m being challenged and feel the life-blood pumping in? There is no other way of being alive than being in that conversation. Ken Wilber sets the scene: 

\begin{quote}
For authentic transformation is not a matter of belief but of the death of the believer; not a matter of translating the world but of transforming the world; not a matter of finding solace but of finding infinity on the other side of death. The self is not made content; the self is made toast.	\cite{wilber_one_2000}
\end{quote} 

Finding the forum, community, and safe space for the dialectic to emerge was key to my understanding that everything we know is through difference, comparing and contrasting our internal dialogue with others. Are we asking the right questions? Back to Paulo Freire\cite{freire_pedagogy_1996}:

\begin{quote}
How can I dialogue if I always project ignorance onto others and never perceive my own? How can I dialogue if I am closed to – and even offended by – the contribution of others? At the point of encounter there are neither yet ignoramuses nor perfect sages; there are only people who are attempting, together, to learn more than they now know.
\end{quote} 

This can be viewed as per the Swedish social researchers, Alvesson and Skoldberg, in three theoretical hermeneutical strands that each individual needs to develop for their own understanding and interpretative practise\cite{lessem_integral_2010-2}:  

\begin{itemize}
	\item a hermeneutic understanding of history, language and meaning 
\item a social theory of society as a totality, and  
\item a theory of the unconscious.
\end{itemize}  

Impact therefore asks us to become our own social researcher and dig into our hermeneutic background and culture to explore how these may differ from others when we look towards our impact sphere. It has been said that it is our eyes that are the windows to our soul; it is also only through our own eyes that we can see ourselves in the mirror. If we don’t look for transformational development for our own internal Impact, we will only be able to perform the impact tasks intellectually as ordered by our organisation. Such a state would be incongruous if we are to live as authentic impact investors; we have to find the light from within that can shine into our world of impact.  

The Swiss-American psychiatrist Elisabeth Kubler-Ross put this aspect most beautifully\cite{maffin_soulistry_2011}, ‘People are like stained-glass windows. They sparkle and shine when the sun is out, but when the darkness sets in, their true beauty is revealed only if there is a light from within.’ There it is, made categorically clear, the inside-out and outside-in, double-sided mirror in which we can reflect our Impact consciousness back out into the world.   

Within our firms, when we look at the ingredients for good organisational culture, we find that these are human characteristics from which we need to draw our understanding of our organisational impact culture. Here’s an exercise: please rank each principle below on a scale of 1(worst)–10(best) for your organisation. Then honestly ask yourself: where are the gaps? 

%\begin{figure}\centering
%	\includegraphics[width=.8\linewidth]{part2_7-p2}
%	\caption{Fr. Bill Byron's 10 Principles of the Good Corporate Culture (1--5)}\label{part2_7-p2}
%\end{figure}

\begin{scriptsize}
	\begin{longtable}[c]{L{.75\linewidth}}
		\caption{Fr. Byron's Principles of Good Corporate Culture}\label{part2_7-p2}\\
		\toprule
		\textbf{Integrity}. Wholeness, solidarity of character, honesty, trustworthiness and responsibility\\
		\addlinespace[.4em]
		\textbf{Veracity}. Telling the truth in all circumstances; accountability and transparency\\
		\addlinespace[.4em]
		\textbf{Fairness}. Treating equals equally, giving to everyone his or her \\
		\addlinespace[.4em]
		\textbf{Human dignity}. Acknowledging a person's inherent worth; respectful recognition of another's value simply for being human\\
		\addlinespace[.4em]
		\textbf{Participation}. Respecting another's right or not to be ignored on the job or shut down from
		decision making within the organisation\\
		\addlinespace[.4em]
		\textbf{Commitment}. Dependability, reliability, fidelity, loyalty and consistency\\
		\addlinespace[.4em]
		\textbf{Social Responsibility}. An obligation to loon to the interest of the broader community and to treat the community as a stakeholder in what the
		enterprise does\\
		\addlinespace[.4em]
		\textbf{Common Good}. Alignment of one's personal interests with the community's well-being\\
		\addlinespace[.4em]
		\textbf{Subsidiarity}. No decision should be taken at a higher level that can be made as effectively and efficiently at a lower lever in the organisation\ldots\, never do for others what thet can do for themselves\ldots\, respect for proper autonomy\\
		\addlinespace[.4em]
		\textbf{Love}. An internalized conviction that prompts a willingness to sacrifice one's time, convenience, and a share of one's ideas and material goods for the good of others.\\
		\bottomrule
	\end{longtable}
\end{scriptsize}

%\begin{figure}\centering
%	\includegraphics[width=.8\linewidth]{part2_7-p3}
%	\caption{Fr. Bill Byron's 10 Principles of the Good Corporate Culture (6--10)}\label{part2_7-p3}
%\end{figure}

Developing the new \textsc{\lc{CIO}} role is not easy, and none of this can be accomplished unless we are willing and able to work with the inner self and not rely on our organisational position to make things happen. The temptation is always to take a seemingly easier route, a short-cut that we think no-one will notice. 
  
We need balance to our endeavours that does not compromise the end objective of becoming. This means going beyond conventional thinking, as the former four-star general Colin Powell states: ‘Leadership is the art of accomplishing more than the science of management says is possible\cite{quotes_collin_nodate}.’  

As an emerging \textsc{\lc{CIO}} in integral guise, a key question to be asked is: What new life and life-forms seek to enter the world through me? This meditative question may take a lifetime to answer, and in many ways I hope it does; our work is never done. If our sole purpose is working towards retirement where we can finally relax, sit back and enjoy the good things in life, we will have missed the point. 

\section{Impact contemplation}

We are in a constant process of creation. Each transaction is an opportunity for the best of our inner selves to emerge. With the integral approach that means the whole of us, every single component part, and to the very depth of our soul. This means taking time for contemplation.  

Joseph Campbell instructs us: 

\begin{quote}
You must have a room, or a certain hour or so a day, where you don’t know what was in the newspapers that morning, you don’t know who your friends are, you don’t know what you owe anybody, you don’t know what anybody owes to you. This is a place where you can simply experience and bring forth what you are and what you might be. This is the place of creative incubation. At first you may find that nothing happens there. But if you have a sacred place and use it, something eventually will happen.	\cite{stillman_soul_2012}
\end{quote} 

Indeed, something always happens; there is always something around the metaphorical corner. The theologian and teacher, Beverly Lanzetta writes: 

\begin{quote}
Contemplation is beyond the normal consciousness of the mind, granting access to the mystery, known only by love. Here, the normal activities of the human personality come to rest, in order to hear what has remained unheard and to see what has been hidden or veiled. The mystics call this kind of knowing ‘unknowing’ insofar as it approaches reality from the spiritual core of the person and not from the mind alone. 

Far more than a meditative practise or a temporary respite from worldly concerns, contemplation revolutionizes conventional attitudes and roles in order to transform the foundation upon which life is lived. And to illuminate the hidden teaching of love inscribed in our souls. 	\cite{lanzetta_monk_2018}
\end{quote} 
 
In 2012, Pope Benedict XVI invited Rowan Williams, then Archbishop of 
Canterbury and leader of the Anglican Church in England, to address the Synod of Catholic bishops. Williams emphasised the broader, foundational and radical importance of contemplation and building further on the ‘why’, which we can clearly see its importance and for impact\cite{noauthor_archbishops_nodate}: 

\begin{quote}
Contemplation is very far from being just one kind of thing that Christians do: it is the key to prayer, liturgy, art and ethics, the key to the essence of a renewed humanity that is capable of seeing the world and other subjects in the world with freedom— freedom from self-oriented, acquisitive habits and the distorted understanding that come from them. To put it boldly, contemplation is the only ultimate answer to the unreal and insane world that our financial systems and our advertising culture and our chaotic and unexamined emotions encourage us to inhabit. To learn contemplative prayer is to learn what we need so as to live truthfully and honestly and lovingly. It is a deeply revolutionary matter.	
\end{quote}

We also must understand that there are cultural differences in how any such practises are actualised. For example, in many African and African American cultures contemplation is a communal outward experience, in contrast to the West, where it is more individual and private.   

I hope that by taking some time to travel through some of the background and key tenets of the integral four worlds, we have gained an appetite for more understanding and for knowing how to implement this approach in ourselves and our organisation. 

Impact investments is a blend of art and science. We have been looking mostly at the artist in us, complemented by a scientific framework. I make no excuses for the integral approach; yes, it is complex, but that is because it is complete. Like all things, progress comes with practise. Our journey will be straight, it will always have corners, behind which we find new experiences and insights. 
 
When we travel, the unexpected fills the journey with new and expanded meaning but, if we don’t have the wherewithal to see it, it will pass us by and be lost forever. Our task is to be open and contemplative to what the impact dimension can bring into our lives using both mind and heart.  As C.G. Jung said, ‘Your vision will become clear only when you can look into your own heart. Who looks outside, dreams; who looks inside, awakes\cite{noauthor_carl_2013}.’  We cannot sleepwalk into impact as if it is just another technical subject we need to conquer.  

A last word on the importance of impact on contemplation: 

\begin{quote}
All forms of contemplation share the same goal: to help us see through the deceptions of self and world in order to get in touch with what Howard Thurman called ‘the sound of the genuine’ within us and around us. Contemplation does not need to be defined in terms of particular practises, such as meditation, yoga, tai chi, or lectio divina. Instead, it can be defined by its function: contemplation is any way one has of penetrating illusion and touching reality.\cite{palmer_brink_2018}
\end{quote}  

Our individuation depends on how we interact with the function and practise of impact in its entirety; as in marriage, we ignore or leave important pieces out at our peril. Trust our instincts, and when we move into the realms of possibility we will find that faith meets us. It is our homecoming into the house of belonging. 

\begin{footnotesize}
	\begin{quote}
		\textsc{\lc{THE HOUSE OF BELONGING}}
		
		I awoke this morning in the gold light turning this way and that thinking for a moment it was one day like any other. But the veil had gone from my darkened heart and I thought it must have been the quiet candlelight that filled my room, it must have been the first easy rhythm with which I breathed myself to sleep, it must have been the prayer I said speaking to the otherness of the night. 
		
		And I thought this is the good day you could meet your love, this is the black day someone close to you could die. This is the day you realize how easily the thread is broken between this world and the next and I found myself sitting up in the quiet pathway of light, the tawny close grained cedar burning round me like fire and all the angels of this housely heaven ascending through the first roof of light the sun has made. This is the bright home in which I live, this is where I ask my friends to come, this is where I want to love all the things it has taken me so long to learn to love. 
		
		This is the temple of my adult aloneness and I belong to that aloneness as I belong to my life.
		 
		\emph{There is no house like the house of belonging.}
	\end{quote}
\begin{flushright}
	David Whyte, 1996 
\end{flushright}
\end{footnotesize}

%\begin{figure}\centering
%	\includegraphics[width=.8\linewidth]{part2_7-p4}
%\end{figure}