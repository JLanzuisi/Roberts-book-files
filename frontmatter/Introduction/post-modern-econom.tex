\chapter{Post-modern economics}
\label{postm-econ}

Current economic thinking (and, as a subset, finance and investment thinking and
practise) is based on neoclassical economics epitomized by the Chicago School (Milton
Friedman and Eugene Fama). Neoclassicism which has developed since the mid/late 19\textsu{th}
Century is rooted in methodological individualism and has five core axioms:

\begin{enumerate}
	\item Individuals maximise utility and firms maximise profits.
	\item People act independently on the basis of full and relevant information.
	\item Markets provide the most efficient mechanism to allocate capital and resources.
	\item Individualism as an agent provides a sufficient incentive for decisions – the
	‘rational man’ [sic] theory.
	\item Equilibrium will emerge as a natural consequence of agents’ instrumentally
	rational preferences and choices, which can be identified and associated with
	value.
\end{enumerate}

Neoclassicism sees the financial harvest as the only driver and excludes the
relationship to social and natural dimensions; it has no developmental axis that recursively
helps to correct itself,

\begin{quote}
	Can we live with a forest in a way that makes it possible for the forest to evolve? To me, that’s very different from asking how to harvest the forest
	appropriately\cite{haggard_chapter_2020}
\end{quote}

Neoclassical economics is rooted in David Hume’s Treatise of Human Nature
(1739) where he famously divided decision making into three distinct modules: Passions,
Reason, and Beliefs. Passions provides the core energy for the destination; Reason
navigates us along a course that gets us there, drawing upon our created sets of Beliefs.

Hume also wrote that; ‘Reason is, and ought only to be, the slave of the
passions’. However, this is an incomplete picture for which there are only partial answers
but also one which is easily and intellectually understood by cause and effect, which is the
only connecting principle our limited intellect can understand in our meaning making. To
activate other intelligences, we need an integral framework to dig into our deeper
understanding.

Thanks to the influence of behavioural economics, neoclassical economics is
evolving to add models in which economic participants are imperfectly informed,
frequently irrational (or boundedly rational). As a result, Homo Economicus is slowly
evolving to resemble the real human more closely. However, the basis of neoclassical
theory retains its roots firmly within liberal individualism.

Another area influencing and driving the discipline of economics today is
anthropology, and especially Karl Polanyi’s work. Thro\-ugh meticulous fieldwork (often
missing in today’s economics) he demonstrated that other principles apart from the
efficient mar\-ket-exchange hypothesis and self-interest, such as recipro\-city, loyalty and
honour, create the glue that bonds the economy together. If society is influenced too
much by market exchange, ‘counter-movement’ resistance develops.

Polanyi’s followers today include economists such as Piketty, Varoufakis,
Arns\-per\-ger, Eisenstein, Daly and Anielski who see economics through a systems lens be
it rooted in political, social frameworks or from Nature-based perspectives. The risk, as
always, is that any power complex that becomes over-balanced develops a neurosis and
arrogance that will justify and manipulate the system to retain its position and interests.
Arrogance is one of the outcomes and consequences of the power complex when it
sees itself as superior in relation to perceived inferiority,

\begin{quote}
	Manipulation, like the conquest whose objectives it serves, attempts to
	anesthetize the people so they will not think. For if the people join to their
	presence in the historical process critical thinking about that process, the threat of
	their emergence materializes in revolution... One of the methods of manipulation
	is to inoculate individuals with the bourgeois appetite for personal success.
	This manipulation is sometimes carried out directly by the elites and sometimes
	indirectly, through populist leaders\cite{freire_politics_1985}
\end{quote}

Mark Anielski, for example, embodies and embraces the increasing importance of
the commons which is the cultural and natural resources accessible to all members of a
society, including natural materials such as air, water, and our habitable earth. He has
developed measurement systems for the well-being of the commons instead of just
documenting economic activity, consumption and circulation of money. He has created
new measures that focus us on what really matters: happiness and well-being.

Anielski also advocates alternatives to \textsc{\lc{GDP}} which drive and dictate policies across
the globe.

\section*{It is not too late}

In 1968, Robert Kennedy campaigning for the \textsc{\lc{US}} presidency, delivered a now-famous
speech at the University of Kansas in which he said:

\begin{quote}
	(\ldots) even if we act to erase material
	poverty, there is another greater task, it is to confront the poverty of satisfaction --- purpose
	and dignity --- that afflicts us all.
\end{quote}

Why haven’t we progressed since then? Old paradigms die hard and are difficult to
shift. Investments being a prime driver and critical inflexion point of economic activity, one
cannot underestimate the importance impact has as a catalyst to shift change perceptions
in all areas of our society.

The banking and finance industry has been searching for some answers from its
history, and is embracing the works of Peter Drucker. Drucker’s thinking around moral
codes and culture as the main success factors in any business has recently been
resurrected as a potential key to unlocking the lack of understanding of the interlocking
components of the organisation (e.g., economic, social and human, cultural, etc.) on
which understanding a restitution and repair mechanisms can be built\cite{drucker_management_1999}.
This, in my opinion, is due to the fragmented management approach we often encounter
which a more integral management framework would alleviate.

Don Beck, in his audio version\cite{beck_spiral_audiobook} of the book ‘Spiral Dynamics’ reframed Gandhi’s
quote, ‘Be the change you want to see in the world’ into ‘Do the change that others need
done for them.’ This suggests that if we take a deeper perspective on how we operate,
our contribution will always include the self and the other. The more levels of
consciousness and psychological flexibility we can generate within a specific context, the
more effective and loving the contribution will be towards creating the impact we desire.

This not only refers to ‘ways of knowing’ but also the ‘hows of knowing’ that include
formats of sub-conscious signalling and subliminal interconnectedness. W. Edward
Deming through his ‘System of Profound Knowledge’ clearly advocates that management
needs to look at organisations more holistically and examine the evolutionary aspect of
development and the understanding of human motivations from a psychological
perspective. This is lacking in many organisations based on post-modern economic
principles.

Social and environmental stresses are also redefining risk management by
introducing not only new risks (such as with stran\-ded assets) but also the risk of systemic
collapse in some correlated areas, in combination with slow GDP growth, resulting in what
is called the ‘Seneca effect’.

Its essence is found in a line written by the ancient Roman Stoic philosopher
Lucius Annaeus Seneca: ‘Fortune is of sluggish growth, but ruin is rapid’. This sentence
summarises the features of the phenomenon that we call ‘collapse’, which is typically
unexpected and sudden, like the proverbial ‘house of cards’ effect. But why are such
collapses relatively common, and what precipitates them?

Several books have been published on the subject, including The Collapse of
Complex Societies by Joseph Tainter (1998), The Tipping Point, by Malcom Gladwell
(2000), and Collapse by Jared Diamond (2005). Collapse is a collective phenomenon that
occurs in ‘complex systems,’ with a special emphasis on fractures and chaos in system
dynamics and the concept of ‘feedback’ leading to the collapse of structures such as the
financial system, famines and population collapse, the fall of civilisations and, the most
important of our age, the risk of collapse of our ecosystem from overexploitation and
climate change.

\section*{Motivation and reward}

Let us look at one of the core myths in many industries around motivation and reward,
namely that higher pay inevitably produces, and is correlated to, better performance.

\begin{quote}
	In eight of the nine tasks we examined across the \emph{three experiments, higher
	incentives led to worse performance} (authors highlight)\cite{boston_large_2005}
\end{quote}

The London School of Economics investigated fifty-one pay for performance plans
across industries, and concluded: ‘We find that financial incentives ... can result in a
negative impact on overall performance’\cite{gurerk_motivating_2009}.

A possible psychological explanation is that if we feel guilt and shame for the
rewards we receive, subconsciously we are likely to manipulate the situation/result so that
we become ‘worthy’, even if this means lowered performance; this is also known as the
‘self-sabotage mechanism’.

Based on my conversations with friends in the finance industry, many people work
almost exclusively for financial rewards but would do something different and more
meaningful if they were given a valid choice. This represents a major challenge over the
next few years as new recruits enter the system, and this is where working with impact
frameworks will make a difference.

From my experience in banking, I would argue that many firms have a poor
working culture so that employees believe that they need a premium to feel motivated.
Many such firms unknowingly also induce an internal fear-based culture through their
operating procedures. Also, it is clear that a fear-based culture is a major contributor to
the self-protective behaviour of attempting to maximise short-term personal gains (from
money and prestige) without considering the overall extent and effect of one’s actions
(i.e., in contrast to the search for true and real impact).

Most financial firms operate without deeper understanding of their culture and, as
such, do not have a compass to direct their development. However, there are many
significant economic business reasons for working strategically with culture. For example,
technical synergies in M\&A activities are often severely depleted due to lack of mapping
of the cultural fit. Given recent M\&A activity in the banking industry, this is a key area
where the levels of cultural integration drive ultimate shareholder value; therefore, this is a
prime commercial reason for leaders to embrace and manage cultural development.

Understanding prevailing culture is key in the ongoing compensation debate. Firms
need to develop new strategies for external drivers such as bonuses, titles, etc. Many
firms have yet to work with their culture to fully develop both personal intrinsic and
extrinsic motivational drivers. Motivation is dualistic in that it derives from either an internal
(intrinsic) or an external (extrinsic) source, or both. Examples from Professor Reiss’s work\cite{reiss_who_2000} 
suggest such sources of intrinsic motivations including power, order and
acceptance.

Thus, it is the leader/manager who dominates the cultural integrity of the
organisation. As such, it is the leader/manager that mainly represents the interface with
the firm and its cultural stand. This implies that it is the leader/manager’s neurological
patterns and character that mainly define how employees interpret and connect with the
organisation. Witness the adage: ‘the relationship with one’s supervisor (is) a lens through
which the entire work experience is viewed’\cite{gerstner-metaAnalytic}. Once
again this requires us to step into uncharted territory from our limited perspectives.

There are several aspects of leadership that comport with the objectives of this
book.

\begin{itemize}
	\item Leadership misalignments and conflicts of interest: Western
	governance structures and modes of compensation should
	include influences (and be open to alternatives) from, for
	example, Islamic and Buddhist finance.
	\item Incumbent leadership resists change, which creates ossified
	organisational structures and paradigms: Some firms have
	stagnated because of historical and archaic ownership structures
	when modernity and shifts in the organisation require change that
	has not been recognised (e.g., from patriarchal to collaborative,
	cooperative forms).
	\item The need for political and power relations to be uncovered: Most
	firms suffer from emotional abuse in various forms, including the
	silencing of stakeholders through dominance in equity. This is
	prevalent in many family firms where concentrated power exists.
	\item The social constructs and relationships between capital, debt and
	interest: For example, what are the visible social contracts such as
	in compensation policies, and under which economic paradigm,
	have they been constructed?
\end{itemize}

In this vein, to uncover and research the deactivating and development necessary for
true liberation for oppressed employees to become activated each I\textsu{3} engagement is, in
effect, a mini-critical-theory research project to understand the investee firm’s
economic and social constructs, contracts, and operational culture.

\section*{A New Era?}

\begin{quote}
	Libertarian action must recognise this dependence as a weak point and must attempt
	through reflection and action to transform it into independence. However, not even the
	best intentioned leadership can bestow independence as a gift. The liberation of the
	oppressed is a liberation of women and men, not things. Accordingly, while no one
	liberates himself by his own efforts alone, neither is he liberated by others. Liberation, a
	human phenomenon, cannot be achieved by semi humans. Any attempt to treat people
	as semi humans only dehumanizes them.\cite{freire_pedagogy_1996}
\end{quote}

It is therefore critical for each investment firm to become the ‘libertarian’ by
understanding their role for transformational capital as part of a liberation of the people
involved and not just of things. In this book, I will not cover the myriad psychodynamic
relationships where emancipatory processes may be required (e.g., male/ female;
victim/persecutor, etc.) through any I\textsu{3} process; but they exist at a deeper level if we look
closer.

Andrea Jung\footnote{No relation to C.G. Jung}, 36 the former \textsc{\lc{CEO}} of Avon, current \textsc{\lc{CEO}} of Grameen America and
campaigner for women’s rights, suggests that there is an obvious correlation between an
economically empowered woman and the investments she makes. 

This helps drive her
social and moral conscience and aims for bettering her community. At the organisational
level, this translates into boss/subordinate and involves all the unfulfilled needs for
nurturing and empowerment that we seek inside organisations and their many transient
and enforced relationships. 

At the deepest unconscious levels, these needs resonate with
our family and social history. Consequently, it is what we have neglected to empower
within ourselves that we seek in the outer world, only to become enslaved to our projects
and our projections.

This key aspect validates and reinforces the way we interpret our world that
includes the unconscious. 

Importantly, it also establishes and reinforces the relationship
between traditional emancipatory aspect of slavery and links to our variant of
contemporary corporate enslavement.

Chomsky puts it this way:

\begin{quote}
	The principle that human nature, in its psychological
	aspects, is nothing more than a product of history and given social relations removes all
	barriers to coercion and manipulation by the powerful\cite{chomsky_reflections_1975}.
\end{quote}

As the Western-dominated paradigm equates money with power, investment firms
need to be cognisant of their position and capacity to inadvertently manipulate
management into outcome objectives that have no emancipatory grounding and will, if
anything, only reinforce the power structure.

A core driver for change is that the current financial system has reached its
capacity to deliver genuine growth and well-being. 

Impact, by definition, asks much bigger
questions that require us to attend to our capacities in new dimensions; in my opinion, we
also need to question our attitudes, relationship and attachment to conventional dogmas
which have the money-power system at their center. 

This might conflict between one’s
beliefs and necessities for life, so for most, the questions will never be investigated.

Some have taken a strong position, such as the American journalist and social
activist Dorothy Day (1897--1980) who argued:

\begin{quote}
	We need to change the system. We need
	to overthrow, not the government, (\ldots) but this rotten, decadent, putrid industrial capitalist
	system which breeds such suffering\ldots\cite{day_pilgrimage_1999}
\end{quote}

If impact investments are to become a solution for the world going forward, we
collectively need to include a bigger ‘other’.

\begin{quote}
	The foundation of political economy and, in general, of every social science, is
	evidently psychology. A day may come when we shall be able to deduce the laws of
	social science from the principles of psychology\cite{pareto_manual_2014}.
\end{quote}

This, as we know today, is also the core driver behind many of our conflicts,
inequality and system stress. 

The philosopher Bernard Stiegler argues that the global
economic and consumerist model is now highly toxic, and has potentially reached its outer
limits of function. He suggests that those denying this want to secure the benefits as long
as they can\cite{stiegler_for_2010}.

If so, at the collective level most current and traditional investment philosophy
relates to a profit maximisation and continuation of the status quo. 

The impetus of the
emergent impact stems from the awareness that we must collectively find a new approach
and better ways to invest that benefit both people and the planet.

To this discourse, we now also add the political conflict that exists in our minds
between the two main hemispheres of the brain. 

The left and right sides of the brain have
significant differences in types and processing capacity; there is actually a political map
working inside trying to reconcile these differences.

For most of our organisations, which tend to be highly left-brain dominated, this is
an additional challenge because many of them have devalued, toned-down and detached
right brain intelligence such as intuition and imagination. 

We create conflict, become
polarised and narrow-minded and close down opportunities for resolution in order to
protect the ego and to prove ourselves right. However, for impact we need these sides of
the brain to be more fully connected to avoid political infighting. As Albert Schweitzer said
in 1959,

\begin{quote}
	Three kinds of progress are significant for culture: progress in knowledge and
	technology; progress in the socialisation of man; progress in spirituality. The last is
	the most important. Technical progress, extension of knowledge, does indeed
	represent progress, but not in fundamentals. The essential thing is that we
	become more finely and deeply human\cite{schweitzer_reverence_2017}
\end{quote}

Charles Eisenstein in ‘Sacred Economics’\cite{eisenstein_sacred_2011} (pp. 27--28, and pp. 425--426) confirms
the linkages:

\begin{quote}
	Part of the healing that a sacred economy represents is the healing of the divide
	we have created between spirit and matter. In keeping with the sacredness of all
	things, I advocate an embrace, not an eschewing, of materialism. I think we will
	love our things more and not less. We will treasure our material possessions,
	honour where they came from and where they will go (\ldots) The cheapness of our
	things is part of their devaluation, casting us into a cheap world where everything
	is generic and expendable (\ldots) Put succinctly, the essential need that goes unmet
	today, the fundamental need that takes a thousand forms, is the need for the
	sacred—the experience of uniqueness and connectedness (\ldots) We are starving
	for a life that is personal, connected, and meaningful.
\end{quote}