\chapter{A short reminder of our global imperatives}
\label{short_reminder}

Today, there are several core global Impact imperatives and realms that contain
significant amounts of stress. We need to internalize these to make them ‘ours’, so that
we can understand them and then define our contribution. For most, these will be framed
inside the \textsc{\lc{ESG}} acronym which explicitly means:

\mypar{Environmental} It is difficult to deny climate stress and
environmental degradation. As one of the core foundational stones
for impact investments, the landmark \textsc{\lc{COP21}} agreement in Paris
witnessed international agreement on limiting global warming to
below two degrees Celsius above pre-industrial levels by 2050.
Meanwhile, the 17 \textsc{\lc{SDG}}s established in 2015 build on the success
of the Millennium Development Goals established by the \textsc{\lc{UN}} in
2000 to fight poverty and inequality, and tackle climate change.

\mypar{Social} Our global economy is producing economic inequality and
new levels of social deprivation, contrary to the prevailing economic
theory that ‘all boats float higher with a rising tide’. This has given
rise to crime and migration. Hidden parts of the social and
community dimension are often disregarded within a firm’s mission
when firms act as disconnected islands within the broader system
in which they operate.

\mypar{Governance} We have many variations of governance and
ownership structures that can provide an alternative to the current
short-term shareholder. Many Western firms embody a central form
of concentrated power. Sometimes this risks developing different
levels of egocentric leaders with hubristic tendencies that
accentuate organisational demise. Such leadership is becoming
increasingly disconnected from real long-term stewardship of firms
and its impact potential. We need to be aware of this.

In addition to the 17 \textsc{\lc{SDG}}s, we also find the following key driver:

\mypar{Demographics} Inter-generational capital stewardship is shifting
from para\-digms of finance and Internal Rate of Return (\textsc{\lc{IRR}}) into
deeper thinking. This includes broader perspectives about how
money is earned and who benefits from it. This includes calling
more women towards working in the impact industry because they have a stronger tendency to move towards ‘sustainable’ investing in
comparison to many men.

The good news is that, according to the Global Sustainable Investment Alliance
(\textsc{\lc{GSIA}}), \$23 trillion (26\%) of all assets under management in 2016 were in ‘socially
responsible investments’ that take account of \textsc{\lc{ESG}} issues. However, what does ‘take
account of’ mean? And how do we significantly increase this? We will answer these
questions more fully in later chapters.

\epigraph{Herein lies the global knot: The seemingly irreconcilable conflict between and
	among the haves, they have nots, they have a little but want more, and they have a lot but
	are never content. There must be a better way}{Beck\cite{beck_spiral_2006}}