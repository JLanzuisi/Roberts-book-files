\chapter{The need for further integrality}
\label{the_need}

The above quote summarises the fundamental need for impact investments and the
developmental axis for the integration of a deeper four-worlds journey. Lessem and
Schieffer argue that to build a paradigm-shifting approach for transforming self,
organisation, community, and society, we must alleviate imbalances whilst aiming to
enhance a sense of calling, belonging and rootedness (local identity) with a view to
catalyse a meaningful, wholesome contribution to humanity (global integrity).

Integral Worlds is modelled on natural principles and on life itself. It aspires to heal
today’s fragmented, conflictual and often destructive individual, organisational, communal,
and societal ways of being. It holds a vital key to the necessary (and imminent) shift of
global consciousness towards a more healthy, holistic, peaceful, participatory, conscious
and co-creative approach to development in tune with the whole of creation.

Integral Worlds initiates processes of healing and holistic realignment by activating
all complementary parts of any human system: nature and community; culture and
consciousness; science, systems and technology; as well as enterprise, economics, and
politics. The continuous focal point of any transformation process within an Integral
Worlds’ perspective is the inner core of the individual or collective entity, its deepest value
base, spiritual and moral source leading to serving ourselves and our expanded others.

Real transformation has less to do with intelligence, willpower or seeking perfection, and
more with finding honesty, integrity, humility, willingness, and surrender. To the extent we
can evolve into servant leadership in impact, grounded and rooted in our calling, culture
and the soils of our community, this will form part of our emancipation and individuation
process:

\begin{quote}
	Individuation is a process informed by the archetypal ideal of wholeness, which in
	turn depends on a vital relationship between ego and unconscious. The aim is not
	to overcome one’s personal psychology, to become perfect, but to become familiar
	with it. Thus, individuation involves an increasing awareness of one’s unique
	psychological reality, including personal strengths and limitations, and at the same
	time a deeper appreciation of humanity in general\cite{luton_individuation}
\end{quote}

\begin{figure}\centering
	\includegraphics[width=.8\linewidth]{p2-lessem_schieffer}
	\caption[Integral Worlds Approach]{Integral Worlds Approach (Trans4M \cite{trans4m_integral_nodate})}\label{p2-lessem_schieffer}
\end{figure}

The underlying circular design of the above central model acknowledges that,
since time immemorial, the circular shape has been a symbol for the totality of life. It also
symbolises the cycle of life that each living system undergoes.

\begin{flushleft}
	The Story of a Circle\\
	\small
	\emph{A circle expands forever}; it covers all who wish to hold hands.\\
	\emph{And its size depends on each other}, it is a vision of solidarity.\\
	\emph{It turns outwards to interact} with the outside and inward for self-critique.\\
	\emph{A circle expands forever}; it is a vision of accountability.\\
	\emph{It grows as the other is moved} to grow. A circle must have a centre.\\
	\emph{But a single dot does not make a circle}; one tree does not make a forest.\\
	\emph{A circle}, a vision of cooperation, mutuality and care
\end{flushleft}

In Integral Worlds, the outer globe marks a worldly, holistic perspective. Embedded in the
outer global circle is the local context. At the very centre of such a globally embedded
context is the ‘inner core’. It is here, at the core of an individual, organisation, community
or society, that the impulse for transformation or development is initiated --- be it through a
perceived imbalance of the overall system, which becomes our objective concern, or
through a particular, subjective evolutionary calling.

This inner personal core and the outer global circle are connected through the
‘4Rs’ of Integral Worlds: Realities (world views), Realms (knowledge fields), Rounds
(different levels, from self to world), and Rhythms (transformative rhythms that apply to all
of them). These main themes and core values inform the integral journey.

We use the cardinal directional points of South, East, North and West as holding
vessels and containers for what is inside each realm of the integral approach.

\textsc{\lc{Southern Reality and Realm of Relationship}}\\
Main theme: Restoring life in nature and community\\
Core value: Healthy and participatory co-existence

\textsc{\lc{Eastern Reality and Realm of Inspiration}}\\
Main theme: Regenerating meaning via culture and spirituality\\
Core value: Balanced and peaceful co-evolution

\textsc{\lc{Northern Reality and Realm of Knowledge}}\\
Main theme: Reframing knowledge via science, systems and technology\\
Core value: Open and transparent knowledge creation

\textsc{\lc{Western Reality and Realm of Action}}\\
Main theme: Rebuilding infrastructure and institutions via enterprise and economics\\
Core value: Equitable and sustainable livelihoods

This can only be achieved if we work and engage integrally, i.e., with and inside all
the four dimensions of integral development as shown in figure~\ref{p2-lessem_schieffer(2)}.

Below, in figure~\ref{p2-lessem_schieffer(3)}, we show the integral development map with its sixteen fields, presenting the major
developmental task in each field. This map introduces the core challenges of the full integral
development journey --- challenges which can be addressed holistically with the help of
integral development theory.\footnotetext{Lessem and Schieffer (2014, p. 130).}

\begin{figure}\centering
	\includegraphics[width=.65\linewidth]{p2-lessem_schieffer(2)}
	\caption[Integral Development: Map 2]{Integral Development: Map 2 (Trans4M \cite{trans4m_integral_nodate})}\label{p2-lessem_schieffer(2)}
\end{figure}

\begin{figure}\centering
	\includegraphics[width=1\linewidth]{p2-lessem_schieffer(3)}
	\caption[Integral Development: Summary]{Integral Development: Summary (Trans4M \cite{trans4m_integral_nodate})}\label{p2-lessem_schieffer(3)}
\end{figure}