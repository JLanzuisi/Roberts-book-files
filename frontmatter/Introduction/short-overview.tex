\chapter{Short overview of global trends/macro drivers}
\label{short_overview}

	 \textsc{\lc{A growing debate about how neo-liberal}} capitalism has reached its
	outer limits and is being replaced by ‘post-capitalism’.
	
	\textsc{\lc{The emerging possibility}} that a global paradigm is developing
	towards collectivism and solidarity and away from individualism and
	self-centricity. This is predicted as the next stage in human
	development by Claire Graves (Spiral Dynamics, 2005); and Elias
	Dawlabani\cite{dawlabani_memenomics:_2013}.

\begin{figure}%[H]\centering
	\includegraphics[width=.8\linewidth]{p1-memenomics}
	\caption{\textsc{\lc{Meme}}nomics (Trans4M \cite{trans4m_integral_nodate})}\label{p1_memenomics}
\end{figure}

Dawlabani highlights the need for individual and organisational developmental
change, suggesting;

\begin{quote}
	This is the New Frontier of leadership where political and corporate
	governance must have the tools and the courage to transition the world to the values
	that recognise the magnificence of existence and what it means to be alive on a planet in
	peril. (Ibidem)
\end{quote}

\textsc{\lc{Information technology is reshaping the workplace}}, driven by
several organisational micro trends:

\begin{itemize}
	\item A restructuring around the degree and level of contractual arrangements
	and obligations and the overall firm/emplo\-yee relationship.
	\item Increasing automation blurring the lines between work and free time as
	people become more available for alternative activities.
	\item The knowledge economy is morphed into the information economy as
	knowledge workers become less reliable and necessary as repositories of
	knowledge in comparison to free on-line availability.
	\item Alternative forms of collaborative production and services spontaneously
	sprouting both inside large firms (e.g., ‘velcro-teams’) as part of their need
	for innovation and to generate creative strategies, but also in new
	collaborative formats (e.g., self-managed spaces, the shared economy,
	etc.).
	\item Direct innovation that captures these shifts and interjects products that
	solve new emergent competitive gaps in finance, e.g., alternative
	currencies, peer-to-peer finance, not-for-profit firms, etc.
	\item A new market has developed for the storage and diagnostic analysis of
	socially-produced information as captured by firms in an attempt to create
	a scarcity model on which to capitalise. As a result, big technology firms
	attempt to privatise this information and control the data. However, as
	some of this data can be triangulated and made available, firms are losing
	their position to price correctly using only market data. As such, this model
	is likely to be superseded by the already growing information-abundance
	model, where most data is freely available. Where self-regulation can’t
	keep up with change, restrictions will be imposed by regulators who
	already seek to limit the monopolisation and abuse by tech giants of such
	data.
\end{itemize}

\textsc{\lc{Company valuations and market capitalisations}} have moved from tangible assets
on balance sheets of circa 80\% twenty years ago to circa 10\%--20\% today. A
massive shift is occurring towards intangible valuations such as goodwill,
intellectual property, customer and brand values, etc. This creates increasing
market correlation risks between intangibles and therefore needs better
understanding and strategic work by firms, enabled by a full-spectrum impact
approach.

\textsc{\lc{As some industries and markets}} reach peak saturation, we see firms reaching for
leverage, financial engineering, and merger and acquisitions to drive shareholder
value, thus increasing risks.

\textsc{\lc{Over the past 4-5 decades}}, owners of capital have been well rewarded through
returns, unlike labour which, combined with increased inequality, is a core driver
behind many of the social conflicts we see today.

In 1962, the economist Kenneth Arrow argued that the purpose of inventing things
in a free-market economy was to create intellectual property rights. He noted: `Precisely to
the extent that it is successful there is an underutilisation of information\cite{mason_end_2015}'

Most Western economies are driven by debt. The West is now so enmeshed in the
debt spiral that there is no way out. Given the short-term political focus, governments will
always be tempted to use debt to pay for current electoral promises and to stay in power.

In addition, debt mortgages the future for present value; it takes expected future value and
brings it to the present. This has been helped largely by fixed-asset price inflation on
which debt can be secured and leveraged, for example in the housing market. What we
forget is that there is actually no real value being created: The house itself does not care if
it is worth \$100,000 or \$1 million because it is still the same house; of course, the people
living there care as it gives them the illusion of wealth.

This has driven \textsc{\lc{GDP}} growth and is why governments in effect have become reliant
on House Price Inflation (\textsc{\lc{HPI}}) for economic growth. However, in many Western countries
this has not only led to massive speculation on property values but also crowded out
much of the next generation, who no longer can afford to buy houses at current prices.
Are there any winners or have we simply playing a game of who can access finance to
create \textsc{\lc{HPI}} today, which needs to paid by the next generation?

Another related mega trend is the ubiquitous low interest rates. We need to
differentiate between those who benefit from a low interest rate and those who still pay
high levels of cost for capital and who are proportionally worse off than during previous
rate cycles. With rate convergence, we continue to fuel the debt spiral. Some are
suggesting that this may soon end; if so, given the current global economic conditions and
the state of the finance industry, the last financial crisis will seem quite mild in comparison.
Another outcome of rate convergence is a lowering of absolute returns and
possibly reduced financial return differentiation between managers, which is where impact
returns can make a difference.

The development of the I\textsu{3} methodology is underpinned by some strong
developments that are currently gaining momentum within the investment management
industry:

\begin{itemize}\small
	\item Realisation that impact does not necessarily mean lower returns.
	\item Impact when connected to the bottom line makes commercial sense.
	\item Institutional investors are redesigning their product offerings to include impact.
	\item Impact is becoming an integral part of most generic risk-mana\-ge\-ment models.
    \item Addressing new and broader impact risks such as stranded asset risk.
	\item Increasing fiduciary responsibilities to generate positive \textsc{\lc{SDG}} impact outcomes.
	\item Legal and reputational risks are increasing in relation to, and contrast with, to
	ignorance of impact.
	\item Increased levels of resources are available, e.g., tools and systems for impact
	management and measurement.
	There is global growth of the family office where conscious capital is developing
	strongly.
	\item Impact investments are developing as the key growth area within the global
	investment industry.
	\item Open source technology is being defined and designed according to individual
	needs.
	\item Developing thematic and sectoral approaches that form part of a politically
	agnostic process across the industy’s method, methodology and management
	model.
\end{itemize}