% Description: This file is for the forewords.
% Log:
% 08/02/2020 20:37:22 GTM-04 Half way through the forewords.
\chapter{Forewords}
\label{forewords}

\section*{Mark Anielski}
\label{mark_anielski}

Dellner provides value for a new generation of impact investment practitioners, including institutional
investors and investment organisations, as well as the average investor interested in how
their investment may have well-being impacts on the world. 

Like the sextant that constituted
a new instrument for the navigation of oceans, Dellner's work provides an important new tool
for investors to navigate the complex and often tricky waters that constitute investment. Like
using an effective map, Dellner's I\textsu{3} model helps decision makers understand and navigate
their investment decisions by considering the broadest suite of possible impacts to the
wellbeing of a wide variety of stakeholders affected by an investment or by an enterprise.

Most importantly, Dellner's proposed model for integral accounting, reporting and
management for the investment world is a refreshing new challenge that builds on at least
two decades of advances in sustainability measurement and reporting. In my mind, what has
always been imperative, from the perspective of both conventional accounting and economic
measurement systems, is the need for an integral form of asset accounting that goes beyond
simply financial capital asset accounting. Accounting for the human, socio-cultural, natural
and built capital assets, in harmony with the financial assets of any organisation, community,
or nation, is critical to a wise and sustainable stewardship of the genuine wealth of nations.

Dellner presents a practical road map together with an integral `sextant' to help a new
generation of investors who are genuinely interested in measuring and accounting for the
impacts of their decisions and their investment portfolios in line with the values of their
clients, be they individual, institutional, or societal. Understanding how investments in
companies, enterprises, and other financial instruments contribute to a well-being return on
investment constitutes a new frontier in the financial world of banking and investments.

Building on a continuous evolution of new measures of progress (including
triple bottom line, balanced scorecard, sustainable, and social accounting) and reporting
standards such as the Global Reporting Initiative (\textsc{gri}) standards, impact investment seems
to be the latest fad in a financial world desperately trying to present a different model for
investing financial assets with some semblance of virtue and values.

The integral approach to impact investment which Dellner presents is another
value added perspective that comes from his experience in the banking and investment
world. I believe it will contribute to a serious improvement in how investors and investment
managers can manage investment portfolios because of its capacity to verify the positive
well-being impacts of their investment choices.

My efforts as an economist, trained in accounting and forest economics, have been
to shift the focus of measures of progress to well-being impact measures across all aspects
of fiscal, monetary, and investment management.

`Integral' is an important word that will shape a new consciousness in the investment
world; integral means comprehensive, inclusive, non-marginalizing and embracing. Integral
approaches in any field attempt to be exactly that: to include as many perspectives, styles,
and methodologies as possible within a coherent view of the topic. Integral is a close cousin
to words like `resilience', `harmony' and `flourishing'.

I believe the current system of financial capitalism has been operating without any
concern for well-being impacts; it has a myopic focus on chasing financial returns, even
though the degradation of human, social, natural or environmental assets may take place.
There is a fundamental failure to account for losses or depreciation caused by the way we
choose to live. Putting on a new set of lenses using Dellner's integral model with a
four-worlds perspective involves a serious examination of both individual conscience, and the
`conscience' and virtues of the investment portfolio itself. Robert Kennedy once said that the
main shortcoming of national-income accounting and measures of economic progress,
namely the \textsc{gdp}, `measures everything except that which is worthwhile'.

Dellner has developed a practical working model for helping impact-in\-ves\-tment
decision makers increase their consciousness of the totality of the well-being impacts of their
investment portfolios and their investment choices. His I\textsu{3} model will help you, as an investor
or impact-investment fund manager, help your investment choices become more virtuous
and thus impact well-being and even happiness. This can be verified. To my mind, Dellner
provides a tool for helping the financial world start to account for the impacts on the
well-being of the communities and the environment which their actions affect.

Make no mistake, we have a long way to go before the financial and accounting
worlds take seriously the importance of measuring well-being returns on their investments,
and have accounting systems that can measure the well-being impacts on all five forms of capital
asset classes. As with any journey, we need a map, and instruments to help us.

Consider that the double-entry bookkeeping system used by businesses around the
world took more than 525 years to develop. It was originally developed by Franciscan friar
and mathematician Luca Pacioli and Leonardo da Vinci in 1492. Pacioli designed the
debit=credit system of accounting, together with balance sheets, as a tool for the Medici
bankers of Florence, and businesses in Venice. He argued that a wise business person kept
accounts of the assets, liabilities and equity of his or her enterprise because `all wealth
ultimately came from God or the Creator'. This shows increasing consciousness that all
wealth (or ultimately well-being) is a gift from the Creator or God and to ensure humans
flourish is to be a wise steward of all of the assets that were gifted by God.

The use of Dellner's proposed integral model for managing investments will require
an ongoing reflection on the words of T.S. Eliot:

\begin{quote}\centering\itshape
	Where is the wisdom we have lost in knowledge? \\ 
	Where is the knowledge we have lost in information?
\end{quote}

Indeed, the beginning
of wisdom within the financial sector may in fact be the realisation that wise financial asset
stewardship will indeed require an integral approach and mindset, which sees everything as
integral (interconnected).

The image of a circle, rather than upward sloping productivity curves and stock
market indices, is the image that comes to my mind when thinking about an integral
approach. Moving to this future `promised land of integral finance' will require a fundamental
examination of the nature of money itself and its creation. My critique of the debt-money
system suggests that the very nature of debt-money creation, and compound interest, is the
primary driver for the need for constant economic or \textsc{gdp} growth, as well as for a constantly rising stock
market and financial performance indices.

Dellner's book is a clarion call for courageous change in a world drowning under
mountains of unrepayable financial debts. The \textsc{us} alone has more than \$70 trillion in
outstanding debt, which consumes roughly 50\% of the income of an American household in
the form of hidden interest charges. Few economists and financial sector experts have
presented alternatives to an international debt-money crisis that has no apparent solution.
Stock markets must continue to grow as all of us chase unsustainable financial returns and
wait for the game of musical financial chairs to stop. It is not money that is scarce but the
courage and willpower to imagine a new economic system with forms of money that reflect
the truth of natural laws, of abundance and of resilience as revealed in Nature.

It is clearer every day that the financial system is operating unsustainably and
perpetuating --- or ignoring --- environmental and social problems. What few people realize is
that global debt situation is essentially like an inoperable cancer. A new economic
system is necessary that is built on the wisdom of the four-worlds (considering the cultural
wisdom of the various cultures or nations from the North, East, North and West) that Dellner
uses to construct his integral model.

Dellner's work needs to be taken very seriously by today's generation of impact
investment managers and advisors, particularly those who are advising the next generation
of young people who are inheriting vast sums of financial and material wealth from their
parents and grandparents. Dellner's ideas and proposals provide an important map and
compass for wealth-management advisors to a new generation of investors, who seem to
have a different appetite for investments that contribute to a better world, rather than simply
generating high financial returns. This is a new generation of investors with a higher
consciousness, more sensitized to the ecological crisis the world faces. They long to invest
their time and money into initiatives and companies that align with their own values for a
better world.

My own proposal for a new economic system, based on well-being and measuring progress
in terms of changes in the conditions of well-being of our communities and the environment
as part of a new integrated five-capital asset-accounting model, is very much in harmony
with Dellner's proposed integral model for impact investment. Ultimately investments are the
allocation of resources, time and money into a suite of assets with an expected `return',
usually measured in financial terms (profitability, dividends and return on assets). But
financial returns are only one form of well-being returns. A well-being based approach to
impact investment would consider the positive and negative qualitative effects, as well as the
monetary (full costs) impacts on human, social, natural and built assets of a community,
nation, enterprise and even investment portfolio.

Measuring these impacts is not as daunting as some might think. Again, Dellner posits a
process by which the impact investor and manager can `journey' around the circle of the
four-world views of an enterprise (in my model they can `journey' through each of the four
key asset classes) and ask the question: 'What well-being impact will my investment make
on this asset class, and how can that impact be measured and verified?'

As Dellner describes it, his I\textsu{3} model `is a map-making journey through the integral lens
where each of us will need to find answers that work and fit within our own context. I\textsu{3} is not a
thing or product, it is more of a dynamic roadmap and lens.'

Ultimately the onus will be on each of us, whether an individual investor, investment
manager, banker or financial minister to put on a new set of glasses to examine our own
consciences as we make investments. We will hopefully be motivated by this, and a
yearning for the good of others and Nature, to determine whether our choices are likely to
contribute to a better future and a well-being outcome.

This book is an important read, particularly for the younger generation of investors who want
their investments and money to do good.

\begin{flushright}
	\textsc{mark anielski, economist and author}\\ \small\emph{The Economics of Happiness: Building Genuine
	Wealth \& An Economy of Well-being: Common-Sense Tools for Building Genuine Wealth
	and Happiness}
\end{flushright}

\section*{Professor Dr Ronnie Lessem}
\label{Professor_Dr_Ronnie_Lessem}

Robert Dellner's formidable work follows in the footsteps of much that has come before, in the areas of social auditing, reporting, and accounting, prior to the establishment of the field he has
now entered and evolved in a duly \emph{integral} guise, namely `Impact Investment'.

In the 1970s and 1980s, I was heavily engaged in the fields of so-called social auditing,
social reporting, and social accounting. This was, at least in part, a reaction to my sterile
experience of having being an articled clerk to a firm of `Western' accountants in the City of
London in the 1960s. I had become bored to tears by all the figures, all the numbers, which
represented a very impoverished representation of the firms I audited around the \textsc{uk}.

So a decade later, now in my thirties, having joined Matrix Consul\-tants\cite{hargreaves_business_1975} in London, whose
focus was on corporate `social affairs', perhaps anticipating what was to come over
the later course of my life and work, I was given the opportunity to develop a richer form of social
accounting and reporting. In my first published article, entitled Accounting for an Enterprise's
Well-being\cite{lessem_accounting_1974},  I wrote in the 1970s:

\begin{quote}
	Business enterprise today is being called upon to exercise significantly wider  `social
	responsibility' than has traditionally been the case. As a result, individuals, communities and
	national governments are beginning to call for statements of  `social account' which reflect a
	company's performance in the eyes not only of financial shareholders, but also of other
	stakeholders in the community at large. As a result herein I attempt to extend fundamental
	accounting principles, which have traditionally embraced only monetary stocks and flows,
	towards physical, social and psychological exchanges. I therefore provide a foundation both
	for the development of the accountant's/auditor's traditional role and for a means of
	communication between interest groups within and without the enterprise. As such I do not
	attempt to develop thoroughgoing quantitative measures to the same degree of specificity as
	conventional financial accounts; rather I aim to develop a novel framework, to which both
	management practitioners and theorists may apply their own specific refinements.
\end{quote}

Crude as this opening account may have been, to my knowledge since then there has been
no other attempt to apply such `double-entry' principles to environmental, social, 
psychological, and financial transactions. Yet for me such `double-entry' (asset and
liability, credit and debit) accounting involved life principles rather than exclusively financial
ones. For example, for the assets acquired by a mining company in terms of, for example,
coal, there is a liability incurred as far as the Earth is concerned. Such a liability at the time
of writing (August, 2019) is more likely than not the cause of the continuous flooding in Great
Britain, ultimately `debiting' millions --- if not billions --- of pounds to the nation's financial
accounts, not to mention the monumental loss of personal lives, and losses to Nature.

A year later in 1975, I\textsu{3} turned to the more broadly based field of \emph{Corporate Social Reporting},
following up on my previous work:

\begin{quote}
	The particular field in which I requested documentation, in a letter sent out to 400
	companies, was corporate social responsibility in general, social and/or human resource
	accounting, consumer programmes and activities, pollution control, energy conservation,
	recycling, community affairs, volunteer programs, race relations, corporate contributions and
	manpower lending
\end{quote}

Corporate social reporting, I then ascertained, could be categorised:
\begin{itemize}
	\item By a particular \emph{channel}: newsletter, annual report, composite social
	report
	\item Through the particular \emph{medium} of words, number, pictures
	\item Including a specific \emph{mix} of activities, policies, results, standards,
	achievements
	\item Conveying specific information about physical and human resources,
	products meeting social needs, distribution of finances, internal and
	external stakeholders
	\item Employing a particular \emph{form} of presentation: scorecard, inventory,
	balance sheet
	\item Conducted by the company itself or by an external \emph{source}
	\item\emph{Focused} on a mere account of what is, or an account of what should
	be, in relation to a potential standard or comparison
	\item\emph{Concentrated} around the company's main product, and/or market, or
	on peripheral stakeholder interest
	\item Finally, in varying \emph{degrees} of breadth or depth of concern.
\end{itemize}

However, by the end of the decade I had abandoned the whole social accounting/
reporting/auditing field to move onto broader and deeper managerial and intellectual
pastures. In fact, for all the rich potential that I had uncovered, I discovered that most --- if not
all --- the accounting practitioners in the field (there were very few academics engaged in it)
became mesmerised by numbers: physical numbers, financial numbers, statistical numbers.
I began to feel, as Nigerian social philosopher Bayo Akomolafe\cite{akomolafe_these_2017} has so recently and
eloquently stated, that the whole exercise was becoming self-defeating.

Instead of getting closer to the environment, the person and the community, the `social
accountants' were putting more and more distance between such phenomena and
themselves, not to mention the practising managers. The trouble was I only had a general
intimation as to where to go from there, as I was not yet a social science `researcher' in its
purest sense nor had yet discovered an \emph{integral} way. In fact it was more than three decades
before I began to see that integral economic light that would pave the way for \emph{integral}
research.

Fast forward three decades. Early in the new millennium, in my article\cite{lessem_managing_2001} written for the \textsc{uk}
journal \emph{Long Range Planning} (my \textsc{mba} thesis at Harvard Business School was on corporate
planning), I was beginning to develop an integral approach, encompassing `North', `East',
`South' and `West':

\begin{quote}
	The world of economics and business has become dominated by one cultural frame of
	reference --- `North-Western' --- to the point that the hidden strengths of other cultures, even
	those of China and India which are pursuing a strongly `Westernised' economic course
	today, are being ignored by individuals, organisations, and societies alike. Before the demise
	of communism there was at least an alternative approach, albeit one in opposition. Now, the
	post-modern age of the information society is almost universally capitalist, and even in its
	latest manifestation, that of globalisation, it exploits difference (market and consumer
	segmentation) rather than differentiating and integrating between and within cultures and
	economies. No ecology, including the modern university, can thrive for long when one
	element, propositional knowledge for example, is rampant.
\end{quote}

Truth be told, for me the world of accounting, like that of business and management, is as
strongly `Westernised' today as it was in the 1970s, notwithstanding decades of social
reporting, social accounting and social auditing. This is despite the fact that today social
enterprise and social entrepreneurship abounds, not to mention also the advent of the `triple'
or `quadruple' bottom-line!

In the new
millennium this integral approach has been applied by Alexander Schieffer and myself at the Trans4m Centre for
Integral Development to enterprise \cite{lessem_transformation_2009} 
and economy\cite{lessem_integral_2010-1}, research\cite{lessem_integral_2010} and
development\cite{schieffer_integral_2014}, as well as through Trans4m Communiversity Associates\cite{lessem2019idea}
to \emph{The Idea of the Community} as well as to our \emph{Islamic to Integral Finance}.

Dellner has applied this integral approach to accounting and investment, thereby
serving to uplift `impact investment' from its somewhat --- at least in terms of theory ---
spurious state into an emerging discipline in its own right, both in theory as well as in practise. It is
now for others to take on from where he, so richly and expansively, leaves off.

\begin{flushright}
	\textsc{professor ronnie lessem},\\ \emph{Trans4m Communiversity Associates, August 2019}
\end{flushright}
%\clearpage
\section*{Alejandro Cañadas}

Robert Dellner’s book is coming at a perfect time when there are a growing demand and supply of Impact Investment interest and implementations. It is estimated that from a total of 66.4 Trillion \textsc{\lc{US}}\$ representing all the asset managers by the top 400 ranked by worldwide and external institutional Assets under Management (\textsc{\lc{AUM}}), 13.2 Trillion \textsc{\lc{US}}\$ of \textsc{\lc{AUM}} is within Impact Investment initiatives. Higher interest in Impact Investment is excellent because these efforts are focused on seeing Impact Investment as an extension of socially responsible investing. Namely, they focus on companies that promote ethical and socially responsible consciousness, such as environmental sustainability, social justice, and corporate ethics. This is something positive that goes beyond traditional work in finance founded on the Efficient Market Hypotheses (\textsc{\lc{EMH}}).\footnote{The traditional work in finance that was founded on the Efficient Market Hypotheses (\textsc{\lc{EMH}}) that states that asset prices reflect all available information at that point in time. A direct implication is that it is impossible to "beat the market" consistently on a risk-adjusted basis since market prices should only react to new information. The \textsc{\lc{EMH}} was developed by Eugene Fama’s influential 1970 review of the theoretical and empirical research to estimate financial market returns. See the Fama’s seminal paper \cite{fama_paper}.}

Impact investing goes a step further by aiming to reduce negative impacts and actively seeking investments that can create a significant, positive impact. Impact investing focuses on investing in companies or organizations to create measurable societal and environmental benefits while still generating a favorable financial return. 

Impact investing is typically centered on addressing a social issue, such as poverty or education, or an environmental issue, such as clean water \cite{maverick_top}. Dellner’s book is an excellent complement to the growing demand and supply of Impact Investment by moving several steps forward. 

Dellner developed the I\textsu{3} model, which is a systemic, unified, comprehensive, holistic, and integrated system for Impact Investment that will 
help decision-makers understand and navigate their investment decisions for real ecological impact. 

The Integral model itself is based on the Trans4M model of Integral transformation and praxis as developed over decades by Dr. Ronnie Lessem and Dr. Alexander Schieffer.

What is more, Dellner’s I\textsu{3} model builds on two decades of advances in sustainability measurement and reporting that uses both conventional accounting and economic measurement systems, which is the foundation of an integral form of asset accounting that goes beyond merely financial capital-asset accounting. 

Dellner’s I\textsu{3} model assesses the accounting for the human, socio-cultural, natural/environmental, and built-capital assets, in balance and harmony with the financial assets of any organization, community, or nation, which is critical to wise and sustainable stewardship of the genuine wealth of nations.

Dellner’s book brings a new paradigm in Finance because the I\textsu{3} model is a unified, systemic, comprehensive, holistic, and integrated model, centered on the development of a moral core. 

It is also a practical road map to help a new generation of investors who are genuinely interested in measuring and accounting for navigating the impacts of their decisions and their investment portfolios in line with the values of their clients (individual, institutional, or societal). 

That is why this book is ideal for anyone who is looking for the secret key to truly integrate the whole of Finance and Economics with the world of personal and communitarian flourishing and wellbeing.  Dellner’ shows in his book that exists a true harmony among Finance and Economics incarnated in real communities that create a true inherent unity between our consciousness and the world ecology. 

\begin{flushright}
	\textsc{\lc{Dr Alejandro Cañadas}},\\ PhD, Mount St. Mary's University School of Business, USA
\end{flushright} 

\section*{Author's foreword}
\label{author_foreword}

\epigraph{ Few can foresee whither their road will lead them, till they come to its end.}{Legolas, \emph{Lord of the Rings}, J.R.R. Tolkien.}

This book is a synthesis and fusion from my decades in finance, investment and
psychology. I realize they are now inseparable and I'm thankful for my journey, which has
given me opportunities for constant discovery and renewal. I have encountered numerous
women and men who have been instrumental in shaping my journey. As fellow travellers, I
will be forever grateful for their loving guidance and thoughtful support. My objective is to
share insights to help others to expand on their own `impact' journey. 

Here we can define
impact broadly, as all the component parts inside an investment that we value, find important,
and that make up our footprint and defined outcomes. This book is meant as an introduction;
no single volume can do justice to the complexity of the subject. This book on
integrative finance provides one possible way of seeing, thinking and working more deeply
with impact, something that is largely missing in finance and investment literature. Today, we
are witnessing an emerging consciousness enabling us to better understand the roles and
responsibility that finance and investments have. And so to better understand the
causes and effects of global issues, where each individual, institution, and industry is equally
responsible. As the only constant in life is change, this is also a story about change, not
small incremental change, but transformational change. 

As we enter `the integral age', we
see deeper systems thinking. We are all looking for our own version of what is true for us
and how this may be integrated into our lives. We are looking, not just for absolute truths, but
also relative truths that can only be found and understood by exposing and exploring
contrasting opposites within ourselves. Impact is a creative construct that has more in
common with art than mathematics. That is why impact from the purely technical perspective
will never be sufficient to reach the kind of impact `which other methodologies cannot reach',
because we also need to include the psychodynamic of the self, the other, the organisation
and the prevailing culture of our society. This expanding lens of contrasting integration is the
essence of integral impact, which this book explores to the fullest extent possible. 

No system or approach per se is the solution; however, as human beings operating within the system,
we are the solution. There is no envisioned outcome or objective to this book; it intends the reader to
`take what you like and leave the rest'. May our paths cross on some part of this mutual
journey; until then, I wish you the very best on this important voyage of discovery that I
trust will be filled with meaning and purpose.

\begin{flushright}
	\textsc{robert dellner}, \emph{2019}
\end{flushright}
%\vspace{3em}

\epigraph{The real voyage of discovery consists not in seeking new landscapes,
	but in having new eyes}{Marcel Proust}