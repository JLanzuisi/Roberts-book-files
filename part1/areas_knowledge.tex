\chapter{Areas of knowledge}

\epigraph{Mathematics, natural sciences, human sciences, history, the arts, ethics, and to which I add religious knowledge and indigenous knowledge}{Author, Areas of knowledge}

Factors that transcend individual ways of knowing and areas of knowledge: 

\begin{itemize}
	\item  Nature of knowing: what are the differences between information, data, belief, faith, opinion, knowledge, and wisdom? 
\item Knowledge communities: what is taken for granted in a community? How can we decide which beliefs should be checked further? 
\item Knower’s perspective and applications of knowledge: how do age, education, culture, and experience influence selection of sources and formation of knowledge claims? If we know something, do we have a responsibility to use our knowledge? 
\item Justification of knowledge claims: why should claims be assessed critically? Are logic, sensory perception, revelation, faith, memory, consensus, intuition and self-awareness equally reliable justifications? 
\item Use of coherence, correspondence, pragmatism, and consensus as criteria of truth. 
\end{itemize}

No form of knowledge can be purely intellectual; it always passes through our psyche, our emotions and our spiritual self. One supporting definition of religion that I recently heard was “Giving the best that you have to the best that you know”. The direction of knowledge creation in most philosophies and religions can be defined as either Ascenders (pointing us upwards toward the One, the Eternal, and the Absolute) or Descenders (pointing us downwards toward the sacred within the many, the momentary, the mystery and the earth); it is seldom simultaneously both. 

For the renowned authority on Islamic Studies, Seyyed Hussein Nasr,

\begin{quote}
in the Orient, of which the Near East is a constituent part, knowledge has always been related to the sacred and to spiritual perfection. To know has meant ultimately to be transformed by the very process of knowing.	\cite{nasr_knowledge_1989}
\end{quote} 

This last sentence is key for any impact approach and we will return to it frequently.  

The political scientist Adam Webb has developed four separate ethos --- four self-understandings, four images of the ideal character --- as opposed to the more narrowly defined, ubiquitous and atomistic ‘Western’ one (Webb, 2006, pp.36-45). 

Each of these four is universal, cropping up repeatedly in different civilisations and eras. As permanent focal points of human culture, they have existed in creative tension with each other. But in modern times, the first of the four and the simplest --- atomisism --- has gone on the offensive and wrought havoc across the globe.  

The second ethos is \emph{demoticism}. The Greek word demos means the common people as a whole (the integral ‘North’), and \emph{demot}, the member of an ancient township.  

The third ethos is the ‘Eastern’ perfectionism (the integral ‘East’). Perfectionists reject the demotic ideas of sameness across all human beings. This perfectionist ethos has two major flavours: aristocratic and mystical. Whilst aristocratic perfectionists seek self-cultivation within the world, mystical perfectionists seek this beyond the world. Both focus on refining the best elements within themselves, elements that set them apart from humanity’s common denominator.
  
The fourth ethos (the integral ‘North’) Webb calls \emph{virtuocracy}. The word combines the 
Latin \emph{virtus} (virtue) and the Greek \emph{kratia} (ruling). Historically, virtuocrats have included the 
Catholic clergy, the Islamic \emph{ulama}, the Indian \emph{brahmins}, the Confucian \emph{mandarins}, literati, and other public-minded intellectuals. For perfectionists, the idea of transcendence of special qualities and insights distinguishes a minority. With demots, they are embedded in a social world where one can realize oneself by affecting others. 

In Chinese Taoism, the perfect person is seen as one who knows the Tao and lives according to this knowledge, implying that he/she lives according to the ‘how’ in ‘nature’. The originator of Taoism, Chuang-Tzu, wrote: The man of virtue can see where all is dark. He can hear when all is still. In the darkness he alone sees light. In the stillness he alone can detect harmony’ (Giles, 1961, p. 119). 

In Judaism, according to Nasr (1997), the significance of \emph{hokhmah} (wisdom) is critical. In the books of Job, Proverbs, and Ecclesiastes, the term \emph{hokhmah} (later translated as \emph{sophia}) appears nearly one hundred times. The Jews also believe that the Torah is the embodiment of wisdom.  

In Islam, the traditional names used by the sacred scripture are all related to knowledge: \emph{al-qu’ran} ‘recitation’; \emph{al-furqan} ‘discernment’, and \emph{umm-al-kitab} ‘the mother of books’. The Qu’ran refers to the importance of intellect and knowledge in almost every chapter; the first verses pertain to ‘recitation’ which implies knowledge (\emph{ilm}) and science (\emph{ta’lim}). The Arabic word for intellect, \emph{al’aql}, is in fact related to the word ‘to bind’, for it is that which binds man to his origin. Etymologically it could be compared to religion itself, for in this case \emph{religio} also means what binds and relates man to God. Even the Arabic world for poetry (\emph{al-shi’r}) is related to the root meaning of consciousness and knowledge rather than ‘making’ in the Latin root \emph{poesis}.  

Turning to Christianity, in modern times its sapiential dimension has been forsaken but Nasr maintains it cannot be totally divorced from it. Thus, in John’s Gospel: ‘In the beginning was the Word’ was interpreted for centuries as an affirmation of the primacy that logos was the prime source of both revelation and knowledge. Thereafter, our secularized reason created the separation of the sapiential gospel from the pursuit of science.  

Perhaps the most universal way to identify the two spiritual traditions of knowing and not-knowing is through light and darkness. The formal theological terms are \emph{kataphatic} or the ‘affirmative’ way (employing words, concepts, and images) and \emph{apophatic} or ‘negative’ way (moving beyond words and ideas into silence and beyond-rational knowing). I believe both are good and necessary; they affirm the need to understand our respective mirrors and shadows and only together do they create a form of higher consciousness. This is why our impact approach must be based on working with and exploring defined polar opposites in each of the coordinates. 

However too much of a good thing (e.g., light) can damage, so we must find a balance. In addition, we can only make visible much of our darkness by shining light on it, in the same way as we see the moon by the light from the sun. However, lunar light is more subtle, filtered and indirect, so we can observe and find nuances that sharp, direct light cannot see and which often creates its own shadows. Equally, we need to have courage to face our shadows as if and when we numb out the dark we also numb out the light. All things are a mix of shadow and light, and as such we need to embrace and include these in our map making. The Islamist futurist Ziauddin Sardar (2006) offers a relevant point: namely that research and teaching at universities today have almost no relevance to the problems of poverty and underdevelopment; much of our open-ended enquiry for context-specific impact should incorporate a plurality of views but free thinking is either absent or strongly discouraged. Yes, this is context dependent but we should be aware of such imbalances from our predominantly ‘Western’ perspective.  

At least five Islamic concepts, according to Sardar, have direct bearing on knowledge: justice – \emph{adl}; knowledge – \emph{ilm}; worship in its broadest sense – \emph{ibadah}; human trusteeship of the Earth’s resources – \emph{khilafa}, and philanthropic endowments – \emph{waqf}. Of these, \emph{ilm} is distributive knowledge: it is not a monopoly of individuals, a specific class, group or sex; it is not an obligation of only a few, absolving most of society; it is not to a particular field of enquiry or discipline, but covers all dimensions of human awareness and the entire spectrum of natural phenomena.  

Our impact interactions must reflect such distributions. Epistemology and societal structures feed on, and navigate, each other: when we construct our images of society, and develop social, economic, political, scientific, and technological structures, we use our conception of knowledge and its epistemology. So why in our ‘Northern’ perspective do we have such a strong focus on knowledge and its epistemology?  

Epistemology is so important because it is the major operator that transforms a world view into a reality. Any organisation will have to navigate this terrain and create unique and distinct skills, capabilities, and competitive advantages. For such knowledge to become integrally informed, we need the dynamic built into the integral model as we move deeper into each realm.  

We can now build our integral topography to see how these are connected developmentally, distinguishing between four layers (images, ideologies, institutions and inclinations) which Lessem and Schieffer call the ‘4Is’ (2014, p. 413 ). All are required for the eastern socioeconomic laboratory that builds integrally on what has come before. For thoroughgoing integral impact development to occur, all four levels need to become dynamically interconnected.  

We begin with the images at the deepest level of the enterprise. They form the source of individual or institutional creativity and imagination. 
%\clearpage
\begin{footnotesize}
	\begin{longtable}[c]{R{5em} R{5em} L{8em} L{4em}}
		\caption{Development Topography}\label{table1-ways_knowing}\\
		\toprule
		Topography & {\scriptsize Developmental Layers} & Expressions & Integral Realities\\
		\midrule
		Top-Soil & Inclinations& {\scriptsize Visible attitudes and behaviours /  
		outer practise} & West\\
		\addlinespace[4pt]
		Sub-Soil & Institutions & {\scriptsize The institutional frameworks that organise and direct our attitudes and behaviours, as well as the scientific disciplines underlying them} & West \& North\\
		\addlinespace[4pt]
		Bedrock & Ideologies & {\scriptsize The philosophies and worldviews 
		that inform our way of thinking / this layer includes ontology and epistemology, 
		defining what counts as valid knowledge and how new knowledge is to be created} & North 
		\& East\\
		\addlinespace[4pt]
		Core & Images & {\scriptsize The deep-rooted images, beliefs and 
		archetypal structures that inform, often unconsciously, our lives and that are directly related to our physical, psychological and 
		spiritual existence (‘human infrastructure’) / these root images provide in turn the 
		foundation for philosophies and 
		worldviews, as well as for institutional and 
		conceptual frameworks} & East \& South\\
		\bottomrule
	\end{longtable}
\end{footnotesize}
\footnotetext{Source: \cite{lessem_integral_2015} (p.401)}

\subsection*{Images: Touching the Core}

The deepest source of individual and organisational --- and indeed societal --- development is archetypal images drawn from ancient stories (such as creation myths), and the humanities (from the cultural depths of religion and spirituality) inclusive of language in its original context. They inform and fuel our imagination, from which we can begin to create our vision. Examples and manifestations are what the PR and advertising companies use to build images in consumers’ minds. 

\subsection*{Ideologies: The bedrock on which we stand }

At the bedrock level, we are dealing with philosophies and ideologies that lie well below the surface of everyday life and which we take for granted as dogma. Often this bedrock is imported from elsewhere as part of our historical marching orders. An example is Russian and Chinese socialism, where the countries were forced to change without being aligned with the local cultural and societal core, causing immeasurable pain and suffering.  

All M\&A activity will expose clashes of ideology between companies, resulting in many conflicts. As such, ideologies always risk distorting whatever processes of renewal may be taking place unless we, as part of our impact approach, address such disintegration consciously, and from the start, as part of the process.  

We also see this phenomenon when outer ideological imprints fall upon stony ground because they are not embedded or creatively assimilated in the individual, organisational, and societal consciousness. 

\subsection*{Institutions: constructed from our collective intelligence }

Institutional and conceptual frameworks create the holding vehicles in which we can understand and formalise our relationships and interactions. They include legal systems, political and economic structures, and predominating forms of public, private or civic enterprise. We must distinguish carefully whether we relate with a Spanish cooperative, a Zimbabwean \emph{integral kumusha}, an American corporation or a Japanese ‘\emph{keiretsu}’ (business group).  

Included in this subsoil of de-personalised systems, we also find the management models, usually restricted to Western ones, which inform the design of much of our conventional business institutions. Whereas it is individuals who characteristically initiate with their personal, formative and idiosyncratic inclinations, institutions keep things going over the long haul with their standardising rules, policies, and procedures. Anyone who deals with institutional change and culture would recognise immediately the power complexes of any system.  

\subsection*{Inclinations: the visible surface }

Now we come to the practise of our individual and organisational inclinations, including attitudes and behaviours. Engaging with diverse development contexts, here we may discover how cultural differences operate, from exchanging business cards in Japan, whether or not to shake hands with Arab women, how formal or informal we should be with the French, and our attitude to time whether in Harare or Hamburg. Many everyday nteractions with different people and cultures tend to be conducted in almost instinctive ‘topsoil’ terms, and are thus focused on individual traits and identities.  

What is therefore crucial for our socio-economic impact laboratory is that it taps into, and stays connected with, all layers. In many individuals, organisations, and societies, in particular in the developing world and its impact methodologies, we will discover significant ruptures between the four layers. We will notice that ideologies (bedrock) are often imported and not sufficiently home-grown or assimilated at the core level. Equally, in particular in the business world, we see a lack of connectedness between the institutional design of a private enterprise in the Anglo-Saxon style (institutional layer) with the guiding cultural philosophy (bedrock) within many societies, and so on.  

The development topography as part of our impact assessment can help us reveal these ruptures, make them conscious and subsequently engage in re-constructing our enterprises, enabling them to become more authentic and sustainable. We can now turn to the implications for sustainable development and Impact. 

\section{Integrating capital stocks}

Paul Hawken, together with his compatriots Amory and L. Hunter Lovins, has reconceptualised business in an ecological light to promote sustainable development (2010). 

What is required, Hawken suggests, is the diligence to understand when and where Western-style markets are dysfunctional and misapplied, and to choose the correct targeted actions to help them operate more holistically while retaining their pragmatic vigour and efficiency. 

If true, it is the fulcrum on which impact pivots and on which much of its future rests.  

At an enterprise level, Hawken and his colleagues seek to ensure four distinct capital holdings or stock (in line with Anielski): financial, manufactured, natural, and human. These should be as prudently stewarded as money is by the corporate finance director.  

This leads to our I\textsu{3} suggestion: not a purely financially-oriented enterprise investment model but ultimately a more holistically-based market integration of all capital stockholdings, as I\textsu{3} and much of impact seeks to achieve.  

What we can foresee as a result is that the next revolution (after the current digital revolution) will possess a particular structural and developed process. Within it, four kinds of ‘capital stock’ (structural) would be recognised, each one manifesting itself in four types of interconnected ‘capital flows’ (process). These capital stocks are natural, human,  manufactured, and financial. The flow forms enable companies and communities to engage in sustainable development, leading to technically productive, biologically integrative, economically facilitative, and ecologically restorative development. This is what is meant by transformative sustainability.  

If we do not transform our historical pain in our psychology, we will most assuredly transmit it – usually to those closest to us: our family; our neighbours; our co-workers, and, invariably, the most vulnerable, our children and all those impacted by our presence. Consider the analogy of energy circuits: most of us are relay stations and only a minority are transformers – people who actually change the electrical charge that passes through them.  
Impact has to become a transformative part of the process but is often kept static at the level of an old and limited belief system.  

\section{More on beliefs}

Our beliefs are formed when we take on board something that for us has become true. A core belief may contain hundreds of smaller beliefs that support it; consequently, it may be difficult to change.
  
Our values are also supported by many such affirming beliefs and they tend to reinforce each other. We may have had the experience that a previously deeply held view is shaken by new information; whilst it shudders as it finds new ground, when it lands it must comport with the rest of our system. Most people hold untested beliefs as private certainties, since our analytical model of the world cannot easily accept contingent states. These are reinforced by our language, semantics, and behaviour to claim universal validity for our take on ‘reality’. In Jarowski’s book\cite{jaworski_synchronicity:_2011}, he discusses with David Bohm the relationship between language and ‘reality’,: 

\begin{quote}
	The implicate order is in the first instance a language. It is not a description of reality but a language, an inner language, where you cannot associate each word to a thing. It is more like music. You cannot say one note means anything. It is like a painting. There are various spots of paint in an impressionist painting, but when you step back to see the picture, there is no correspondence between the spots of paint and what you see in the picture. Similarly, the implicate order and its mathematics does not directly come to describe a sort of correspondence with reality. It is simply a language. This language is referring to something that cannot be stated. The reality which is most immediate to us cannot be stated (p. 174)
\end{quote}  

This will have implications for us as we move impact into its next stages of development. 
 
As John Beckett wrote in Under the Ancient Oaks: ‘Hold your beliefs lightly, but while you hold them, treat them as though they were true and explore them as deeply as you can.’ 

As we have seen when looking at neurological filters, we don’t necessarily have the capacity to process incoming information and data to assess their true meaning. Essentially, we sift reality through several filters to create our simplified version, i.e., our perceptions.  

The Danish existentialist philosopher Soren Kierkegaard once wrote that: ‘Life is not a problem to be solved, but a reality to be experienced.’ From this, we also see that it is through our internal representation, including our language, that we create our world. Our world has no shape or meaning until we describe it, and it is only when we describe it that it activates possible distinctions that help govern our behaviour and actions. However, we do not really describe the world that we see but rather we see the world as we describe it.  

Understanding one’s relationship to reality is a constant challenge and a never ending task. If we can see behind these filters, we get increased levels of meaning. Organisations are no different because they are organised and run by people who are challenged and limited by their own filters. We often see this discourse played out in our internal politics in the form of tensions and disagreements.  

Much of a firm’s culture is hidden even to senior management, particularly if there is no conscious attempt to understand it, as I have discovered from my experience in most financial firms. Much of what is contained inside its shadow elements comes out occasionally for all to see and experience during stressful moments. The shadows also contain the hidden agendas and conflicts of interest which remain unspoken, which the firm prefers that the world does not know\cite{corlett_mapping_2003} (p. 27).

\begin{figure}\centering
	\includegraphics[width=1\linewidth]{areas_knowledge-p1}
	\caption{Map of the organisational psyche}\label{areas_knowledge-p1}
\end{figure} 

As in life, and in our investment thesis, the question is how we find reality and then return to it. Denial and other deflections are the mind’s protection mechanisms to avoid that which cannot be reconciled or accepted coming through the filters. This is why hindsight provides such a convenient solution to discourse. As the well-known quote from John Maynard Keynes states: ‘When the facts change, I change my mind. What do you do, sir?’  

It is only when we get to the end of a particular journey that we can look back over the territory and join the dots. As meaningless as an event may have seemed at the time, we are meaning-seeking and relationship- driven creatures, and as such, it is by synthesising the very constructs of experiences that we can reframe their internal drivers which connect how we see potential and future possibilities.  

As these mental constructs multiply, and we realize the enormity of our project at hand, we may be touched by what C.G. Jung defined as ‘synchronicity’ as our own, private, ‘acausal connecting principle’ of meaning. He suggested that synchronicity is ‘a meaningful coincidence of two or more vents, where something other than the probability of chance is involved’  (Rowe et al.,1997), i.e., cannot be explained just by cause and effect.  

This can be trained into deeper awareness and eventually become part of our everyday belief system. Depending on how this system was established and how we now hold it in terms of negative versus positive, our potential is diminished or enhanced. We can be ready, and even look forward to, any ‘drama’ if we can see the connectedness and synchronicity with how it will eventually turn into something positive.  

From my own beginnings only I, and no one else, can (or is qualified) to see this unfolding. Others may have opinions on what could have been done, i.e., if \textsc{\lc{ABC}} was done differently, then \textsc{\lc{XYZ}} would have been different, but this is at best wishful thinking. It is an illusion, since it is the individual who is connected to their past and must take responsibility. 

This is also a question of ownership. If you don’t feel you were in control and responsible of your beginnings (and to a degree I hold this view) to the same degree are we able to see and tie together our future potentials with possibilities, own and hold them to empower us into the next chapter of our journey.  

Elisabeth Kübler-Ross said: 

\begin{quote}
The most beautiful people we have known are those who have known defeat, known suffering, known struggle, known loss, and have found their way out of those depths. These persons have an appreciation, a sensitivity, and an understanding of life that fills them with compassion, gentleness, and a deep loving concern. Beautiful people do not just happen.%\footnote{\url{https://www.aquinasacademy-pittsburgh.org/myaquinas/confidence/ parentcommunication5-3-3-14.pdf}}
\end{quote}

If true – and clearly one can’t sit on the fence about this – then, as Frederick Douglass said: ‘Without a struggle, there can be no progress.%\footnote{\url{https://www.blackpast.org/african-american-history/1857-frederick-douglass-if-there-nostruggle-there-no-progress/}}’  

Since progress is a fundamental driver in the universe, we have to embrace our struggles since without them there is no story from which to grow. Liking this with the need to grow inside our Impact journey, C.G. Jung suggested that, 
‘The greatest and most important problems of life are all in a certain sense insoluble… They can never be solved, but only outgrown\ldots This ‘outgrowing’, as I formerly called it, on further experience was seen to consist in a new level of consciousness. Some higher or wider interest arose on the person’s horizon, and through this widening of view, the insoluble problem lost its urgency. It was not solved logically in its own terms, but faded out when confronted with a new and stronger life-tendency’ (Jung in Wilhelm 1931, 1962, pp. 91f). 

Without our troubles, we would have no lessons in life and no tale to tell from which others can gain insights. Neither would we have the input from which to shape our personalities for the better, gain acceptance and grace on the journey, and compassion for all things which can positively transform ourselves.  

The ancients have known this for millennia. Marcus Aurelius in Meditations instructs us to: ‘Accept the things to which fate binds you, and love the people with whom fate brings you together, but do so with all your heart\cite{quote_marcus_nodate}.’  

My own interpretive narrative is formed by the conscience, and constructs of the self which are immediately reshaped and reframed as new aspects, angles and inputs are added to the puzzle. I know from my own experience that, regardless of how I interpret something today, rethinking it immediately changes the way I will view. If I ‘sleep on it’, meditate, and let it rest within myself, it transforms into a different form and new intelligence. 

Whilst we swim in the force fields of our perceived reality, everything we know is through the lens and prism of difference, and experienced perspectives.  

Given a core issue of our time is increasing inequality in how capital is created, measured, and allocated, much-needed insight is required to address these imbalances. We must find organisational approaches, since it is within our organisations that we spend much of our time, and encounter our psychological mirrors.  

I collaborate with Mark Anielski, the economic strategist who developed the Genuine Wealth model\cite{anielski_economics_2007} (p.65--78) as a pragmatic tool for governments and business to measure trust, relational capital, and well-being; in the book’s foreword, Herman Daly referred to Mark as ‘God’s auditor.’ In his book, Anielski elaborates on key areas which directly relate and complement our integral work, namely: 

\begin{itemize}
	\item How to rediscover the original meaning of the language of economics. 
\item How to measure genuine wealth, which consists of five capital assets: human, social, natural, built and financial. 
\item How nations, governments, communities, and businesses are using the genuine wealth model to build the new economy of well-being. 
\item How at the family level, applying the genuine wealth model creates happiness. 
\end{itemize}

I fully endorse Mark’s search to create new knowledge to build happiness, which in turn helps inform and navigate the construct and direction of I\textsu{3}. All such co-created knowledge is constructed activity in which knowers are active contributors and shapeshifters. All knowledge comes in multiple forms (explicit, implicit, and tacit) and all inputs are interpreted by the individual receiver based on their perceptual filters and background. 
 
This means that from our impact perspectives we need to pay particular attention in cultures with deep roots different from our own.  

David Peat offers a challenge, 

\begin{quote}
The knower and the known moreover are indissolubly linked and changed in a fundamental way. Indigenous science can never be reduced to a catalogue of facts or a database on a computer, for it is a dynamic and living process, an aspect of the ever-changing, ever-renewing processes of nature \cite{peat_blackfoot_2006}(p.6)
\end{quote} 

This is often missed completely by the Western paradigm impact-driven organisations that professes to do investments where indigenous social structures prevail but whose structures are in actuality negatively impacted. How we engage with reality is critical, as is how we interact with the narrative that evolves as part of our meaning-making activity. As our map forms clarify and crystallise, we activate our new narrative stories with the world so we can develop new meanings and social impacts. As a continuous interpreter of such stories, we look to how we can become catalysts, social innovators and transformers of society, communities and people.  

Chaos theory suggests that nothing is lost in the universe. The world’s top physicists used to believe that anything that entered a black hole was obliterated, but no longer do. Current research shows that black holes transform objects rather than destroying them\cite{hankel_black_2015}. For us humans, such transformation comes from developing our new levels of consciousness, our character, and our creative capacities. 

According to the quantum physicist, Amit Goswami\cite{goswami_quantum_2014} (p. 1) the stages of creativity are: apparition; incubation; insight; transformation; and manifestation. Whilst our creativity is rooted in our notional spaces of ideas, images, and ideals, many years ago researcher Graham Wallis described what happens as people approach problems with the objective of constructing creative solutions. His four-stage process is (1926, p.10) : 

\begin{enumerate}
	\item  Preparation: we define the problem, need, or desire, and gather needed information; we establish the criteria for verifying the solution's acceptability. 
\item Incubation: we step back from the problem and let our minds contemplate and work it through. Like preparation, incubation can last minutes, weeks, even years. 
\item Illumination: ideas arise from the mind to provide the basis of a creative response. These ideas can be pieces of the whole or the whole itself. This stage is often very brief, involving a tremendous rush of insights within a few minutes or hours. 
\item Verification: one carries out activities to demonstrate whether or not what emerged in illumination satisfies the need and the criteria defined in the preparation stage. 
\end{enumerate}

\begin{figure}\centering
	\includegraphics[width=.8\linewidth]{areas_knowledge-p2}
	\caption{Iterative Model}\label{areas_knowledge-p2}
\end{figure} 


How the above is implemented is predicated on the level and format of governance and procedures that we have constructed, in combination with the level of open spaces that our culture has created. These are questions about our inner capacity for respect, trust, open listening, courage to speak, humility and resolve in finding answers and solutions. As all questions and answers require frequent self-examination and self-correction, if we can achieve this, we can build the space within our protocols to allow a possible Impact 2.0 and beyond to emerge.  

C.G. Jung said: ‘Only the mystics bring creativity into religion’ \cite{jung_mysterium_1970} (p. 477). He was referring to the need to create positive referential discourses in our otherwise static environment. 

Paulo Freire’s offers clear instruction on how to relate to knowledge, 

\begin{quote}
For apart from inquiry, apart from the praxis, individuals cannot be truly human. Knowledge emerges only through invention and re-invention, through the restless, impatient, continuing, hopeful inquiry human beings pursue in the world, with the world, and with each other (p.72) 	\cite{freire_pedagogy_1996}
\end{quote} 

Our understanding comes from our capacity to create and label contrast. The colour green would not be ‘green’ unless we could picture it and position it within a colour spectrum in relation to our own context. The French label this ‘vert’ and its representation differs according to how we have cognitively created the multitude of pictures that subconsciously emerge whenever the word is mentioned or retrieved from memory. In English ‘\emph{vert}’ may also conjure up areas such as ‘intro/extrovert’ and ‘invert’ which stems from the Latin verb ‘\emph{vertere},’ meaning ‘to turn or overturn’, so different pictures emerge.  

The same applies to words such as ‘money’, ‘capital’, and ‘investments’. An investment firm generates specific meanings for each according to our culture and myth streams. All investment firms will have gained a form of relational stewardship of capital based on a combination of capabilities, market position, track record, expertise, and espoused brand values, including trust and integrity.  

In addition, people individually represent their own cultural brand in conjunction with (and contrast to) their firm, particularly with clients and other stakeholders. This need for compliance and alignment may create a challenge with the firm which manifests as personal stress. This dimension is critical since to a large extent it dictates how the firm and its employees interact with the outside world, and the type and strength of its culture.  

In my experience working within large financial organisations such as 
Citigroup and BNPParibas, understanding culture was highly surface orientated. Working with a deeper cultural awareness was often actively discouraged. Given the dominant dimensions of technical expertise, a highly technology-driven business, and a linear, hierarchical mode of organisational management, there was little room for much else.  

I love Herbert Spencer’s quote in the Alcoholics Anonymous Big Book, which reminds us of one of our main barriers to knowledge, namely contempt: ‘There is a principle which is a bar against all information, which is proof against all arguments, and which cannot fail to keep a man in everlasting ignorance—that principle is \emph{contempt prior to investigation}’\cite{alcoholics_anonymous_alcoholics_2008} (568, author’s emphasis.)  

Whilst technical problems can often be solved by higher levels of thinking, all other problems are ‘adaptive problems’, i.e., they cannot be solved technically and require a shift in thinking and behaviour. One can argue that understanding culture and organisational relationships is often diametrically opposed to the prevailing knowledge base of most financial institutions. This may seem paradoxical, given that most firms and their management would claim the very opposite.  

\begin{itemize}
	\item      Technical expertise requires significant investment in time and effort but, once mastered, is relatively simple to implement and use because it comprises logical, digital, linear left-brain thinking patterns based on prevailing logic and learned paradigms.  
\item Culture and relationships are ostensibly easy to acquire. For many organisational leaders, psychology is considered inert knowledge; thus many believe it is easy to acquire and they can easily become an ‘expert’. However, unlike technical knowledge, this dimension is difficult to implement. Many organisations have failed, leaving behind costly and painful corrective cultural development and behavioural change programs. 
\item Senior management often stays within less risky technical ‘comfort zones’, giving rise to an incoherent strategy built on existing operational culture minus a progressive approach.  
\end{itemize}

As E. F. Schumacher suggested: ‘Any intelligent fool can make things bigger and more complex\ldots It takes a touch of genius --- and a lot of courage to move in the opposite direction\cite{quote_schumacher_nodate}.’   

Quantum physics has shown the interconnectedness and oneness of the operating principles of the universe and its relationship to our consciousness. Joseph Jaworski (\cite{jaworski_synchronicity:_2011} pp. 99 -100) outlines a discussion with the physicist David Bohm about Bell’s Theorem (later confirmed by Alain Aspect) on pairing and time-travel in particle physics, where Bohm confirms that: ‘The oneness implicit in Bell’s Theorem envelops human beings and atoms alike’. The physicist Henry Stapp of University of California at Berkeley said that Bell’s Theorem is ‘the most profound discovery in the history of science’ (Ibid).  

It proves that the elements of the world are fundamentally inseparable. This is one of the key premises for our integral four worlds and our understanding of true impact. In 
Jaworski’s discussion, Bohm explains that, 

\begin{quote}
Yourself is actually the whole of mankind. That’s the idea of implicate order-that everything is enfolded in everything. The entire past is enfolded in each one of us in a very subtle way. If you reach deeply into yourself, you are reaching into the very essence of mankind. When you do this, you will be led into the generating depth of consciousness that is common to the whole of mankind and that has the whole of mankind enfolded in it. The individual’s ability to be sensitive to that becomes the key to the change of mankind. We are all connected. If this could be taught, and if people could understand it, we would have a different consciousness (Ibid, p. 80-81)
\end{quote} 

This statement has profound implications for our understanding of impact. There is an implicit order in impact that only our consciousness can uncover and enfold, and which our map making will have to include. Consciousness is the key here, not knowledge.  

Bohm goes on to tell Jaworski: ‘You’ve got to give a lot of attention to consciousness. This is one of the things of which our society is ignorant. It assumes consciousness requires no attention. But consciousness is what gives attention. Consciousness itself requires very alert attention or else it will simply destroy itself. It is a very delicate mechanism.' (Ibid, p. 82). 

When we work with impact, we emphasise the states of consciousness and how we can generate more, not less consciousness. Much of our workplace is paradoxical: the more we assimilate to our place of work, the less able we are to find ourselves and new avenues for growth. Deeper insights often seem to come from other insights, activities and venues. 
 
Another paradox is that most firms state how they embrace diversity and difference whilst this is not valued at operational levels. Management often finds it easier to manage compliant staff, while supporting employees who ‘toe the line’. We will investigate these dynamics in greater detail later, but we know that to accomplish things we must not only dream (our South), but also believe (our East); not only plan (our North) but also act (our West), iteratively in that order.  

\epigraph{All men dream; but not equally. Those who dream by night in the dusty recesses of their minds wake in the day to find that it was vanity; but the dreamers of the day are dangerous men, for they may act their dream with open eyes, to make it possible.}{T.E. Lawrence}

