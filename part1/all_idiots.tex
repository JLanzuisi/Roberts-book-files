\chapter{We are all idiots}
\label{all_idiots}

Whilst this section title is not intended to be literal, all of us have limitations in seeing the whole picture and understanding what is happening inside our own minds.  There are several psychological elements that influence how we make our choices.  

\begin{itemize}
	\item Our character, sometimes inherent, can also be developed and is malleable over time.  
	\item We see the world through the internalised prism of the family and context of origin which needs reframing to be current and personal.  
	\item Massive neurological filters that only let through information that is familiar and deemed acceptable which needs to be opened. 
	\item The belief system created through our experiences. We only have two interpretative choices available when confronting experience: comply or rebel, both provide food for our beliefs which allows us to hold and justify any actions and experience within our thinking system.  
	\item The tendency to overinflate the ego to compensate for our fears, denial, insecurity and the fantasy of the unfulfilled self. This is sometimes known as ‘hubris’. 
\end{itemize} 

Unfortunately, for some people too much money removes or shields them from the lessons and experiences they needed to find a path for natural personal growth, development and individuation. As Jung reminds us; \emph{Individuation does not shut one out from the world, but gathers the world to itself}\cite{noauthor_individuation_nodate}.  Working deeply with impact may alleviate such situations.  
Moving our investigation of meaning making from psychology into economics, the economist, John Maynard Keynes wrote in his essay ‘Economic Responsibilities for our Grandchildren’,

\begin{quote}
	When the accumulation of wealth is no longer of high social importance, there will be great changes in the code of morals\ldots\ We shall be able to afford to dare to assess the money-motive at its true value. The love of money as a possession --- as distinguished from the love of money as a means to the enjoyments and realities of life --- will be recognised for what it is, a somewhat disgusting morbidity, one of those semi-criminal, semi-pathological propensities which one hands over with a shudder to the specialists in mental disease. All kinds of social customs and economic practises, affecting the distribution of wealth and of economic rewards and penalties, which we now maintain at all costs, however distasteful and unjust they may be in themselves, because they are tremendously useful in promoting the accumulation of capital, we shall then be free, at last, to discard\cite{john_maynard_keynes_economic_1930}.
\end{quote}

So here we are in today’s world and economy, wondering why many things are the same. There has been little visible change and, in some areas, such as inequality, there has been a dramatic negative shift. However, this is also where we – and our firms --- have a clear role to play in alleviating imbalances.  

Are we gripped by some collective mental disease, addiction to \textsc{\lc{GDP}}, or have we been trained to be ‘idiots’? The word Idiot originally is derived from the Greek word ‘idiotis’, meaning a ‘person lacking professional skill’, ‘a private citizen’ or ‘individual’. As such, please don’t be offended as “idiot” does not mean what we usually think but rather that we, as individuals, are part of a greater evolutionary whole of which we may lack some understanding.
  
The energetic theory of evolution postulates that there are four planes of being: the energetic, the atomic, the cellular, and the plane of creatures. Evolution suggests that each plane represents a stable energetic platform on which the next can develop; thus, integrally speaking each plane is transcended and included by the next. Each plane was created in three stages: first, individual entities learned how to become viable and independent inside the energy framework of their existence; then, these entities learned how to bond to form viable, independent group structures; and finally, they learned how to cooperate to create a higher order entity which now becomes the stable energetic plane for the emergence of the next plane of being. 

The only entity able to create a sufficient stable energetic platform at the atomic plane is the carbon atom. And, at the cellular plane, the eukaryotic cell, and apparently at the plane of creatures, \emph{Homo Sapiens}. 

Human evolution, as we know it, is not a straight line, nor is it predictable, in some instances it’s ever regressive and unless we work diligently to rectify all the negative and false influences, the next stage of evolution which I see as a regression may be the so called ‘digital man’. 

\begin{figure}\centering
	\includegraphics[width=1\linewidth]{all_idiots-p1}
	\caption{Human evolution, Joe Magee}\label{all_idiots-p1}
\end{figure}

In my native Sweden, we use a word for a business or firm that encapsulates its essence, namely ‘\emph{näringsliv}’, meaning something that nurtures or nourishes an organism. \emph{Näring} means nutrition and \emph{liv} means life itself. If Impact Investments is to be a driver for the ‘näringsliv’ we are trying to create, we must develop the organisational imperative to fully understand what actual impact has in accordance with its objectives and our mission.

This is a concept far removed from conventional thinking in finance. Another imperative for Impact is to understand these critical drivers and develop alternative accounting methodologies, since many cannot be captured using conventional digital and numeric accounting standards.  

Whilst we have seen enormous improvements in our economy and our well-being over the past century, we are also planting seeds for the next century in which access to capital will dominate the global agenda and drive its change, mainly in the pursuit of economic growth in isolation to generate its return.  

One of the key changes is the dramatic shift towards human and intellectual capital as the engine of business, which is being challenged but also enabled by the introduction of technology and artificial intelligence, this in effect is a double edged sword. However, as we have seen countless times, advances in technology must comport with the development of our consciousness—our real challenge today as technology can only improve technique, not our intentionality. As Yuval Noah Harari writes,  

\begin{quote}
	The danger is that if we invest too much in developing AI and too little in developing human consciousness, the very sophisticated artificial intelligence of computers might only serve to empower the stupidity of humans… To avoid such outcomes, for every dollar and every minute we invest in improving artificial intelligence, we’d be wise to invest a dollar and a minute in advancing human consciousness. Unfortunately, at present we are not doing much in the way of research into human consciousness and ways to develop it. We are researching and developing human abilities mainly according to the immediate needs of the economic and political system, rather than according to our own long-term needs as conscious beings.\cite{harari_sapiens:_2015} (pp. 70-71)
\end{quote}

Whilst some of these changes may seem inevitable, the role of capital lies in designing the complementary impetus and strategic direction in alleviating some of today’s imbalances. This is why any impact design has to include a deliberate dynamic for each impact realm to communicate and rebalance (by stealth or otherwise) to advance investor/ investee consciousness. 

Another impulse with implications for impact is the shift in enterprise work and a much higher reliance on knowledge to drive productivity and therefore sustainability. According to Drucker, 

\begin{quote}
	The most important, and indeed the truly unique, contribution of management in the 20th century was the fifty-fold increase in the productivity of the manual worker in manufacturing. The most important contribution management needs to make in the 21st century is similarly to increase the productivity of knowledge work and the knowledge worker\cite{drucker_management:_1999}
\end{quote}

Significant shifts in the knowledge that workers value and how they seek to make contributions, blended with technology, will create new types of winners and losers. We have heard some clarion calls about what it means to be a citizen and our collective responsibility, which have resulted in, for example, the universal basic income (\textsc{\lc{UBI}}) movement. Whilst some of these evolved initiatives are at an early stage, they are clearly rooted in the emergent levels of consciousness alluded to earlier.  

There may come a point in our own development where we recognise the harm we inflict on ourselves and others through our indiscriminate actions. Abraham Maslow wrote, 

\begin{quote}
	The serious thing for each person to recognise vividly and poignantly, each for himself, is that every falling away from species-virtue, every crime against one’s own nature, every evil act, every one without exception records itself in our unconscious and makes us despise ourselves. Karen Horney had a good word to describe this unconscious perceiving and remembering; she said it ‘registers.’ If we do something we are ashamed of, it ‘registers’ to our discredit, and if we do something honest or fine or good, it ‘registers’ to our credit. The net results ultimately are either one or the other—either we respect and accept ourselves or we despise ourselves and feel contemptible, worthless, and unlovable\cite{maslow_toward_1999}
\end{quote}

If true, impact investments can not only restore imbalances but also enable us to individuate and emerge as global citizens. From our investment perspective and ability to change and influence the investee, a prime objective is how we develop our Impact knowledge base and our own capacities to implement this knowledge where needed. Nowhere is this more important than for those who work on impact within other geographies and cultures. As Ziauddin Sardar writes,: 

\begin{quote}
	\ldots throughout the Northern hemisphere, centuries-old traditions of learning and scholarship had been erased, replaced by a set of ill-fitting Western-style institutions. These institutions had little grounding in the history or culture of countries they were in\ldots\cite{sardar_how_2006}
\end{quote}

We will return to his work later when we examine ways of knowing.  

Much of this disconnect, which has occurred throughout the ages, is the relationship to Nature, with creating harmony as the central idea. This has been replaced with other beliefs, including transcendence and salvation. Many primal people retain this connection with Nature and value it above all else.  
Huston Smith described ‘primal people’ as,  

\begin{quote}
	oriented to a single cosmos, which sustains them like a living womb. Because they assume that it exists to nurture them, they have no disposition to challenge it, defy it, refashion it, or escape from it. It is not a place of exile or pilgrimage, though pilgrimages take place within it. Its space is not homogenous; the home has a number of rooms, we might say, some of which are normally invisible. But together they constitute a single domicile. Primal peoples are concerned with the maintenance of personal, social, and cosmic harmony. But the overriding goal of salvation that dominates the historical religions is virtually absent from them\cite{smith_worlds_1991}
\end{quote}

Impact processes need to include this lost element to more fully understand the contextual relationship of each investment. It is well known that people are unable to observe and recognise an event unless there is a prior experienced context and language. Peter Schwartz called this \emph{paradigm blindness}, (Schwartz 1994)\cite{schwartz_art_1996}. This is why our impact framework needs to be expansively systemic and dynamic and integrated with our moral core.  

To a great degree, our connection with the social dimension and Nature depends on our relationship and experience with it. \textsc{\lc{HRH}} Prince Charles suggests that: 

\begin{quote}
	We need to escape the straitjacket of the Modernist world view\ldots so that we can reconnect our collective outlook to those universal principles that underpin the health of the natural world and keep life's myriad diversity within the limits of Nature's capacity\ldots If people are encouraged to immerse themselves in Nature's grammar and geometry they are often led to acquire some remarkably deep philosophical insights\cite{charles_harmony:_2010}.
\end{quote}

Religion is often seen as contrary to science, which I disagree with. A key figure of the renaissance, Muhammad Abduh, wrote, 

\begin{quote}
	Religion therefore must be accounted a friend to science, pushing man[sic] to investigate the secrets of existence, summoning him[sic] to respect established truths, and to depend on them in his moral life and conduct(1932, p.11)
\end{quote}

This is why we also need to include some form of spiritual practise within impact, if for no other reason than to balance an otherwise imbalanced scientific approach.  

One area today requiring extreme vigilance is our reliance on technology without including the deeper self. We all know at a deep level that when we engage with, and do, the right thing we connect and draw on a spiritual energy which is otherwise unavailable. This is how important impact investments can be for you.  