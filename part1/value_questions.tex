\chapter{The value of questions}

\epigraph{I find intelligence is better spotted when analysing the questions asked rather than the answers given.}{Sir Isaac Newton\cite{develop_how_nodate}}

When working with high levels of information, such as in investment analysis, we must be attuned to the value of questions with stakeholders. Questions perform a function that cannot be made visible by working through information alone. Appropriate questions have the ability to draw out deeper, previously hidden insights. 
 
As a psychotherapist, I know a client cannot be helped or cured by anything I may say. Questions elicit the contextual answers that are right for each individual from the subconscious levels of our mind. This has been known for centuries within religious traditions; Judaism, for example, uses the term ‘midrash’ which is to question, to keep spiritual meanings open, often reflecting on a text or returning questions with more questions. 
 
In dialogue, limited by our own level of maturity and consciousness, we cannot find solutions or outcomes that go deeper than our own perspectives unless we include “the other”. This is what Thomas Aquinas hinted at: ‘Whatever is received is received according to the manner of the recipient’ (Aquinas 1a, 1, 9)  

It is by allowing and posing open questions that we can surmount these barriers and limitations. Modernism is often dominated by answering ‘How?’. We need to find balance and meaning from deeper questions, such as ‘Why?’. The Czech economist, Tomas Sedlacek suggests that this is how one can define modernism,

\begin{quote}
The era of scientific thought set a goal of pushing through a method of examining the world that would not allow doubt and would be free of any subjective, disputable dimension. Perhaps the most important character of the modern era has been the change in emphasis from the question ‘why?’ to the question ‘How?’. This shift is from essence to method. The scientific era has tried to demystify the world around us, to present it in mechanical, mathematical, deterministic and rational garments and to get rid of axioms that cannot be empirically confirmed, such as faith and religion	\cite{sedlacek_economics_2011} (p.171).
\end{quote}

These questions, however, have a structure and hierarchy to help us understand what levels of our neurology are being activated and analyse thought. The anthropologist Gregory Bateson initially conceived ‘The Neurological Levels\cite{levels_logical_nodate}’ to which I have added ingredients to highlight some of the areas inside each system. 

\begin{figure}\centering
	\includegraphics[width=.8\linewidth]{value_questions-p1}
	\caption{The Neurological Levels}\label{value_questions-p1}
\end{figure} 

We know that organisations need to operate at all levels but we also know that, depending on the type and nature of each firm, specific levels have to be fully engaged and activated. For example, consider the different requirements between a generic equipment manufacturing business versus a creative and innovative business which only has intellectual property. Looking at the value chain with its drivers and stakeholder analysis through the integral impact lens uncovers pivotal balancing items.  

The neurological levels concept was further developed by Dilts (1990, Ibid), inspired by the work of Gregory Bateson (a leading anthropologist, philosopher and seminal figure in the early development of \textsc{\lc{NLP}}) and particularly by his logical levels of learning construct (1972)\cite{paul_tosey_batesons_nodate}. For this model of abstracted levels of what we learn and how it affects us, Bateson drew on the logic of mathematician and philosopher Bertrand Russell. Taking his cue from Bateson, Dilts conceived a hierarchy of levels used by the mind to order its relationship with the world. Dilts linked these 'logical levels' to neurological functions and the structure of the brain --- thus, ‘neurological levels’!  

While the supposed 'logicality' of the model has been attacked, and Dilts' use of neurology is sometimes open to question, almost everyone who has worked with the model testifies to its power to describe what we might call ‘nominal level adaptation’. In other words, the match of Identity - towards the Values and Beliefs which flow from Identity - into the base layer of our Environment in which we find ourselves and can identify with.  

According to Dilts, the key to a healthy psyche is to align the levels from top to bottom so that the identity has the values and beliefs to acquire the necessary skills and knowledge in order to demonstrate behaviour appropriate to the environment. For example, someone who is a teacher (identity) would almost certainly behave differently in the classroom (environment) than when they are being a worshipper (identity) in a church (environment).  

Where neuroscientists have found Dilts' insight creates further understanding of functioning is at the levels of attribution in brain structures and our capacity to be phenomenally perceptive. This generates the relationship between values and beliefs, and skills and knowledge. Skills and knowledge (also known as capability) involve higher-level processing primarily in the cerebral cortex of the brain. Here mental maps are formed from sensory information, other mental representations, where imagination can form, and from which our plans are made. However, values and beliefs, which relate to the question, 'why?' are also associated with the limbic system and, in particular, the amygdala's stimulation of the hypothalamus. 

The limbic system's integration of information from the cortex and regulation of the autonomic nervous system means that physiological changes such as tingling of the skin and increase in heart rate often occur when we are operating at this level. However, the amygdala can react to certain sensory information faster than the cortex can evaluate it and plan, resulting in what Daniel Goleman calls 'emotional hijacking'. People can act before thinking (and without any sense of the consequences) when their values and beliefs are challenged.  

Categorizing information according to different levels is a way of making important distinctions in our experience. The influence is generally affected from top to bottom; in other words, changes made at one level affect all underlying levels. How? and Why? are therefore integrally connected – but how does this apply to us when we are working in finance?
  
Hermes’ \textsc{\lc{CEO}} Saker Nusseibeh in his paper\cite{nusseibeh_question_2017} suggests: 

\begin{quote}
In a lecture I heard some years ago, a philosopher asserted that science tries to answer the question ‘how’, while philosophy tries to answer the question ‘why’. In looking at the corpus of work produced by academics and practitioners on finance, it seems to me that most, if not all, are trying to answer the ’how’ question, but almost none attempt to answer the ‘why’ question. I think this is because finance as a discipline sees itself as an extension of economics; and economics, since the nineteenth century work of French economists such as Walras and later of Marshall, has been seen in essence as a science, and therefore this preoccupation with the ‘how’ question is a result of a spillover of that assumption.	
\end{quote}

Impact investments suggest that we do more than cover the usual finance and commercial business areas; as ‘scientific’ investors, we must develop the capacities that transcend the ‘How?’ and move to the heart for further and deeper questions that inform our language. As Terry Tempest Williams says\cite{couragerenewal_epigraph_nodate},: 
‘The human heart is the first home of democracy. It is where we embrace our questions. Can we be equitable? Can we be generous? Can we listen with our whole beings, not just our minds, and offer our attention rather than our opinions?’ 

In Man’s Search for Meaning\cite{frankl_mans_2006}, Victor Frankl explores the deepest areas of the human differences between ‘Why?’ and ‘How?’ within the context of a German concentration camp. Within such a unique – and hopefully never to be experienced again – environment, the ego is stripped down to its bare bones as our perceived identity collapses in on itself. Frankl found that, regardless of physical health, strength, age and other factors that would usually be related to longevity, how we give meaning in our lives was the preponderant factor. Frankl's credo became Nietzsche's famous phrase, his noble end peroration: ‘Those who have a 'why' to live, can bear with almost any 'how'‘ (Ibid. p. 95). 

With impact, we have the potential to build our own ‘Why?’ into something truly beautiful and beneficial for ourselves and our bigger ‘other’. If we do this, we become balanced and fully aligned with our ‘How?’. As such, there is a deeper layer to this ‘how’ question as Richard Rohr suggests: 

\begin{quote}
If we did not enter deeply the learning process of ‘how’, we will use our actions to defend ourselves, protect ourselves from our shadow, and build a leaden manhole over our unconscious. We will settle for being right instead of being holy and whole, for saying prayers instead of being one	\cite{rohr_dancing_2014}
\end{quote} 
  
Much of this development of perspectives comes only with maturity. As a natural progression of changing priorities and requirements, as we age, we must ask different questions than in our youth and mid-life, since the earlier questions no longer serve us. 

We can turn again to Parker J. Palmer writing, 

\begin{quote}
I won’t know the answer until I get there. But on my way to that day, I’ve found a question that’s already brought me a new sense of meaning. I no longer ask, what do I want to let go of, and what do I want to hang on to? Instead I ask, what do I want to let go of, and what do I want to give myself to?’ The desire to hang on comes from a sense of scarcity and fear. The desire to give myself comes from a sense of abundance and generosity. That’s the kind of truth I want to wither into	\cite{palmer_brink_2018}( pp. 26-27).
\end{quote} 

This more mature and generous state of mind is fundamental to seeing and hearing what is inside the Integral Impact dimensions.  Starting from the bottom, let’s now walk through Bateson’s neurological levels.

\smallskip
\textsf{Environment: \textsc{\lc{WHERE? \& WHEN?}} }
 
The level of environment includes obvious things like our surroundings, the external context, the Where? and the When? But it also embraces more nebulous elements like our social environment.

There is also our internal environment, which we create through our thoughts, feelings and sense of well-being. 

\smallskip 
\textsf{Behaviour: \textsc{\lc{WHAT?}} }
 
Behaviour is what we do --- or don't do. It involves both deliberate and 'accidental' actions, occurring at both conscious and unconscious levels. 

Issues on these levels relate to what is happening or being done, for example, actions necessary to carry out tasks like complete a project, write a report, start a new task, etc. 

\smallskip 
\textsf{Capability: \textsc{\lc{HOW?}} }
 
Capability is about the how and the how-to of life: the knowledge, skills, processes and talents that we may have both physically and mentally. 

These abilities may be inherent or learned. New skills can be learned and, with a positive attitude and desire, our capabilities can expand.  

\smallskip
\textsf{Beliefs and values: \textsc{\lc{WHY? }}}
 
Beliefs and values provide the criteria for judgement and action – the Why? – for individuals and organisations. Our beliefs and our values shape our understanding of why things are possible or impossible. They provide us with a rationale and drive our actions. 

As such, they also relate to a deeper, personal level which is linked to what we believe to be true and reinforces our motivation. For example: Do we believe the project will give us value? What factors are important to us? What value do we perceive in learning a new skill? 

\smallskip
\textsf{Identity: \textsc{\lc{WHO? }}}
 
Identity is the sense of self: who we are and how we describe and express ourselves. This could be our personal identity or a relationship identity. 

Psychologically, this area is felt to be most significant and will be the most well-defended. If someone feels criticized at this level, they will tend to react very strongly! 

\smallskip
\textsf{Spirit: \textsc{\lc{FOR WHOM/FOR WHAT?}}} 
 
This level relates to a bigger picture where questions about some larger purpose come into play, such as our personal mission in life or our degree of ‘passion’, e.g., what we want to achieve, what contribution we want to make. For us as individuals, this often means the spiritual. It takes us into questions about our mission and vision. 

The dimension of mission and vision can also apply to groups. Because these give deep meaning to our life by answering questions about for whom and or for what, they can be emotionally supercharged and heavily defended or aggressively promoted, as in certain religious groups. 

The Neurological levels can help: 

\begin{itemize}
	\item Clarify how we perceive a situation, e.g., our thoughts and ideas, what the real issues are 
	\item Highlight at what level work needs to be done to achieve change, or how we may need to intervene or interact 
	\item Identify the genesis of a problem within an organisation or relationship to help find a solution and move forward
\end{itemize} 

Whilst learning and change can occur at different levels, change is usually easier in the context of the first level ‘environment’. Change at a higher, logical level usually impacts on the lower levels. However, change at a lower level will not always affect change at a higher level. Therefore, to solve a problem at one level, a change may be required at a different level first. We may want to change our behaviour but we struggle because the change may be linked to another logical level which we need to address first. 

As an example, let’s look at the simple sentence: ‘\emph{I can’t do that here}’. 

\emph{I} can’t do that here. The emphasis is on the \textsc{\lc{IDENTITY}} level: who could do the task, or what could you do? 

I \emph{can’t} do that here. The emphasis is on the \textsc{\lc{BELIEF}} level: why, what factors are important to help you continue? 

I can’t \emph{do} that here. The emphasis is on the \textsc{\lc{CAPABILITY}} level:  how, do you need additional skills or knowledge to proceed?
 
I can’t do \emph{that} here. The emphasis is at the \textsc{\lc{BEHAVIOURAL}} level: what actions can the person do? Does the task have a positive intention and link with your personal development? 

I can’t do that \emph{here}. The emphasis is on the \textsc{\lc{ENVIRONMENT}} E.g.: where, when or with whom could you take action? Where do you need to work? What time of the day will be best? Where do you need to be to do the task?  

Moving these types of questions from the self to the organisation, Peter Drucker with Jim Collins (2008) suggest starting with a ‘self-assessment’ as a process and method for what you are doing, why you are doing it, and what you must do to improve the organisations performance.’ 

They suggest asking five essential questions:
  
\begin{enumerate}
	\item What is our mission? 
	\item Who is our customer? 
	\item What does the customer value?  
	\item What are our results?  
	\item What is our plan?  
\end{enumerate}

This self-assessment method is designed to bring real meaning to a firm whilst meeting the growing needs for solutions for the environment and social-sector organisations, all of which need to focus on a clarified mission, demonstrate accountability, and achieve measurable and reportable Impact results to stakeholders. 
 
As you may already have realised, whilst these questions are ostensibly simple, you and your organisation might spend a lifetime ascertaining their meaning and implications. Needless to say, these are equally important for any investee company and can form an integral part of our due-diligence process.  

A short summary follows: 

\mypar{Mission:} As a construct of our mutual core desires, this needs to be constructed from our deep values and beliefs in combination with the firm’s fundamental purpose. For some firms, their clarified mission is only skin deep (e.g., around profit maximisation for shareholders in extractive industries) as such missions create no unity, and contradicts deeply held personal values. Mission must, however, be combined with how the firm operates in its practises, structures, processes and cultural norms. These, in turn, need to be aligned with its tactics and strategy, which must also be flexible enough to change with market conditions. In this way, we have a constant core of principles that act as an anchor, plus a changing set of operational and implemented drivers which are integrally informed by the mission.   

\mypar{Customer:} Here we look at our primary customers and our secondary customers who are the broader decision makers, influencers, and initiators of a purchase. As any parent knows from experience when their child implores them to purchase a toy (without which life would be miserable!), influence can be the key trigger. In addition, we have supporting customers from whom we may take advice on our purchases. The rule is that there is always someone other than the prime customer who may say No. 

In addition, we must know our customers inside-out, including their deeper motivation and their value proposition. Theodore Levitt’s famous suggestion that someone who buys a drill does not actually want a drill but is trying to make a hole in the wall is pertinent here.  

\mypar{Customer value:} This question is strongly linked to organisational knowledge creation as customer value can reside deep inside motivation, about which most firms will make significant assumptions. A starting insight might be whether a value activates neurology which creates and reinforces energy and value to flow across the types of customers. What is the firm’s process to ascertain the value proposition, and how is this linked to the impact proposition? 

\mypar{Results:} These must be created and clarified through the lens of how we define a firm’s success as outcome objectives, targets and goals. This includes quantitative and qualitative forms of measurement both now and in the future. These outcomes and outputs also ask constantly what needs to change to fully align any business model to its market place. Separately, but intrinsically linked, are how leadership and management defines what constitutes development and results in accordance with the firm’s strategy. Importantly, this requires a firm to understand the difference between a person’s individual contribution i.e., anything within a person’s control and sphere of performance versus riding along on extrinsic waves of performance due to external market drivers and factors. For the impact investor, this is key in directing the management and leadership of the investee to become more ‘integral’ rather than simply linking performance to a share price or exit. 

\mypar{Plan:} We are back to ‘begin with the end in mind’ regarding a cross-section of goals, which flow from our identified mission. Here we need to look at the overall alignment between the ‘plan’ and our finances and operations, organisational areas that are often full of contradictions and mixed messages. Ownership and economic performance participation are also intrinsic cultural and operational drivers toward which management and staff will align with its vision and mission. Ownership implies executional responsibility and accountability for any goal and plan of action. None of the above can be fulfilled (or even held static) without transformational management on which we will expand on later.  

Whilst neoclassical economists argue that nothing can have value without being an object of expected ‘marginal utility\cite{wikipedia_marginal_2020}’, the classical economists, including Adam Smith, David Ricardo and Karl Marx, argue that value is determined by intrinsic labour. In Lady Windemere’s Fan (1892) Oscar Wilde’s Lord Darlington in Act 3 suggested that a cynic was ‘a man who knows the price of everything and the value of nothing\cite{wilde_lady_nodate}.' Let us try to move away from such cynical suggestions. David Graeber suggests schools of thought on value that can converge in the present\cite{graeber_toward_2001}. 
 
\begin{enumerate}
	\item Sociological: conceptions of what is ultimately good, proper, or desirable in human life. 
	\item Economic: the degree to which objects are desired, particularly as measured by how much others are willing to give up to get them. 
	\item Linguistic: dating to the structural linguistics of de Saussure (1966), and might be most simply glossed as ‘meaningful difference’. 
\end{enumerate}

In today’s market economy, social constructs have been eroded by a more transactional and distanced approach to value creation. This has implications for our Impact approach if we have an Impact outcome that includes restoring the lost cohesion and fabric within a community and local landscape for a grounded business.  

Karl Polanyi\cite{polanyi_great_2001} (p. 43), states that there are three different forms of value in a market economy,  

\begin{enumerate}
	\item Commodity value: The intrinsic value of a good or service under market conditions.  
	\item Exchange value: The representative value (not price) when traded and exchanged against other goods and services.  
	\item Experiential value: The altruistic and experienced social value derived from providing a good or service outside of price and monetary compensation, e.g. blood donations, which decline in numbers if monetarily compensated. 
\end{enumerate}

If we are to become fully integrated Impact investors, we need to expand our capacity to feel and experience value. Even firms transitioning from finance-only impact returns into Impact 1.0 may do so badly, and without any real conviction. This will require their management to dig into possibly untapped capacities, given that any additional impact at any level has cost implications for firms.  

However, as discussed, even if the prime driver is for firms to follow the market and clients into Impact, the experiential value stems from clients willing to contribute to the bottom line. A distinguishing characteristic of the market society is that humanity's economic mentality has been changed as a result of commoditisation and financialisation, where we see transactional parts as separate events. Prior to ‘the great transformation’, people based their economic needs and requirements on human values such as reciprocity and redistribution which connected across personal and communal relationships. Competitive markets were created that undermined these earlier social needs, replacing them with formal institutions and corporations that promoted a self-regulating, market economy. 

The expansion of capitalist institutions with an economic liberalist mindset not only changed the operating legal frameworks but fundamentally altered mankind's economic and social relations. Prior to the great transformation, markets played a very minor role in human affairs and often were not capable of setting prices because of their small size, the distance between parties and lack of information. It was only later that the myth of our propensity toward rational free trade became widespread. However, Polanyi asserts instead that ‘man's economy, as a rule, is submerged in his social relationships (Iid, p.48). He therefore proposes an alternative ethnographic economic approach which he labelled ‘substantivism’, as opposed to ‘formalism’.   

For Polanyi, these changes destroyed the basic social order that had previously reigned. Central to the change was that factors of production, like land and labour, could now be capitalised and sold on the market instead of allocated according to tradition, redistribution or reciprocity, or used in the commons. He emphasises the scope of the transformation because it was both a change of human institutions and human nature (Ibid. p. 35-36). And more recently, through globalisation and the attendant financialisation of our economic resources to generate more returns than just from production. Many aspects of finance have become completely separated and disconnected from human nature with several forms of finance being highly extractive and costly to human life.  

Polanyi is important for Impact investments since his thesis looks at the potential schisms in social impact and aims to highlight the lost social structures within a particular culture. This aspect is key when we operate and invest in cultures with stronger ties to social bonds, including reciprocity and redistribution. Investments for them need to be designed to at least maintain the local social fabric and at best be a catalyst for regeneration and growth:

\begin{quote}
	Reciprocity is a matter of keeping the gift in motion through self-perpetuating cycles of giving and receiving\ldots Through reciprocity the gift is replenished. All of our flourishing is mutual\footnote{Robin Wall Kimmerer. Braiding Sweetgrass}
\end{quote}  

Without entering the political debate on the validity of redistribution and finance as both an enabling and extractive industry, it is clear that for some investments there is a trade-off between financial returns and making contributions to impact areas which support the system but where there is less clear linkage to a firm’s economic performance (e.g., community building and activation).  

At the core are the words suggested by Rabbi Hillel the Elder in his Ethics of the Fathers, 1:14, more than two millennia ago\cite{astor_me_nodate}:  

\begin{itemize}
	    \item If I am not for myself, who is for me? This is the story of the unique self, how I need to learn, respect, honour and teach myself before others can, and the golden rule.  
	\item If I am only for myself, what am I? This is the story of the collective us and how we weave our shared values into a togetherness with our lives and the ‘other’.  
	\item If not now, when? This is the story of the present which is always a constant. The rest is a construct by our ego, minds and emotions.
\end{itemize}  

The stories that we tell ourselves about our world are our perceptions, which become our projections. That perception is projection is a well-established psychological axiom, but we forget that this acts as double-sided mirror that reverberates back and forth to create justification and our take on truth. This is why our narrative for our Impact philosophy is critical for our success.  

This leads us into how the dynamic of such stories shape our views on how we work within Impact. One of our key attributes as humans is our developmental level of curiosity; this is one of our key drivers for growth, given that it emanates from our capacity for both love and courage. Curiosity is one of the greatest gifts we possess as humans; it is curiosity that drives most of our endeavours, innovation and risk taking. Filling the gap that our curiosity has created, is one of the most satisfying feelings we can experience. When our curiosity is left unsatisfied or incomplete, we often go to some lengths to close the loop.  

Scientists have known this phenomena for decades and it has influenced our story telling for millennia. Building an investment thesis is equally about creating a story that not only convinces and can be validated, but also persuades and can close the various loops and narratives that make up our creative story. In this context, curiosity is an important ingredient not only to evoke but also to expand narrative accounting, measurement and reporting.  

In his paper ‘The Psychology of Curiosity’ (1994), Professor George Loewenstein highlights four ways to involuntarily induce curiosity in humans:  

\begin{enumerate}
	\item the ‘posing of a question or presentation of a puzzle’  
	\item ‘exposure to a sequence of events with an anticipated but unknown resolution’  
	\item ‘the violation of expectations that trigger a search for an explanation’ 
	\item knowledge of ‘possession of information by someone else’.  
\end{enumerate}

This reads as the perfect structure for any integral investment thesis and beautifully fits into our four-worlds model. 
 
\begin{enumerate}
	\item South: The question is really whether this is Grounded (\textsc{\lc{GENE}}) and Rooted (4Rs) in our Calling (4Cs). If it is not, it will not create the energy for us to care in the first place.
	\item East: This is about whether this exposure of events Resonates (4Rs) within our Context (4Cs) which creates the Emergence (\textsc{\lc{GENE}}) of the anticipated gap which we think needs to be resolved.   
	\item North: The grasping, searching and Navigating (\textsc{\lc{GENE}}) of the Relevant (4Rs) knowledge and understanding that violates its expected closure and that can co-creatively (4Cs) fill the gap for our explanation and find any possible solutions. 
	\item West: The gap is filled by the co-created Contribution (4Cs), which now closes the gap in our Rationale (4Rs) so we can fully Effect (\textsc{\lc{GENE}}) our investment story/thesis. 
\end{enumerate} 

We know from our own experience that information gaps gnaw at our need to find the answers. That is why I³ is designed and structured in this iterative and dynamic way so that the required impact thesis that leaves no unconscious or deliberate gaps that later need filling.  

The dualistic mind presumes that if you criticize something, you don’t love it. Wise prophets would say the opposite but institutions prefer loyalists and sycophants ‘company men/women’ to prophets. We’re uncomfortable with people who point out our imperfections.  

\epigraph{One does not become enlightened by imagining figures of light, but by making the darkness conscious. The latter procedure, however, is disagreeable and therefore not popular.}{Carl Jung (1967, p265-266)}

Human consciousness does not emerge at any depth except through struggling with its shadow. It is in facing our own contradictions that we grow. It is in the struggle with our shadow self, with failure or with being wounded that we break into higher levels of consciousness. People who learn to expose, name and still thrive inside contradictions are what I would call prophets; they have transcended knowledge into wisdom.  

Let us now explore the creation of knowledge and how this may relate to our Impact approach.    