\chapter{Re-defining success}

\epigraph{Success, like happiness, cannot be pursued; it must ensue, and it only does so as the unintended side-effect of one’s personal dedication to a cause greater than oneself}{Viktor Frankl}

Our views on ‘success’ and what we are willing to do to achieve it, are highly dependent on what stage of our life we are at. Personally, I can relate to the early years of being very goal-orientated in achieving results, whether in school or sports;  later in life, with a young family to house and feed, ‘success’ related to what my family considered it to be. Then there was how I needed to perform to become ‘successful’ in my chosen career, where recognition came with appraisals, advancement and remuneration. Whilst this created a comparative hierarchy, it was difficult to define real ‘success’ since much depended on the nature of our clientele.  

At this stage of my life, I started to recognise conflicting messages between family and work. As my career and family has matured, I can now afford to re-define my own formula for success in accordance with my situation in life and my needs. I now find that an increasing amount of ‘success’ includes internally defined parameters based on external ‘doings’.  
Let us look at this through a lens with which you are probably already familiar (figure~\ref{defining_success-p1}) 

\begin{figure}\centering
	\includegraphics[width=.8\linewidth]{defining_success-p1}
	\caption{Psychologist Abraham Maslow’s hierarchy of needs}\label{defining_success-p1}
\end{figure} 

Depending on our individual circumstances, we will have fulfilled all of these to various degrees. We will also have identified our levels of needs versus shortfalls. Chronological human development suggests that, in general, we achieve these levels in stages during our lives. This implies that we will have different views and objectives to fulfil depending on these stages, and these may need to be taken into account when designing an impact objective.  

Most obvious is that in the first half of life we need to cover more of our basic needs by earning money, and if possible, generating capital and savings. Whereas, in the second assuming things have gone reasonably well (which may be a big assumption for some), we realise this does not necessarily fulfil us. We can then afford to make more informed and aligned choices and we opt for greater self-realisation. We can find this when we work creatively with impact.  
Maslow\cite{maslow_toward_1999} (p.145) suggests that: 

\begin{quote}
	To summarise, \textsc{\lc{SA}} [self-actualising] creativeness stresses first the personality rather than its achievements, considering these achievements to be epiphenomena emitted by the personality and therefore secondary to it. It stresses the characterological qualities like boldness, courage, freedom, spontaneity, perspicuity, integration, self-acceptance, all of which make possible the kind of generalised \textsc{\lc{SA}} creativeness, which expresses itself in creative life, or the creative attitude, or the creative person.
\end{quote}

The word ‘epiphenomena’ means: ‘a secondary phenomenon accompanying another and caused by it; specifically: a secondary mental phenomenon that is caused by and accompanies a physical phenomenon but has no causal influence itself’ (Merriam-
Webster)\cite{EPIPHENOMENON_definition_nodate}. It is a by-product of something else. In this case, Maslow tells us that creativity is simply the second stage of being a virtuous individual; this is what our organisations need to espouse and nurture. As Warren Bennis suggested ‘There are two ways of being creative. One can sing and dance. Or one can create an environment in which singers and dancers flourish’(Whitaker \& Lumpa 2014, p. 148). 

If through their processes firms can help a person to develop the character traits of boldness, courage, freedom, integration and self-acceptance, such a person will contribute positive creativity and heart into any impact process. As Eleanor Roosevelt (2012) wrote, ‘Success must include two things: the development of an individual to his utmost potentiality and a contribution of some kind to one's world.’ Creating impact success is, therefore, the key ingredient in nurturing ourselves and helping us to build our legacy of success. The Bauhaus architect Walter Gropius, in a letter to his students in 1964 put it as follows, ‘If your contribution has been vital there will always be somebody to pick up where you left off, and that will be your claim to immortality’\cite{antoniades_introduction_1976}(p. 68).

We must learn to embrace change, ambiguity and learning from whatever source. As has been discussed, this is an inside-out process, much like a diamond cutter must polish away the imperfections of the rough stone to uncover its inner splendour and beauty. Idowu Koyenikan\cite{koyenikan_wealth_2016} says: ‘Success comes from the inside out. In order to change what is on the outside, you must first change what is on the inside'. 
 
Maslow suggests that,

\begin{quote}
	The muscular person likes to use his muscles, indeed, has to use them in order to self-actualise, and to achieve the subjective feeling of harmonious, uninhibited, satisfying functioning which is so important an aspect of psychological health. People with intelligence must use their intelligence, people with eyes must use their eyes, and people with the capacity to love have the impulse to love and the need to love in order to feel healthy. Capacities clamour to be used, and cease their clamour only when they are used sufficiently \cite{maslow_maslow_2000} (p.41).
\end{quote}

Maslow said this in another way as well: ‘What one can be, one must be.’ Here we have clear instruction on what we need to do with our Impact desires and ambitions.  

How can we move towards affluence. When we think of affluence, we tend to think of cars and houses and baubles of all kinds. But Eric Butterworth\cite{butterworth_spiritual_2001} writes,

\begin{quote}
	Its literal meaning is ‘an abundant flow,’ and not things at all. When we are consciously centred in the universal flow, we experience inner direction and the unfoldment of creative activity. Things come too, but prosperity is not just having things. It is the consciousness that attracts things. The goal should not be to make money or acquire things, but to achieve the consciousness through which the substance will flow forth when and as you need it.
\end{quote}

If we ask people what they want in life, many would answer ‘happiness’. But is our happiness coming from internal or external experience? And to what extent do we engage in activities we would rather avoid versus things we enjoy and would want more of? When we adjust our goals towards making a contribution, the quality and enjoyment of our lives improves. With impact we have the opportunity to improve the lives of others; when we do this, our own lives automatically improve.  
Money is both related and unrelated to our happiness. On the one hand it is inert, similar to static energy; it has no real intrinsic value and is useless on its own. On the other hand, it can be activated through an outside force and become embedded with the capacity to generate and catalyse energy beyond itself.
  
As such, money is another clear example of ‘cause and effect’. It also has a complexity that we have inherited along the way. For many people, a significant disconnect occurs between the desire for money and the need to make it. For example, some people  dislike their job but it pays for a level of need and lifestyle; essentially there is a ‘package’ between the things they need to do to make money versus the ‘happiness’ they get from spending it. We don’t need to make judgements about this choice but individually we need to understand our own ‘happiness’ strategy and take responsibility for evolving and develop it in healthy and conscious way. However, as Tom Brokaw reminds us: ‘It is easy to make a buck. It is a lot tougher to make a difference\cite{quote_tom_nodate}.’ 

Here’s the rub: if we expect happiness to come from a precondition being satisfied, we are often disappointed and disillusioned, particularly if these conditions come mainly from external sources. It may seem like a paradox but the relatively poor country of Bhutan scores more highly than the \textsc{\lc{US}} and the \textsc{\lc{UK}} in term of happiness. Over the past decades, they have developed a ‘Gross National Happiness Index’ (\textsc{\lc{GNH}}) that includes nine domains: 

\begin{itemize}
	\item Psychological well-being 
	\item Health 
	\item Education 
	\item Time use 
	\item Cultural diversity and resilience 
	\item Good governance 
	\item Community vitality 
	\item Ecological diversity and resilience 
	\item Living standards 
\end{itemize}

Whilst each of these contains many separate ingredients and combinations, each are considered important as a contributor to what we call ‘happiness’. Whilst money touches all of these in some form, the core drivers and outcomes are beyond what money can buy; at best, money is the catalyst and facilitator for something we may value.  

From a psychological perspective, one can assert that happiness is more of a surface objective that seems like an attractive goal but we often lack real personal understanding of its depth to create any needed change. Importantly, if we reflect on our own experiences we might find that the happiness we found was ephemeral; more often than not, its residual values had less energy the next time we experienced them. So what is going on here? If happiness is not a goal in itself, then what is? To find out more we have to delve deeper. 

Whilst this is a highly personal journey and will be different for each of us, each journey has some common ingredients. But sometimes we need to remove something or at least experience the threat of losing something to appreciate what we have.  As the lyric of a famous song by Kelly Clarkson goes, ‘what doesn’t kill you makes you stronger’. Hang on we might say, ‘Do I have to have near death experiences to be happy? And if so, I’m out of here\ldots’ 

Some of us may go through physical near-death experiences; most of us will go through metaphorical ‘small deaths’ of our beliefs, pre-conceived expectations and the small death of our false self and ego. Underneath lies the real self and our connection to something bigger than ourselves. Here we find the true soil of the soul in which we can grow our understanding of our identity and our purposes in life. As impact investors, this is where the connection to real impact lies; this is where the lens and prism are formed into what we see and do.    
 
Throughout my career, I have had to re-frame my understanding of my work purpose many times, not only to meet my own needs but also because of how this related to others. Our capacities to see the ‘other’ is a product of seeing ourselves ‘less’ in all our affairs. In my view, impact investing provides an ideal platform and venue to work on this aspect. One could even argue that this is the purpose of Impact investing, i.e., to include more than just our own needs and wants. When we do, we connect with something bigger that nourishes the very soil on which we stand. As Rick Warren\cite{warren_purpose_2013} writes, ‘True humility is not thinking less of yourself; it is thinking of yourself less.’  

So how does this relate to ‘success’? I have contemplated many variations of what constitutes success in life. If we can consider ourselves to be ‘successful’, we may find that elusive ‘happiness’ in our lives. If so, success must be important and highly relevant to what we do and how we do it. But do we have a clear understanding of what success means for us? Is it really ours, or has it been superimposed on us by others?  

Our culture, and our family of origin, aim to tell us what ‘success’ is. The ‘marching orders’ which might include other’s antiquated belief systems set in historical contexts and cultures that come in many forms, overt and covert, are often persuasive, subliminal and attractive as we seek answers to our questions about who we are and what ‘tribe’ we want to belong to. It often takes time to experience the dissonance necessary to challenge, and have the courage to challenge these so called ‘marching orders’ if we are to find our own life within life itself.  

Let us look more deeply through ‘The Stockdale Paradox’ which suggests: ‘Confront the brutal reality of the situation you’re in\ldots\ while never losing faith that you will prevail in the end'\cite{concepts_jim_nodate}.
  
‘The Stockdale Paradox’ relates to the journey that Walter Stockdale made whilst a prisoner during the Vietnam War. In Jim Collins’ book\cite{collins_good_2001}, he asks Stockdale

\begin{quote}
	‘‘Who didn’t make it out?’.\\
	 ‘Oh, that’s easy,’ Stockdale said. ‘The optimists.’\\
	‘The optimists? I don’t understand,’ Collins said.\\
	‘The optimists. Oh, they were the ones who said, “We’re going to be out by Christmas.” And Christmas would come, and Christmas would go. Then: they’d say, “We’re going to be out by Easter.” And Easter would come, and Easter would go. And
	then 
	Thanksgiving, then it would be Christmas again. And they died of a broken heart.’\\
	Then he turned to Collins and said, ‘This is a very important lesson. You must never confuse faith that you will prevail in the end --- which you can never afford to lose --- with the discipline to confront the most brutal facts of your current reality, whatever they might be’
\end{quote}

Impact investments allow us the opportunity to confront and examine not only our own reality as stewards of capital but also our responsibility for our impact outcomes. As Socrates reputedly said: ‘The unexamined life is not worth living\cite{wikipedia_unexamined_2020}.’  
To create a real and worthy life journey one must create a mirror in which we can thoroughly examine one’s own life; out of this examination comes an awareness of the true nature and direction of one's soul that, in turn, needs to be satisfied by the exterior manifestation of our outcomes. Our psychological foundation is comprised of many things, including core complexes that we wish we could eliminate but cannot easily be dealt with. Indeed, a good dose of long-term therapy can't eliminate them either. According to James Hollis, therapy can help us better observe our core complexes and their impacts.

This, in turn, will help the individual become a more conscious person with a more mature vision of life: ‘Therapy will not heal you, make your problems go away or make your life work out. It will, quite simply, make your life more interesting.’ 
Thus, the examined life is more interesting, with the resulting corollary, that: 

\begin{quote}
	Consciousness is the gift and that is the best it gets….The theologian Paul Tillich once observed that the chief curse of our time is not that we are evil, though often we are, but that we are banal, superficial. The recovery of depth will never come through an act of intellect, unless that intellect is in service to wonder. We can recover depth, however, by opening ourselves to the numinous which nods at us and invites us. We can also use our imaginative power to seize such moments of beckoning and the images which rise spontaneously from them\cite{hollis_creating_2001}(p.30)
\end{quote}
  
If the results of our choices, or un-reflected actions, are akin to Greek tragedy or drama, then we might also ask what is the myth that best represents our life journey and best explains our existence to us? 

Hollis writes that myth: ‘as it is used here, refers to those affectively charged images (‘imagos’) which serve to activate the psyche and to channel libido in service to some value'\cite{morrissey_stephen_nodate}.

Are we living second-hand lives, the unresolved cast-offs of our parents' experience? Are we living reflectively or reactively? 

This is also where stage development is important, as I need to understand the differing facets between the activities of the first half of life versus the second. For me, there is now a very clear distinction between these two (and possibly many more) stages. 

On one level, the first half seems now more like a preparation and experimentation period akin to a dress rehearsal; in contrast, during the second half I can correct so-called ‘mistakes’ and align myself more towards my interior needs rather than being driven by external forces and expectations. 

I have, in effect, regained sovereignty over myself to shape my kingdom as I see fit. For Hollis: ‘The larger life is the soul's agenda, not that of our parents or our culture, or even of our conscious will.’ This takes time, effort and courage to disentangle and break free. 

C.G Jung in his concept of how we all move into individuation states that it: ‘has to do with becoming, as nearly as one can manage, the being that was set in motion by the gods.’ This, then, at a practical level is a process of psychological and spiritual maturity as we enter the world and express ourselves in the exterior dimension.  

A test for this maturity lies in one's capacity to deal with anxiety, ambiguity and ambivalence. Hollis writes: ‘The more mature psyche is able to sustain the tension of opposites and contain conflict longer, thereby allowing the developmental and revelatory potential of the issue to emerge.’ 

I can recall many times when anxiety about money directed me to change jobs, only to find out that later that this did not fix the problem and the underlying anxiety persisted.  Following this, we also examine and re-shape our attitudes and practises for the second half of life. These include: ‘amor fati’, the necessity to accept and love one's fate; that the examined life is essentially one of healing the wounds from the past; that the examined life is also healing for our ancestors.  

In Bert Hellinger’s work and therapeutic methods best known as Family 
Constellations we can experience such intergenerational links, identify our own role in our history and re-shape its impact on our future. Our unconscious is the primary authority in our lives. Individuation lies, in part, in reflecting upon the processes of the unconscious mind and regaining sovereignty over how this control function exerts power, influences, and affects our outer world and its relationships.   

We can also refer to the myth of Oedipus as reflective of our human condition and journey. How did Oedipus live out the second half of his life? We may each have our own personal myth to discover, a myth with which we identify and which gives our life substance, meaning, and depth. Oedipus, however, is an archetype that represents every person in his/ her flight from the darkness at his/her core to the discovery of soul and meaning in his/her inner and outer worlds. After Thebes, and after the stunning humiliation of midlife, Oedipus spends his final years in humble wandering, contemplating what the gods wished him to know. He un-learns, learns and absorbs during his difficult exile to Colonus, where he is finally blessed by the gods for the sincerity of his journey. It was not so much that he had created his life than that he finally allowed life to create him, as the gods had actually intended. The price to pay for this gift, both precious and perilous, was exile and suffering; however, the price of not finding his calling was ignorance, lack of meaning and annihilation of the soul. Suffering, therefore, is also a function of lack of ability to find our true purpose and meaning and, in the process, individuate. Impact stewardship should aim to alleviate this.  

Worth mentioning is the difference between inner versus outer control and power:
 
\begin{quote}
	On September 9, 1965, I flew at 500 knots right into a flak trap, at tree-top level, in a little A-4 airplane---the cockpit walls not even three feet apart---which I couldn’t steer after it was on fire, its control system shot out. After ejection I had about thirty seconds to make my last statement in freedom before I landed in the main street of a little village right ahead. And so help me, I whispered to myself: ‘Five years down there, at least. I’m leaving the world of technology and entering the world of Epictetus\cite{stockdale_courage_1993}(p.7)
\end{quote}

It is hard for us to imagine getting shot down while flying a combat mission, knowing that we will be beaten and tortured and imprisoned and then, in the remaining seconds of our freedom, to remember the wisdom of Epictetus: ‘‘As I ejected from that airplane was the understanding that a Stoic always kept separate files in his mind for (A) those things that are ‘up to him’ and (B) those things that are ‘not up to him.’ Another way of saying it is (A) those things that are ‘within’ his power’ and (B) those things that are ‘beyond his power’’(ibid) It is difficult to imagine Stockdale, floating down into impending doom, reminding himself that NOTHING outside of his mind can determine how he will respond.  

Here’s a passage from Enchiridion that captures this perspective: 

\begin{quote}
	‘Of things some are in our power, and others are not... examine it by the rules which you possess, and by this first and chiefly, whether it relates to the things which are in our power or to the things which are not in our power: and if it relates to anything which is not in our power, be ready to say, that it does not concern you (2016, Section 1).
\end{quote}

This reminds me of Viktor Frankl who endured the horrors of a World War II concentration camp. In his book\cite{frankl_mans_2006}, he states:

\begin{quote}
	Everything can be taken from a man but one thing; the last of the human freedoms, to choose one’s attitude in any given set of circumstances, to choose one’s own way (p. 86).
\end{quote}  

This attitude comports with the realm of the second part of life. As Frederic Laloux writes,

\begin{quote}
	All wisdom traditions posit the profound truth that there are two fundamental ways to live life: from fear and scarcity or from trust and abundance\cite{laloux_reinventing_2014} (p.15).
\end{quote}  

Finding such answers means asking meaningful questions, which we will address 
later.  