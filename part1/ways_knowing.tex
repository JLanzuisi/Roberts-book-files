\chapter{Ways of knowing}

\epigraph{I wash my hands of those who imagine chattering to be knowledge, silence to be ignorance, and affection to be art.}{Khalil Gibran}

This is the realm of epistemology, defined as the theory of knowledge, especially regarding its methods, validity and scope, and the distinction between justified belief and opinion.  

The key understanding for impact is that, in our search for reality in each dimension, we develop additional knowledge beyond finance. We need to understand the component parts of knowledge acquisition and retention that exist in different formats from those we traditionally find in finance and economics. It’s where we move and create knowledge throughout the integral realms since knowledge only gets embedded if it moves through the heart to the head and into our hands i.e., through action. 

In its identification with the ego, the mind cannot, by definition, fully comprehend reality; if it could, it would immediately dissolve upon recognising its own illusory nature.  

The neurobiologist Candice Pert (1986) argues that whilst the brain resides inside the cranium, the mind resides throughout the body. If true, we need to develop new neuropathways that help inform the whole system of knowledge and understanding away from our predominant mode of scientifically using the brain. This is important, not only to  understand our maps (and dogma) around finance but also in opening the heart for information, whose capacity is infinitely greater in holding ambiguity and difference without entering brain dominated areas of conflict, right/wrong arguments and thereby shutting down primary intelligence from the other.  

The severe limitations of our knowledge-creating institutions, such as conventional colleges and universities, are well understood. Such institutions create and train epistemically-obedient, highly-select members. They act as gate keepers to control what types of knowledge making are allowed, celebrated, disavowed, and devalued. They often preserve the past, while only valuing the quick easy and measurable. Institutions that look at modernity as the only solution are removed from natural principles and are unlikely to develop the types of knowledge need for real impact, so we must look elsewhere.
  
Kant (1988) proposed four fundamental questions:  

\begin{itemize}
	\item What is the human being? (our relational ‘South’) 
	\item What can I hope? (our inspirational ‘East’) 
	\item What can I know? (our knowledge creating ‘North’) 
	\item What ought I to do? (our action oriented ‘West’)
\end{itemize} 

However, much debate exists around the knowledge of subject and object and the space of presence we travel between the two. For example, what can we actually know about an object through only improving our own presence? Our presence is only known by some influence, interaction, and presence from the other side. As such, it becomes mainly a subject-to-subject, knowing and creating the knowledge of an object, and never just a straight line from subject to object.  

Here we begin to see increased dimensional diversity, which for us is a personal journey and formed within our own context and culture. Whilst the Western world draws almost exclusively on the intellectual ‘North’, Impact requires a more complimentary, balanced and deeper approach, with the integral approach and \textsc{\lc{GENE}} starting in the ‘South’.  

This impact practise may then become both our vehicle and fuel for the journey in which we can deepen our knowledge and soften the hard edges that have built up through our life experiences.  

Kant reminds us that: ‘He who is cruel to animals becomes hard also in his dealings with men. We can judge the heart of a man by his treatment of animals’ and Kant also suggests that we need to mature our knowledge through practise ‘Science is organised knowledge. Wisdom is organised life’ (1997, p. 37).  Ultimately impact can only be created by knowledge leading to wisdom. We are mindful of T.S. Eliot, who asked: ‘Where is the wisdom we have lost in knowledge? Where is the knowledge we have lost in information?’%\footnote{\url{https://thehypertextual.com/2012/12/10/ts-eliot-and-the-pyramid-of-knowledge-inorganisations/}}.

Let us now go a little deeper. Richard van de Lagemaat\cite{lagemaat_theory_2011}, says: 

\begin{quote}
The main question in the theory of knowledge (\textsc{\lc{TOK}}) is ‘how do you know’? The course encourages you to think critically\ldots\, in\-volving such things as good questions, using language with care and precision, supporting your ideas with evidence.’ 
The theory of knowledge includes exploring the emotional, intellectual, sensory, intuitive, imaginative, experiential, relational, language, memory, faith, and physical aspects of how we, as humans, develop our capacities in each of these areas.	
\end{quote}  

These knowledge areas are the component parts of the propositions that we hold and justify as ingredients for our impact \textsc{\lc{TOK}} and decision-making processes. Consequently, we need to be cognisant about how they are constructed inside the Impact approach.   

\begin{figure}\centering
	\includegraphics[width=.8\linewidth]{ways_knowing-p1}
	\caption{Propositions}\label{ways_knowing-p1}
\end{figure}

This is true both for ourselves and also our investees in that: ‘In an economy where the only certainty is uncertainty, the only sure source of lasting competitive advantage is knowledge\cite{nonaka_managing_2008}'. 

If we expand the diagram above, we enter the realms of the possibility of knowing, which extend out from the knower into ways of knowing and areas of knowledge into how we link concepts, which gives us our neurological constructs. Given our differing neurology and perceptional filters, there is little wonder that we all end up in different places – but this is also our strength as contributors within the organisational system.  

As David Bohm suggests: ‘The ability to perceive or think differently is more important than the knowledge gained.’ This leads us into further possibilities of knowing which is a dynamic to expand, and can be included on, the map we are creating for our Impact. 

 \begin{figure}\centering
 	\includegraphics[width=1\linewidth]{ways_knowing-p2}
 	\caption{Possibilities of knowing}\label{ways_knowing-p2}
 \end{figure}

To create the integral socio-economic Impact weave, we need to tie these strands of knowledge together into the organisational form of a socio-eco\-no\-mic laboratory that combines learning, experimentation, and innovation. Drawing from Wolverton and McNeely in their book\cite{mcneely_reinventing_2008}, these forms can be linked together as: 

\begin{itemize}
	\item Integral Research/Action Research: In action research, scientific research is inseparable from democratic social action. Scientific knowing, like all other forms of knowledge for individuals and enterprise, is a product of continuous cycles of action and reflection.  
\item Integral Development/Cultural Topography: There is a rupture between the institutional design of a private enterprise and the cultural philosophy within most societies. This needs to be brought to the surface, making us conscious of the rupture prior to re-constructing enterprises.  
\item Integral Enterprise/Sustainable Development: Differing capitals are made up of natural and human as well as manufactured and financial capital. The resulting multifaceted flow forms enable companies and communities to engage in sustainable development.  
\item Integral Economy/A Genuine Well-Being Assessment: is analogous to a corporate annual report to shareholders. It reveals the economic, social and environmental conditions of well-being, using indicators that actually matter to people.
\end{itemize}

With our integral design, everyone can participate in experimental knowledge creation. Scientific knowing for individuals and enterprises, like all other forms of knowledge, is a product of continuous cycles of action and reflection. 
 
As a form of action research, we must acknowledge power relations and the politics behind them. If we want to listen to people, we need to hear them and empower them. Before we can expect to hear anything worth hearing, we have to examine (and possibly challenge) the power dynamics of the context and its social factors.  

For the management philosopher Reg Revans, considered the originator of action learning (2011), the end goals of individual countries and their enterprises will not be found by looking for some miracle in ‘globalisation’ or outside themselves; rather, their salvation will be found within their own shores and within the will of their own people.
  
Revans argues that, at the level of the individual enterprise, it is not unreasonable to suggest an essential part of any R\&D policy is the study of the human effort, out of which the saleable products of the enterprise are largely created. Such a study involves the ‘scientific method’ (survey, hypothesis, test, audit and control --- the core elements of the action learning cycle). 
%\clearpage

 \begin{figure}\centering
	\includegraphics[width=.8\linewidth]{ways_knowing-p3}
	\caption{Action Learning Cycle}\label{ways_knowing-p3}
\end{figure}

Knowledge creation and learning therefore must demand not only research and analysis, but power in order to get the knowledge needed to see one’s part in the big picture.  In particular, one needs to know the effect of one’s behaviour upon those with whom one works. For Revans, this is best achieved within small ‘action learning’ groups. In the Japanese context, he referred particularly to the establishment of such work groups; not only do they have a high degree of autonomy but are organised to enable people a continuing opportunity to develop.  

\emph{You learn with and from each other, in small groups or ‘learning sets’, by supportive attacks upon real and menacing problems}, through\cite{lessem_integral_2010}: 
\begin{itemize}
	\item   an exchange of information – ideas, advice, contacts, hun\-ches, concepts 
\item interaction between set members, offering each other support/challenge 
\item behavioural change resulting more often from the re-interpretation of past experience than the acquisition of fresh knowledge
\end{itemize}

Importantly for Revans, through the action learning process ‘you learn more from comrades in adversity than from teacher on high’. These three group dynamics are, in my experience, one of the success factors (if not the preponderant one) behind the Client Solutions Group I ran at Fortis Bank, which allowed people to feel empowered and gain real ownership of their outcomes. Having in brief looked at some of the dynamics behind knowledge, let’s also investigate some of its ingredients.  