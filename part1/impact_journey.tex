\chapter[Impact as a transformational journey]{Impact as a transformational journey into our calling}
\label{impact_journey}

\epigraph{God does not change people’s lot until they first change what’s in their own hearts.}{The Qur'\={a}n (13:11)}

Throughout my career, I have integrated learning and development \textsc{(L\&D)}. It was
during 2005, whilst delivering the twenty-day flagship programme ‘Deepening the
relationship’ at the (then) Fortis bank, that I got into a discussion with their \textsc{\lc{CEO}}, Angus
McLennan, about how banks needed to synthesise outside learning and development with
inside change and alignment.

On the initiative of Angus, an enlightened and highly competent leader, to effect
these changes we founded the ‘client solutions group’ by absorbing the structured finance
area as an agent for business transformation while working closely with clients. A main
reason why \textsc{L\&D} in isolation does not necessarily deliver value is the misalignment
between outside leadership messages and the inside operational culture. As Peter
Drucker once said, \emph{Culture eats strategy for breakfast}\cite{ross_culture_2016}.

At the time I was immersing myself deeply within culture and its multiple
manifestations to deliver client solutions from both a technical and relationship
perspective. It became evident that there was a contradictory toxic culture within the
industry in general, which could not be ignored.

During the financial crisis in 2009 and following the merger between Fortis and my
former firm, BNPParibas, I decided to leave the firm to study psychology and to integrate
this with my understanding of finance and economics. I was much more interested in
personal human development in practise, and given my preference for a pragmatic
approach to change and education, I decided to train as a psychotherapist. Importantly,
this incorporates both theory and practise to affect change.

Shortly after enrolling at the Institute of Contemporary Psychotherapy, I had a
discussion with Hector Sants, then the \textsc{\lc{CEO}} of the \textsc{\lc{UK}} industry regulator the Financial
Services Authority (\textsc{\lc{FSA}}). We discussed organisational culture and its possible role in
causing the financial crisis. It was apparent that the \textsc{\lc{FSA}} was seeking further input on the
subject, so we agreed that I would write a succinct consultation paper for him. I delivered
this in May 2010 with some follow-up discussions. When Hector subsequently made his
‘crossing the Rubicon’ speech at the Chartered Institute of Securities and Investments
Conference on 17 June 2010, he stated:

\begin{quote}
	As we have already identified, a deficient culture can play a role in, if not causing, certainly exacerbating market failures which generate
	inappropriate decision-making\footnote{Transcript available from the Financial Conduct Authority (\textsc{\lc{FCA}})}.
\end{quote}

A much greater challenge following the financial crisis has been to affect positive
culture change in banking. For a number of years, I had the opportunity to converse with
several \textsc{\lc{CEO}}s of some of the large global investment banks. When questioned on the
subject of culture, it was apparent that much of the work done in this area lacked integrity.
There were ‘tick-box’ exercises, with perhaps an ethics course added to the curriculum; as
such, no visible change has seemingly occurred to date and much of the business
continues ‘as usual’.

This was a major reason for embarking on my psychotherapy program; I wanted to
seek ways to heal the schism and separation in finance culture between money and
consciousness through e.g., impact investments in contrast to banking as it already
contains a higher level of consciousness and often has less separation e.g., in a family
office between capital and consciousness. The question is whether using a deeper Impact
methodology such as I\textsu{3}, we can weave ethics into an investment system almost ‘by
stealth’ within an existing working environment. As Sir Isaiah Berlin suggests\cite{berlin_crooked_2013}:

\begin{quote}
	Ethical
	thought consists of the systematic examination of the relations of human beings to each
	other, the conceptions, interests and ideals from which human ways of treating each other
	spring, and the systems of value on which such ends of life are based
\end{quote}

One of the cultural axioms in finance is the emotional connectivity (or lack thereof)
between ‘cause and effect’ and our responsibility for this. 

In finance there is always a
power distance between the investor and the investee, which needs to be managed.
Depending on our organisation and our influence, we may feel more or less empowered
and responsible for actions and impact outcomes.

As we have all experienced, particularly in large organisations, success has many
owners but failures have very few. We find that in a fear-based culture few want to
understand effects because the risk of being associated with negative effects and
consequences may impact their career. 

This is an important axiom to understand within
our own culture as impact, by definition, is about outcomes and effects. If we only operate
within a conventional culture, dialectic and behaviour, we will not learn.

It takes an enlightened leader to build a culture where a full-spectrum impact
approach is allowed to be created organically. Again, within governance we need to
ensure that our messaging is reinforced by fully-aligned policies and procedures to create
more of a virtuous circle.

Importantly, as impact contains the ‘other’ in as much fullness that we can muster,
if our management is to provide some form of democratic representation to our usually
hidden
‘other’, in what form and format is our governance system and culture willing to allow this
‘other’ to join us? How, and by whom, are ‘the others’ represented when we define and
design our impact outcome objectives? 

Do we have the level of awareness to carry and
develop this responsibility in our ways of operating and conducting ourselves?
We quickly see how onerous this may become; consequently, many firms will ‘not
even go there’ unless we have an integrated framework on which we can weave our
impact tapestry.

Whilst many of us swim within the myth streams of our own ego-inflated self-image,
 we need to find ways to integrate the shadow sides of ourselves with our true self
and light. This, as in all things, is integral when we try to make whole of what previously
were parts. 

It is not an either/or, it is a both/and. We are both the shadow and the light;
the key to integrate is to find out how we can best manage to be both at the same time.
\clearpage
\section[How we grow]{How we grow: renewing self and society through working with impact}

\epigraph{Each man had only one genuine vocation --- to find the way to himself... His task was to
	discover his own destiny --- not an arbitrary one --- and live it out wholly and resolutely
	within himself. Everything else was only a would-be existence, an attempt at evasion, a
	flight back to the ideals of the masses, conformity and fear of one’s own inwardness}{Herman Hesse\cite{hesse_demian_2001}}

As we know, personal growth and character development are irregular and non-
linear functions of our capacities for creating awareness and our ability to take
responsibility, all of which need to be activated within a context. Our character is a part of
our directional compass in life; as such, it is an integral source of our power and guidance
without which we will lose our bearings and freedom.

This is not an intellectual exercise; no one has studied the nature of life itself, only
its images and consequences as they are manifested. As yet, we have not developed
adequate mathematics to understand it. Linear differential equations have brought us to
approximations of the nature of life but not to the essence, which can only be
experienced.

This is a movement that can only come from our inside self, and it is about taking
personal leadership to enable leadership of others. Remember that we can only mirror
and give to any other that which we have established for ourselves. In leadership, primary
greatness is character; secondary greatness is our position, whether in our organisation
or in our perceived status. 

We need to understand that real power is an inside-out job and
comes from working on ourselves from the bottom-up, a task which cannot be skirted.
This is the opposite of our prevailing culture and conflicts with how many people think.

 As
a result, many people will never experience the reality of true and integral leadership
power, or can create a life based on a deeper awareness of self.

An emotionally immature person will tend to borrow strength from position,
experience, intellect, size or negative emotions to make up for a character imbalance.
Whilst awareness and responsibility are foundational, we need to understand that their
ingredients come from different sources. 

Awareness is sourced from both knowledge and
experience, and responsibility is a function of the shape of our character, requiring both to
be worked and assessed by ourselves.

The world today is full of awareness but lacking in responsibility, which is why we
find the corporate world littered with examples of character failure causing corporate
demise. 

It reminds me of the characteristics of trust as outlined in Stephen Covey’s book
\emph{The Speed of Trust}, where he delves into the two components that comprise the core
components of trust: Character and Competence.

Forbes contributor Mike Myatt (2012) suggests there are fifteen common reasons
for business failure, the first being ‘lack of character’. If true, these reasons need to be on
the risk register and part of our impact analysis. Whilst character is fundamental, our
nature and the depth of our relationships also reflect our character, which is why from our
integral perspective we need to start here.

From here we can move into the other realms of Being, Becoming, Knowing and
Doing (South, East, North, and West). If we can develop and integrate these from within
our emergent four-worlds perspective, this will lead and determine our impact contribution
for society. Key to this is our humble understanding that we are born incomplete and need
transformational processes for fulfilment. It is our grounded \emph{Being} that guides our
emergent \emph{Becoming}, which in turn seeks, captures and navigates our \emph{Knowing}, without
which there cannot be any intentional effect and \emph{Doing} that carries sustained impact
value.

\begin{figure}\centering
	\includegraphics[width=.75\linewidth]{p3-impact_journey}
	\caption[Doing, Knowing, Becoming, Being]{Doing, Knowing, Becoming, Being \cite{trans4m_integral_nodate}}\label{p3-impact_journey}
\end{figure}

These are the realms and rounds of our deepening understanding which all need
to be communicating with one another, find harmony and balance. Impact investment
management should not be only a philosophy or a series of academic exercises. Yes,
philosophy and academic exercises are important, but they are stepping stones leading
into our impact architecture, which can only be synthesised in our ‘doing’ before we return
for the next deepening circular rhythm back into our now expanded and more deeply
grounded “being”. These stages are context and outcome dependent.

Donald Robertson tells us:
\begin{quote}
	The ancients conceived of the ideal philosopher as a
	veritable warrior of the mind, a spiritual hero akin to Hercules himself, but since the
	demise of the Hellenistic schools, the philosopher has become something more bookish,
	not a warrior, but a mere librarian of the mind\cite{robertson_philosophy_2020}
\end{quote}

Theory and knowledge are part of the building block of philosophy, which we move
into practise for the more advanced work of synthesis between the four-worlds, oscillating
between our inner and outer worlds. This work of the self includes embracing our shadow
elements that have been created through culture and ego throughout our life stages.

The Jungian analyst Robert A. Johnson (1921–2018) stated this clearly:

\begin{quote}
	We are all born whole and, let us hope, will die whole. But somewhere early on
	our way, we eat one of the wonderful fruits of the tree of knowledge, things
	separate into good and evil, and we begin the shadow-making process: we divide
	our lives. In the cultural process we sort out our God-given characteristics into
	those that are acceptable to society and those that have to be put away. 
	
	This is
	wonderful and necessary, and there would be no civilized behaviour without this sorting out of good and evil. But the refused and unacceptable characteristics do
	not go away; they only collect in the dark corners of our personality. When they
	have been hidden long enough, they take on a life of their own—the shadow life\ldots
	The shadow is that which has not entered adequately into consciousness. 
	
	It is the
	despised quarter of our being. It often has an energy potential nearly as great as
	that of our ego. If it accumulates more energy than our ego, it erupts as an
	overpowering rage or some indiscretion or an accident that seems to have its own
	purpose\cite{johnson_owning_1991}
\end{quote}

The issue is that the more comfort, lack of challenge, stuck identity, and positions
we have to protect, the more likely we are to have our pride dominate opportunities for our
growth, individuation, and emancipation. 

Johnson provides further insight for the
implication for our impact journey and life:

\begin{quote}
	Generally, the first half of life is devoted to the cultural process—gaining one’s
	skills, raising a family, disciplining one’s self in a hundred different ways; the
	second half of life is devoted to restoring the wholeness (making holy) of life. One
	might complain that this is a senseless round trip except that the wholeness at the
	end is conscious while it was unconscious and childlike at the beginning. (Ibidem, p.10)
\end{quote}

In our youth we have a significant need for finding our autonomy, while valuing freedom of
expression in search for our creativity. This freedom is often challenged and compromised
when we enter organisational structures. Finding personal and individual freedom while
remaining inside structural boxes is very difficult. Whilst inside, we are often reluctant,
unable or unwilling to critique those very structures which cover our basic income needs,
give us a sense of security, status and identity. Their internal culture determines what we
can and cannot say, and even shapes what we can or cannot think. We may often feel our
organisation is beyond any honest critique, and as a result we are unwilling to jeopardise
our career path and prospects if we engage in anything that could been seen as
countercultural. In entering organisations, this individual realisation causes a split in our
personality with one part now compliant to conform to meet basic needs until such times
when the split is too large to hold together and we feel compelled to change course.

Psychologist Stanley Milgram (1933\textendash1984) provides a succinct summary for the
effects of a misdirected culture:

\begin{quote}
	Ordinary people, simply doing their jobs, and without any particular hostility on
	their part, can become agents in a terrible destructive process. Moreover, even
	when the destructive effects of their work become patently clear, and they are
	asked to carry out actions incompatible with fundamental standards of morality,relatively few people have the resources needed to resist authority\cite{milgram_obedience_1974}
\end{quote}

It is more often in the second part of life (and not just chronologically) when
through our deepening maturity and consciousness we have the opportunity to work and
integrate our shadows. We have to de-colonise our minds and hearts, as we all live
downstream from causes created by others. We are not necessarily here just to help but if
we can allow our impact work to join and become connected to others on our journey of
emancipation, we cannot not grow.

Here is a shadow work practise, as taught by leadership coach Scott Jeffrey, for
you to reflect on:

\begin{quote}
	Remember that the shadow is elusive; it hides behind us. We each have hosts of
	defence mechanisms designed to keep our shadows repressed and out of view. Shining
	the light of consciousness on the shadow takes a little effort and regular practise. The
	more you pay attention to your behaviour and attitudes, the better chance you have of
	catching your shadow in the act. One of the best ways to identify your shadow is to pay
	attention to your emotional reactions toward other people. We tend to project our
	disowned parts onto other people. Sure, your colleagues might be aggressive, arrogant,
	inconsiderate, or impatient, but if you don't have those same qualities within you, you
	won't have a strong reaction to their behaviour or the conditions in their personality.
	Whatever bothers you in another is likely a disowned part within yourself. Get to know
	that part, accept it, make it a part of you, and next time, it may not evoke a strong
	emotional charge when you observe it in another.\cite{jeffery_shadow_2019}
\end{quote}

If growth at work is a main part of the journey, then impact is core --- or at the very
least must become a significant part of the story. As Walter Randall wrote: \emph{No struggle,
no story, no trouble, no tale, no ill, no thrill, no agony, no adventure}\cite{randall_stories_1995}. Our
struggles in life create our essence and make life and us interesting participants in the
process.

This highlights the oscillating nature of how our respective narratives are created.
In most traditions, including our own, initiation rites marked the entry into adulthood, and
many have now been lost. These have profound psychological and developmental
benefits in how we let go of the old and embrace the new parts of ourselves as we move
towards maturity and possible individuation. Jung confirms that; \emph{The goal of the
individuation process is the synthesis of the self.}

In his book \emph{A Hero with a Thousand Faces}, Joseph Campbell outlined ‘the hero’s
journey’, which he termed ‘the monomyth’. This quest has been adopted in many areas,
including the film \emph{Star Wars}, on which Campbell advised the director George Lucas.
Campbell uncovered this journey whilst studying civilisations; he found that they all had a
form of archetypical initiation, stemming from our psyche and deepest needs: \emph{Each
carries within himself the all (\ldots) therefore it may be sought and discovered within.}’

The journey takes the shape seen in figure~\ref{p3-impact_journey(2)}.

\begin{figure}\centering
	\includegraphics[width=.75\linewidth]{p3-impact_journey(2)}
	\caption{The hero's journey}\label{p3-impact_journey(2)}
\end{figure}

This is both an experiential journey and an existential journey in order to find
sufficient courage for a clear calling to be tested and forged within the furnace of doubt.
He said that \emph{The cave you fear to enter holds the treasure you seek.} Only then can we
move into a co-creative space where challenges, trials and tribulations shape the path
ahead. If we survive this stage, we find ourselves at the depths of our idea of hell,
confronting our innermost fears and shadows in preparation for our ordeal before we start
the return journey with a new identity and place. The ultimate reward is the treasure at the
journey’s end: finding our true and essential nature.

Campbell asserted that:

\begin{quote}
	to realise that one is that essence; then one is free to
	wander as the essence in the world. Furthermore, the world too is of that essence. The
	essence of oneself and the essence of the world: these two are one\cite{campbell_hero_2008}.
\end{quote}

This primordial archetype operates strongly, especially in men, which is why men
find such stories highly attractive. However, whilst we cannot all be Luke Skywalker in
\emph{Star Wars}, we can use our impact context to lay a path for our own hero’s journey. As the
poet, Antonio Machado said: \emph{Wanderer, there is no path. You lay a path by walking}.

Sometimes we may feel despondent and lethargic, which is why our calling has to
be strong initially. Interestingly, the word ‘lethargic’ means a state of prolonged torpor or
inactivity, inertness of body or mind; the word comes from the medieval Latin \emph{litargia} and
from the late Latin \emph{lethargia}. The Greek word \emph{lethargia} actually means ‘forgetfulness’; its
roots go back to Greek mythology where Lethe was the name of a river in the underworld,
also known as ‘the River of Unmindfulness’ or ‘the River of Forgetfulness.’ Legend held
that when someone died, he/she was given a drink of water from the River Lethe to forget
all about his/her past life. Eventually this act of forgetting came to be associated with
feelings of sluggishness, inactivity or indifference.

Drinking such waters may appear attractive at times, given our trials and
tribulations, but if we do, we may forget the natural order of being on our impact journey
and our collective calling, which is key to our becoming. So-called miracles are not so
much violations of the natural order but a restoration of the natural order of things when
being is allowed free access to becoming. So-called evil is not overcome by attack or
even avoidance, but by union at a higher level. It is overcome not by fight, flight or freeze
but rather by fusion or flow.

Whilst the hero’s journey is universal, there are significant cultural influences which
transcend into organisations. Without wishing to generalise, in many Asian cultures such
as Japan, roles are identified differently. In Western culture you fight against some evil as
you search for truth and love, and the hero is an all-conquering and self-made individual.
In Asia, it is the person who collaborates and sacrifices him or herself for others who
becomes the hero, since the individual’s prime responsibility lies in taking care of the
family, community, and the country.

This difference can be seen in the Japanese, Chinese and Korean form of
storytelling known as \emph{Kish\={o}tenketsu}, which comes with four acts. In act one (\emph{Ki}), we are
introduced to the characters. In act two (\emph{sh\={o}}), the actions follow. In act three (\emph{ten}), a twist
that is highly surprising or apparently unconnected takes place. In the final fourth act
(\emph{kets}), the audience is invited in an open-ended way to search and find harmony and
answers – these narratives have no prescribed ending. Is not this approach more like real
life, which has much unexpected change? Is it not more realistic as we construct our own
integral approach in the search for the harmony and balance between life’s conflicting
parts?

This contrasts with the conventional Western approach, which often seeks to find
simple, linear solutions to unexpected change, to impose static controls, and go to war
with the offending parts they believe they now understand. In the East, seeking harmony
in these parts is an element of the joy in finding the answer that provides the organic and
most appropriate solution.

This subtle difference has profound implications in organisations in how
governance is structured, how meetings are managed and how they value and reward
their definitions of success. As any impact approach is formed inside an organisation
through such processes, its culture will determine the width and depth of its impact lens.
This is one of the reasons why there is such a challenge for large, established financial
investment firms and their management hierarchies to develop anything that resembles a
real impact approach. Large firms will tend to revert to the Western mode of operation,
thereby missing the core aim of impact which is to alleviate imbalances and create
harmony.

Often in large firms, individuals who have the greatest talent and capacity for
impact will be seen superficially by management as not fitting the performance culture.
The humble are seen as weak; those who challenge the status quo are seen as obstinate,
and those who seek to add depth and breadth to the narrow dialectic are seen as trouble
makers. As a result, and as a reaction when the modus operandi is challenged with
change, the incumbent management closes ranks and seeks to re-impose and re-enforce
order through control and by limiting the individuals concerned.

I have seen and experienced various aspects of this style of management
throughout my career within large organisations. Sometimes it emanated from a collective
management team but more often it came from a senior manager who needed to hold
power through control; this culture then cascaded down to lower management levels.

I would like to end this section using Gandhi’s oft-quoted seven social blunders 7
since they provide a frame and a potential personal balance sheet for where we, as
individuals, need to focus our growth.

\begin{enumerate}
	\item Wealth without work.
	\item Pleasure without conscience.
	\item Knowledge without character.
	\item Commerce without morality.
	\item Science without humanity.
	\item Religion without sacrifice.
	\item Politics without principle.
\end{enumerate}

I find it of little surprise that the first focuses on our relationship with money. These areas
will be explored further in later chapters.