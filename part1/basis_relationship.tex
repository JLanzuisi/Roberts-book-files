\chapter{The basis and dynamics of relationship}
  
\epigraph{The truth about intimate relationships is that they can never be any better than our relationship with ourselves. How we are related to ourselves determines not only the choice of the Other but the quality of the relationship. In fact, every intimate relationship tacitly reveals who we were when we commenced it. All relationships, therefore, are symptomatic of the state of our inner life, and no relationship can be any better than our relationship to our own consciousness\ldots}{Hollis\cite{hollis_middle_1993}, p.47}

Highlighting again the need for creating a reflective mirror which creates a bigger self. We are going beyond Newtonian physics, with its limitations in predicting or modelling the behaviour of large and moving objects. At the micro level, quantum physics has confirmed that we are constantly in relationships which go deeper but has yet to discover an adequate theory that explains phenomena such as black holes. Scientists are still searching for a unified theory of the universe.  
Relationally, perhaps the term ‘quantum entanglement’ suggests something that we have long intuited but science has only recently been able to observe: 

\begin{quote}
	in the world of quantum physics, it appears that one particle of any entangled pair ‘knows’ what is happening to another paired particle—even though there is no known means for such information to be communicated between the particles, which are separated by sometimes very large distances.\cite[adapted from Rohr R.]{noauthor_quantum_2019}
\end{quote}


Arguably, the most important aspects in anyone’s life (and for our well-being) are the level and quality of such relationships. And the most important relationship is, of course, the one we have with ourselves from which all other relationships are viewed and grounded. We can only see or hear another person as a mirror of our own maturation, awareness and consciousness, all which are ingredients for our judgements. 
 
As an important teacher reminded us several thousand years ago: ‘first take the plank out of your own eye, and then you will see clearly to take the splinter out of your brother’s eye.’ If our judgements are forms of simplification to fit someone else’s round peg into our limited square hole they are also a form of deflection; essentially the ego trying to make right for itself that which is, in reality, different. Our insistence and reliance on judgement, therefore, is a major barrier for us to open up the impact lens.  

A fundamental question is: who do we actually think we are in a relationship with? The always elusive ‘other’? Who do we include and exclude, and for what reasons? As impact is all about defining and designing the outcome contributions of our four realms, we need to look deeply into how we not only build but execute our relationships for agreed impact outcomes. 

As the spiritual teacher Jiddu Krishnamurti said:

\begin{quote}
Action has meaning only in relationship…and without understanding relationship, action on any level will only breed conflict. The understanding of relationship is infinitely more important than the search for any plan of action	\cite{mate_realm_2010}(p.207)
\end{quote}

Many might disagree with this statement, as it may contravene everything we have worked for. However, if you delve deeper you will find its absolute truth. As such, it is also one of the biggest challenges for us and for the impact industry to understand and build into our respective impact workings and approaches.  

Whilst working on Integral Impact perspectives, I thought it would be useful to understand our relationships within the integral framework. Our aspect, attitude, depth and our relationships are probably our most important work, since we are in constant relationship with virtually all things around us, including money.  

As we walk through the four worlds, we can briefly introduce the first integral dynamic which we call the ‘\textsc{\lc{GENE}}’. \textsc{\lc{GENE}} stands for Grounding, Emergence, Navigation and Effect. Each drives one of our realms, e.g., the Grounding of our very being happens in the southern realm of nature and relationships. The \textsc{\lc{GENE}} dynamic is also the function that we work through to deepen our self-understanding within each realm. 
 
From our informed Integral Impact map, we need to explore and delve deeper into relational dynamics and how we can use the \textsc{\lc{GENE}} to make these come alive as part of our development process. We can do this through a simplified integral four-worlds representation for these relational fields, which I call the 4Rs of Rooting, Resonance, Relevance and Rationale.  

\begin{figure}\centering
	\includegraphics[width=1\linewidth]{basis_relationship-p2}
	\caption{4R's of Relationship}\label{basis_relationship-p2}
\end{figure} 

\subsection{Integral South: Rooting}

\mypar{Inside-out:} From our firm’s perspective, starting from the gro\-unding within our Southern dimension, we need to understand the soil in which our relationships to people, places and things have evolved, and how it has created our calling. Within our community, we have shaped our cognitive map through those with whom we are in close relationships, who have given us the vast majority of our values and beliefs. It is here where we are given our ‘marching orders’ and learn what is right and wrong, and what is socially acceptable. These frameworks and ideas sometimes only emerge in the second half of life as we realise that they belong to others and to our past, and have little relevance today. They manifest as autonomous complexes, which are then projected and can create negative consequences for us. 

\mypar{Outside-in: } If we want to work on Impact investments with some integrity, we need to root ourselves not only in the 4Ms (Metaphor, Methodology, Method and Model) but also expand our capacities to re-root ourselves in the soil of a particular investment. This is why we need to develop and work through the 4Ms and, in particular, our first M: Metaphor. This will ground the investment philosophy at organisational level before we impose our model onto others.  

If we assume, for example, a corporate equity investment in Pakistan, we can ascertain the firm’s stakeholder relationships and the nature of its relationship with, and reliance on, the community in which they operate. This in turn feeds into our ‘social’ bucket within ESG. This aspect is restorative and developmental for the firm and its people.  

In the second half of life, we also have the opportunity to reflect and withdraw from some of our long-held relational and cultural ties that hold back our emergent needs of the soul. We can re-frame or discard old ideas about how we see the world through a historical perspective, whilst still grounding ourselves in valuable lessons from our ancestral history. These lessons play out in how our firm frames its reasons for being, adding value to what the firm may say is important. Who is inside-outside any ‘clubs’ such as career ‘fast tracking’, social events, etc.? How does the company manage and work with its employees? How are remuneration, career progression and corporate power structured and managed, and what are the key drivers?  

In essence, such questions and their answers give us part of a culture, and the extent people feel the organisation is one to which they want to belong.  

\subsection{Integral East: Resonance}

\mypar{Inside-out:} Within our context, here we seek the emergence of our meaning making and what nurtures our soul. When we do this, we awaken our true relationship with our investment context and we go deeper into understanding why we are pursuing this route. We seek to engage with our core feelings for what we are engaged with, and we also look for different perspectives to include the possible ‘other’ and what influence this may have on our emotional self.  

Each Impact investment opportunity must resonate within the firm and find the necessary connectivity to progress. Our Impact approach will also need to resonate with our investee to guide our discussions towards the design of the component parts of our Integral Impact Investments Outcome Objectives (I\textsu{3}OO). The I\textsu{3}OO is our context, the formal agreements in which we can negotiate our key priorities for Impact. How we have achieved this will keep the I\textsu{3}OO alive and well within the firm, and keep it on track. 
 
\mypar{Outside-in:} Having grounded ourselves in our specific relationship, we look at how we emerge into multiple levels and why we are suited to create this specific type of relationship. This includes looking at the nature of the investment from the investees’ cultural and spiritual perspectives, which are operationalised through its behaviours, rites,
and codes of conduct\ldots Here we may need to reach beyond the immediate management with whom we may have most of the relational contact into its stakeholders to find cultural resonance and validation.  

To that extent does our own culture and Impact investment philosophy resonate and align with the investee? Is conventional equity the right instrument that helps or hinders any current power complex? What is the alignment between strategy, time-lines, exit, policies, procedures and other aspects of its operations that one will have to work with? If misaligned, might this result in future conflict? Once this is understood, we can begin to structure the investment accordingly and design our involvement in its management/governance.  

What are the levels and resonance of relationships between the firm and its community? How does this investment impact on the community and does it comport with our impact outcome objectives? Does this aspect need re-design or re-rooting to embed and align the community into the well-being and success of the firm? All of the above will inform and shape the governance aspect of ESG.    

\subsection{Integral North: Relevance}

\mypar{Inside-out:} Becoming integrally informed through understanding the initial grounding in our roots and the emergence of our resonance, we see the connected relevance as related to our calling, in our context and how this begins to inform and navigate our capacity for Impact co-creation. Now we activate and build the necessary intellectual resources and creativity on which we will rely for strengthening our resolve and resilience in the face of challenges. We start to communicate and garner interest and relational strength from others for collaboration and co-creation in line with our Impact objectives. This creates and speaks to how we build ‘buy-in’ for Impact credibility with all stakeholders, intentionality and sustainability.    
 
\mypar{Outside-in:} In many respects, this is the realm in which we start making sense to our outside stakeholders and our investee about the Impact approach. We start connecting Impact with their priorities. Again, resilience and sustainability are rooted in the process of embedding Impact changes into the fabric of the firm.   

\subsection{Integral West: Rationale}  

\mypar{Inside-out:} Having travelled through the previous three realms, we now enter the area where we need to create our contribution and effect. This emerges from the rational conclusions within meaning making to make decisions for our actions and behaviours. The sense making based on knowledge from our Integral North can now close the circle as it creates the deeper narrative for what our Impact contribution and effect will look like.  

We now enter our ‘doing’ phase with enthusiasm and conviction for our mission and the benefits it will bring. We align our strategic intentions with mission and purpose to fully engage in implementing and executing our master plan. Having completed the I\textsu{3} process, and with our I\textsu{3}OO now completed with our investee, we implement and execute the agreed plan.   

\mypar{Outside-in:} Our Integral Impact map can now be completed and readied to be travelled by stakeholders, who can now fully engage and implement it. From their unique perspective, they can justify what, where and when they are doing what they are doing. We see alignment and ‘walking the talk’ as stakeholders understand their role and their meaning.
   
To make a contrast, in romantic relationships we are also beginning to understand what works well or not. In John Gottman’s research which is based on 40 years of studying intimate relationships and marriages, and based on a series of questions he was able to predict with an astonishing 90\% accuracy, the probability of divorce rates of married couples.  Gottman called these drivers “the four horsemen of the apocalypse\cite{noauthor_four_2013}: criticism (a shadow North in the form of attack of the others personality or character); defensiveness (a shadow West by victimising ourselves through blaming the other); stonewalling (a shadow East through withdrawal, creating separation through disapproval of the other);and contempt (a shadow South by attacking the others sense of self with the intent to insult or abuse), with contempt being the larger driver in a romantic partnership.  

Let us now look through this lens from a more pragmatic business perspective. As the author of the book Start with Why, Simon Sinek writes: ‘People don’t buy what you do, they buy why you do it\cite{sinek_8_2017}.’ 

If you want to get more insights into this aspect, watch Sinek’s TED Talk ‘How great leaders inspire action\cite{noauthor_youtube_nodate}’, where he explains ‘the golden circle’ of an organisations what? how? and why? I think when most people seek to understand a firm from the ‘outside-in’ through an conventional investment process; the vast majority will be quite clear about the What? of the firm; fewer would know the How?; and even less the Why? of the company which we need to asses and align with our Impact capacity diagnosis.

\begin{figure}\centering
	\includegraphics[width=.8\linewidth]{basis_relationship-p3}
	\caption{Sinek’s Golden Circle}\label{basis_relationship-p3}
\end{figure} 

Sinek asserts that ‘great leaders’ communicate, and I would add ‘manage’ the company, starting with the Why? then moving to the How? and finally covering the What? He relates
this to the biological evolution of the human brain from the neo-cortex (relating to the first circle of What?) and corresponds it to our analytical and rational capacities including our language. The other inner two questions relate to the limbic brain, which can generate feelings and corresponds to trust, loyalty, decision making, and behaviour. These two are related to our capacity for language which also corresponds to our psychology so it’s only when we come to the What? where we usually see communication, now incorporating the previous. As Will Durant suggests, quoting Plato: ‘Human behaviour flows from three main sources: desire, emotion, and knowledge'.

We may have by now noticed that this comports with the integral model, albeit with the difference that we need to start with the Who? which then fully takes into account ‘the golden rule’ that exists with all cultures and faiths: ‘Do unto others what you want them to do unto you’. There we have it: an inside-out in combination with an outside-in perspective, or now fully integrally informed. The golden rule to my mind suggests we stay firmly focused on ourselves and our task. It does not suggest crossing the line of co-dependency where we can overextend our need to rescue, care-take etc., and thereby lose ourselves in one another. 

Rather than two individuals, one seeking to better him/herself at the expense of the other or extending charity to the other, they are simply two cells of the one great Life, each  equally precious and necessary. And as these two people merge, as in a marriage, experiencing that one Life of unity, they discover that ‘doing unto the other’ is not a loss of one’s self but a vast expansion of it, because the indivisible wholeness of reality of finding an expanded love is the core need and requirement of the True Self.  

I can’t think of a better reason for working with integrity towards wholeness and for seeking real impact. 
 
‘Wholeness does not mean perfection; it means embracing brokenness as an integral part of life’\cite{palmer_hidden_2008} (p. 5).  When we talk about Integrity we also mean how we have created an integral wholeness meaning that you have unified your life so that regardless of any drawer that you open you can see who that person really is. 

\epigraph{Nothing is at last sacred but the integrity of your own mind}{Ralph Waldo Emerson}

Authentic love is one piece; how you love anything is how you love everything. Until we love and until we suffer, we all try to figure out life and death. If you have never experienced human love, it will be very hard for you to access love as an inner revelation. If you have never let someone else really love you, you will not know how to love humanly in the deepest way. 

We must understand that the nature of relationship needs to include the qualities that are needed when they are put into tension, stress or shock. What we saw in the financial crisis was another example of how our conventional economic system has built purely transactional relationships mainly on the principles of expediency and efficiency, while minimising the nature and grounding necessary to have real relationships based on trust and integrity. Politically and culturally, the message was that the banks were too big to fail, and thus people were too small to matter, which is the root of many social issues today.  

These types of relationship will break apart when put under pressure and are unlikely to reconnect. Without the \$2 trillion bailout, the chains would have broken and would only have been able to re-establish themselves at a completely new and much lower level.  

All natural systems have a key inbuilt ingredient, namely resilience. They have latent capacities to absorb unexpected chan\-ge, which can hold the structural chains together. In finance this is equivalent to collateral, but I would also include other assets such as creativity.  

When we design and build Impact systems, we need to consider carefully the relational foundations for each impact dimension, their potential weak areas in the chain and the implications if a break should occur including our fallback position for repair and remedy. 

Turning to Carl Jung for clarity: ‘The creation of something new is not accomplished by the intellect but by the play instinct acting from inner necessity. The creative mind plays with the objects it loves\cite{hillman_myth_1997}’(p.48). 

All firms live and die by their relationship with stakeholders and particularly of course, their clients as their only source of cash from operations. The dynamics around client need are highly context dependent and, for some, culture will have less importance than quality of production. 

However, when we design our cultural client dimension, empirical research from over one billion client interviews by Gallup suggests that across industries there are four needs-based client drivers. 

\begin{itemize}
	\item At the lowest level, clients expect accuracy. Without product accuracy and delivery, no amount of additional service will retain a client.  
	\item The next level is availability. The banking industry’s move to ATM, and  internet banking, increased the availability of existing products and services.  
	\item At this level a client expects partnership. They expect to be listened to, the firm to be responsive to their needs with a sense of being on the same side.  
	\item The most advanced level is advice. Clients build the closest bond to firms that have helped them  learn and develop.   
\end{itemize}

	The first two levels of client expectations are quite easy to meet but, as such, they are also quite easy for competitors to replicate.