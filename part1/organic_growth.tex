\chapter{Organic growth and development}

Psychology suggests that around the age of two we develop the capacity to
understand that if a ball rolls behind a chair and is unseen it is still there and has not
disappeared; this phenomenon is known as object constancy or permanence (Mahler and
Piaget, 1975). Depending on how such experiences are held emotionally, we become
more or less comfortable in experiencing the unknown and unfamiliar.

We can all relate to specific childhood memories; when thinking about the past we
often find that we have the same characteristics but less strongly. That is, the essence of
our personality was present from the start and we are still discovering new aspects that
form our perceptions. Many functions we performed in childhood, from building
sandcastles to playing children’s games, create the formative basis for understanding
logical relationships; for example, we might make a stronger foundation and thicker, lower
walls for our sandcastle to withstand the ravages of time and shifting sand. When we
played games involving friends and foe, we formed our cognitive understanding of how to
interact as either, and created our abilities to judge right from wrong. These functions in
adults are, of course, more sophisticated and informed but in essence remain very similar
to our early ‘sandpit’ experiences.

It seems we always and continuously engage in our functional preferences
between sensing and intuiting, thinking and feeling to focus our attention. Having created
attention our now informed decisions are then filtered through our introverted or
extraverted attitude into the outside world. These experiences are not surprising since
human beings are living systems, constantly evolving and maturing. 

The whole process is
a gradual organic development, sometimes punctuated with growth spurts but always
within a whole. We can use a tree’s life cycle as a metaphor, where each ‘stage’ reflects
something unique at a specific time in its development. Using the brain scientist Dario
Nardi’s\cite{darionardi_notitle_nodate} outline \emph{A Fresh Understanding of Function Development} as follows:

\clearpage
\begin{scriptsize}
	\begin{longtable}[c]{r l L{12em} l}
	\caption{A tree’s life cycle as a metaphor}\label{table_tree_metaphor}\\
		\toprule
		Stage & Tree & Tree’s special qualities & Person \\ \midrule
		0 & Seed & A potential tree with genetic instructions for development, heavily reliant and formed by its immediate environment & Embryo \\
		\addlinespace[4pt]
		1 & Sprout & Life emerges and grown in both directions but is still connected to
		and nourished by the seeds ability to exploit the environment & Child \\
		\addlinespace[4pt]
		2 & Sapling & Beginning to harden and take final shape but still soft and pliable
		and easily shaped by the development around it & Youth \\
		\addlinespace[4pt]
		3 & Adult & Now taking full shape and can bear fruit/seed. Stronger and harder
		to withstand changes to the environment & Adult \\
		\addlinespace[4pt]
		4 & Mature & Age and character showing, now with history and a clear role and
		position (meaning/purpose) and dependents in its environment,
		and to the degree possible, the self has withstood the tests of time & Senior \\
		\bottomrule
	\end{longtable}
\end{scriptsize}

There are significant overlaps in these stages. For example, when a tree bears its
first fruits that fruit is usually small and inedible; it is a sort of trial period for things to
come. The change into the adult tree with plentiful tasty fruit can take years and is not
guaranteed.

Whilst this simple example is linear, it lacks many of the feedback loops humans
integrate for growth; it highlights stages of what can be called our function-attitudes, which
has its own life cycle in how our behaviours and attitudes influence our experiences. Such
sequences do not ‘switch on’ automatically at specific ages, rather, each sequence has its
own functional development pattern. 

For example, there is a ‘concrete’ (primitive, early)
and ‘abstract’ (sophisticated, later) version of each function-attitude. Each function-
attitude has a ‘passive’ and ‘active’ mode, and engaging a function-attitude can result in a
‘product’ in the outer world as well as a mental event. 

As a person develops a function-
attitude, he/she tends to engage more abstractly, but also becomes more active and
sophisticated whilst experimenting, seeking integration and assimilation.

Also, each function-attitude will continually manifest itself in intriguing ways. The
reason for outlining this is because, whilst we are all either in the adult or mature stages of
our careers depending on where we are on our investment/impact/life journey, we still
need to reflect, explore, develop, and integrate specific function-attitudes that match our
life situation. As such, the whole pattern is present and operates from the beginning. An
individual as a whole will only mature sequentially with experience and time, transcending
and including its previous stages.

Having withstood several storms myself, I know that I’m not going to be blown over
during the next one. I have developed some understanding of what to do and the
resilience to withstand whatever is thrown at me. I believe that ‘this too shall pass’ and
that all experiences are learning devices that enable me to grow and develop. We all need
to recognise where our personal Impact frame sits within these attitudes, and where we
think our organisation is positioned culturally.

%\clearpage
\begin{scriptsize}
	\begin{longtable}[c]{r L{20em}}
	\caption{The lifecycle of a function-attitude}\label{table_function-attitude}\\
	\toprule
	Stage & The experience and response to a function-attitude \\
	\midrule
	0 & No cognitive awareness, conscious usage or direct development\\
	\addlinespace[4pt]
	1 & We experience the function in its instinctual or concrete form; we try to block out,
	explore, or fixate on the experience; its engagement is rough or child-like.\\
	\addlinespace[4pt]
	2 & We accept and follow a social/cultural version of the function, or we passively
	follow the functional process; we are in its grip; its use is rigid or adolescent\\
	\addlinespace[4pt]
	3 & We ‘grasp’ the function in its many forms; we question, alter, personalize and
	make it our own, as a tool with many options; its use is complex and flexible.\\
	\addlinespace[4pt]
	4 & The function is highly differentiated and well-integrated; using it contributes back
	to the world in a unique way; its use is purposeful, creative and generative.\\
	\bottomrule
	\end{longtable}
\end{scriptsize}

This approach fits with a common matrix that describes the four stages of learning
or competence, from unconscious incompetence to conscious incompetence, to
conscious competence and to unconscious competence that outlines the generic
positions we find ourselves in our knowledge creation. These stages can often cause
confusion, frustration or resistance if we misinterpret our emotional attitude towards them.

Depending on what we are trying to learn, in combination with our attitude, natural abilities
and aptitude, individually we will spend different amounts of time in each stage.
In his book \emph{Outliers: The Story of Success}, Malcolm Gladwell suggests\cite{gladwell_story_2008} it takes a
human being 10,000 hours to reach mastery, or what psychologists would call
unconscious competence.

Martin M. Broadwell in his ‘Teaching for Learning' article from 20 February 1969
outlined these four stages of competence summarised here:

	\mypar{Unconscious Incompetence (stage 1)}
	Most aspects of any process outside our awareness
	- we do not even know what we are missing. We
	may arrange our lives to avoid engaging in the
	process or inadvertently project negative aspects of
	the process on to others.
	
	\mypar{Conscious Incompetence (stage 2)}
	We become aware of what the areas are for
	improvement but we use the process awkwardly,
	but do not understand the process from a wider
	perspective. We often inappropriately either
	over, under and mis-use the process.
	
	\mypar{Conscious Competence (stage 3)}
	We selectively use knowledge and choose what
	aspects of the process will be most effective for
	success. Aware of how to link the process to other
	processes.
	
	\mypar{Unconscious Competence (stage 4)}
	All aspects of the process are seamlessly
	integrated with other functions creating a sense
	of flow. Creativity appears spontaneously in a
	process as we move with effortless results.

Taking this further, we introduce the work of Richard Barrett, an important thought leader
in the evolution of values, culture and leadership in business and society, whose work
resonates highly with our Integral Impact development trajectory. In his book, \emph{The
Evolutionary Man}\cite{barrett_evolutionary_2018}, he outlines an evolved developmental axis based on
consciousness and their algorithms:

\begin{scriptsize}
	\begin{longtable}[c]{R{4em} L{3em} R{7.5em} L{7.5em} L{5em}}
		\caption{Evolved developmental axis}\label{table-evolved_axis}\\
		\toprule
		Stages of Development & Age Ranges & Developmental Tasks & Positive Values & Potentially
		Limiting Values \\
		\midrule
		7. Serving & 60+ Years & Contributing to the well-being of
		future generations and the planet. & Compassion, Forgiveness,
		Humility, Contribution, Future
		Generations & \textsc{\lc{N/A}} \\
		\addlinespace[4pt]
		6. Integrating & 50--59 years & Connecting with others in
		unconditional loving relationships
		to make a difference. & Collaboration, Empathy, Intuition, Mentoring, Partnering, Alliances & \textsc{\lc{N/A}} \\
		\addlinespace[4pt]
		5. Self-Actualising & 40--49 years & Expressing your true nature by
		embracing your soul’s values and
		purpose. & 
		Partnering, Alliances
		Integrity, Authenticity,
		Meaning, Internal Alignment &  \textsc{\lc{N/A}} \\
		\addlinespace[4pt]
		4. Individuating & 25--39 years & Discovering your true identity by
		letting go of your fears and your
		dependence on others. & Freedom, Autonomy, Accountability, Adaptability, Courage, Personal Growth &  \textsc{\lc{N/A}} \\
		\addlinespace[4pt]
		3. Differentiating & 8--24 years & Feeling recognised and respected
		by establishing yourself in a
		community that values who you
		are. & Security, Recognition, Positive, Self-Image, Self-Esteem, Confidence & Arrogance, Pride, Conceit, Superiority, Discrimination \\
		\addlinespace[4pt]
		2. Conforming & 3--7 years & Feeling safe and protected by
		staying close to your kin and your
		family. & Safety, Family, Friendship, Belonging, Harmony & Being liked, Blame, Jealousy, Revenge\\
		1. Surviving & Conception to 2 years & Staying alive and physically
		healthy by getting your survival
		needs met. & Survival, Health, Physical Fitness, Nutrition, Financial Stability & Control, Manipulation, Greed, Caution\\
		\bottomrule
	\end{longtable}
\end{scriptsize}

According to Barrett, there are three stages of the Ego-Soul dynamic: 1, 2, and 3 is about
our ego-development; 4 and 5 about our ego-soul alignment; and 6 and 7 about our soul
activation. The three stages of evolutionary Intelligence have the following algorithms: 1,
2 and 3 are about becoming viable and independent; 4 and 5 are about bonding to form a
group structure; with 6 and 7 cooperating to form a higher order entity. Barrett’s work
helps understand the dynamic developmental dance we are all engaged in; and each
stage needed to be integrated, transcending and including the previous. Whilst such
stages should be seen as simplified outlines with many crossovers and overlaps, the
question remains how we present and work within the Impact field, possibly now
recognising the development required for each stage to emerge.

In addition, it is important to become familiar with some of our personal ‘meta
programs’\cite{bandler_structure_1975} which are mental and neurological codified
processes and repeated habitual strategies that manage, guide, and direct other mental
processes including our behaviour.

Meta programs determine which of our perceptions are selected for our attention.
In other words, they are processes at a higher level than the mental processes they affect
and are key in uncovering individual context and how we got to where we are today. We
could compare meta programs to a circuit board that controls which two phones will be
connected to each other during a conversation, or a thermostat that controls whether our
air conditioning system is turned on or off.

Neuro Linguistic Programming (\textsc{\lc{NLP}}) uses a contemporary meta\-phor taken from
computer science to describe the action of one process upon other programs. Often in
computer programming, one program controls the execution of a number of other
programs, selecting which ones will run at which times and sending them information they
will need in order to function properly.

Many of us are fearful of, or resist reaching out into such territory because of pre-
programmed boundary conditions which were created in our past such as beliefs about
our limitations\cite{charvet_words_1997}. Some of the more important meta programs (out of ca.
50) are:
\begin{enumerate}
	\item Toward or away (i.e. directional move from people, places and
	things)
	\item Sameness versus difference (as preference and for comfort)
	\item External or internal frame of reference (for validation/jus\-tification)
	\item Matcher or mismatcher (generate resonance/dissonance)
	\item Convincer strategy (for decision making)
	\item Possibility versus necessity (as a motivating driver)
	\item Independent, cooperative and proximity (as working sty\-les)
\end{enumerate}

These programs are constructed by nurture and inbuilt by nature. Irrespective of
their origins, at some stage we hermeneutically need to investigate their efficacy and
relevance in our lives to understand their impact. This may at times include a
regressionary journey to re-connect with our past, in particular our childhood states, and
discover how this pre-programming is affecting our current status.
We benefit from moving outside our created maps of our assumed realities; there
are many ways of achieving these multiple perspectives to create a more independent
view of our constructed activity.

The old idea of being born as a blank slate or ‘tabula rasa’ implies that our
baseline early on gets filled with material and knowledge from early on in life. However,
since a blank slate cannot learn anything, this notion cannot be the whole story. As
mentioned previously, all we know is from contrast and comparison, apart from
downloaded ‘factual’ information. This journey of discovery, and the way this knowing can
be released, is in the dance of unity-in-differentiation which is also as it happens inside
the language of discovering and interpreting what is true love.

Interpretation however, depends on perspective. I use the term ‘power-distance’ to
explain how far away someone’s consciousness is from understanding the outcome of
their actions inside a cause and effect. This relates to how the social psychologist, Geert
Hofstede views power relationships as a dynamic between superior-subordinate
relationships, authority and collaborative participation\cite{hofstede_cultures_2010}.

During my work in investment banking, I found that some people in high office
could become pathologically disconnected from their work and, as a result, sometimes
became narcissistically egocentric. A large part of my inner calling was to find and re-
connect disconnected parts of a business, both psychologically and technically.
In my view, this power distance is a psychodynamic, connective, and spiritual
developmental that includes and transcends the traditional moral development axes that
psychologists such as Kohlberg, Piaget, Wilber and Gilligan have put forward, which are
explained below.

Lawrence Kohlberg's cognitivist model of moral development with Habermas,
Gilligan, Dreyfus and Heidegger additions:

\begin{scriptsize}
	\begin{longtable}[c]{L{4em} R{7em} L{16em}}
		\caption{Kohlberg's cognitivist model}\label{table-kohl_cong_model}\\
		\toprule
		Stage 1\footnote{Kohlberg} & Pre-conventional 1: Infantile & Satisfaction of needs and avoidance of punishment; involved ethical comportment \\
		\addlinespace[4pt]
		Stage 2 & Pre-conventional 2: Egotistic & Self-interest orientation; What's in it for me? You scratch my back, and I'll scratch yours \\
		\addlinespace[4pt]
		Stage 3 & Conventional 1: Tribal & Conforming to stereotypical images of majority behaviour; filling social roles\\
		\addlinespace[4pt]
		Stage 4 & Conventional 2: Conservative & Following fixed rules, social conventions and laws; retaining the given social order\\
		\addlinespace[4pt]
		Stage 5 & Post-conventional 1: Democratic & Social contract driven; conventions and laws can change; greatest good through compromise and majority decision \\
		\addlinespace[4pt]
		Stage 6 & Post-conventional 2: Revolutionary & Principled; guidance by universal ethical principles; commitment to justice transcending unjust laws\\
		\addlinespace[4pt]
		Stage 7 & Transcendental (speculative) Transformational & Transcendental morality or morality of cosmic orientation; integrating religion with moral reasoning\\
		\addlinespace[4pt]
		Stage 8\footnote{Habermas} & Dialectic dialogical (last rational stage) & Rational agreement through dialectical dialogue sublimating lower levels \\
		\addlinespace[4pt]
		Stage 9\footnote{Gilligan} & Caring (first intuitive stage) & Caring, emphatic, non-cognitive, involved know-how within context of narrative of relationships that extends over time; giving up rules and maxims; contextual relativistic\\
		\addlinespace[4pt]
		Stage 10 & Dreyfus 1 & Expert Intuitive response to the unique situation out of a fund of experience in the culture; intuitively doing what the culture deems good\\
		\addlinespace[4pt]
		Stage 11 & Dreyfus 2\footnote{Heidegger} & Authentic Visionary response to unique, concrete, existential situation, disclosed in and through resoluteness by the call of conscience\\
		\bottomrule
	\end{longtable}
\end{scriptsize}

Carol Gilligan in her book \emph{In a Different Voice}\cite{gilligan_different_1993}, discovered the stages in women’s development as:

\mypar{Stage 1:} Selfish care (egocentric), where the woman cares only for herself  
\mypar{Stage 2:} Co-centric, where she extends care to her own group  
\mypar{Stage 3:} Uni-centric, being universal care, caring for all groups, and  
\mypar{Stage 4:} Being integrated, integrating masculine and feminine modes.

\epigraph{People don't care how much you know until they know how much you care}{Theodore Roosevelt}

We know from her work, and Ken Wilber’s, that there is also a masculine/feminine dimension to moral development. We also know that it is only when a human goes beyond the egocentric and ethnocentric stages into world-centric that they can jettison their own needs. Prior to this they do not want everybody to be treated equally, which I believe is a prerequisite for working with integrity in the impact space.  

Key to the grounding of the self is our capacity for humility which we will explore in a later chapter on organisational performance. In the book \emph{The Power of Humility}\cite{whitfield_power_2006}, Charles and Barbara Whitfield categorize humility into twelve components:

\begin{enumerate}
	\item Openness to what ‘is’ and our reality and the feeling of peace
	\item Attitude of ‘don't know’: Acceptance of limited knowledge and no judgements
	\item Curiosity: Awareness of the knowing that there is always more
	\item Innocence: The knowing that a pure mind is strength and peace
	\item Childlike Nature: ‘there is a child in you who seeks the parents’ house’
	\item Spontaneity: Living as real selves in the moment of now
	\item Spirituality: Relationship with self and others and personal path of growth
	\item Tolerance: The capacity to respect and accept ourselves and others. Overcoming resistance
	\item Patience: Overcoming the conflict of egoic attainment 
	\item Integrity: Wholeness and authenticity of character in alignment with true self
	\item Detachment: The ability to withdraw emotional attachment to a person, place or thing with love
	\item Letting Go: Inner process of removing the ego and surrendering the need to control
\end{enumerate}

You may want to scale yourself from 1 to 10 in each of these components to get a baseline reading (or, even better, ask someone else to do it for you).

Our character is also our adherence to principles. We don’t want to become what Groucho Marx is suggested to have uttered in gist as \emph{These are my principles. If you don’t like them I have others.}

For us to find meaning and purpose in life, our work has to be grounded in our innermost calling, then activated and expressed into a community to which we feel connected. If not, we are always subject to our culture’s definition of success and will follow others’ marching orders, i.e., the beliefs and values, cultural codes, instructions, ideas and behaviours handed down to us through family and society.  

I fully appreciate that each of the above areas of organic growth and development can constitute an individual chapter of this book, so apologies for the whistle-stop tour but the aim is to outline some areas for further exploration. When working with impact investments, we have an opportunity to catalyse our rich creative, spiritual and culturally diverse heritage. We understand our individual selves and, when expressed in our collective organisations, communities, and societies, we can fully release our human potential.

This is Impact’s promise. Inner growth on a highly personal level is a fundamental prerequisite for sustainable impact and its development. Without developing this inner ability, and together with trust, strength and commitment to actively face our contemporary evolutionary, conflicting and competing pressures, of our time, it is unlikely that we can engage meaningfully in the conscious renewal of societies and the restoration of nature. This can only happen when we realise that we are not necessarily defending Nature, rather, we are an integral part of Nature defending itself.

The fact that you are reading this book means that you have already had a desire for Impact Investments, and that this may be a star to follow. The word \emph{desire} comes from the Latin ‘De’ and `Sire’ meaning ‘from the stars’. Outer growth and Impact, on a collective level, require us to understand and create a contextual dialectic that can tap into the developmental and cultural potential of a particular society. Culture can be understood as a collective psychology, with shared fields of meaning and identity that act as the creative glue within a society. When impact is such, it forms the soil for healthy and continuous processes of renewal.  

With impact, we also engage in the renewal of our values versus outside laws and marching orders. Renewal itself must be preceded by some form of loss, as emptiness makes way for new growth; every transformation requires surrendering a previous ‘form’, hence our need to walk with humility, acceptance and openness.  

However, vigilance is also required towards the authenticity of the impact, as Hannah Arendt wrote of those who enter

\begin{quote}
	the maelstrom of an unending process of expansion, he[sic] will, as it were, cease to be what he was and obey the laws and process, identify himself with anonymous forces that he is supposed to serve in order to keep the whole person in motion, he will think of himself as mere function, and eventually consider such functionality, such an incarnation of the dynamic trend, his highest possible achievement
\end{quote} 

Culture is much more than the arts and creative expressions of a society; it is in how culture shapes the humanities and ways of being and belonging that a culture can be best experienced. Impact therefore may actively include the arts, dance, music, poetry and stories as well as the unique wisdom and spiritual expressions that bring out the inimitable ‘flavour’ of a place and society.  

We need to be conscious that cultural differences may be viewed as a negative life-denying source of division, and an aversion to evolution. Our work life needs to bring in a radical conversation, both with ourselves and with our firm, about our gifts and requirements. The question is: what is the quality of that core conversation? 

We often invest an inordinate amount of time and effort in our professional identity, which we seek to protect at great personal cost. Also, our Western culture values puts on a pedestal, our organisational identity, so we will always to some degree be pulled apart by the forces between self and our culture.  

When working with impact, we have the opportunity to heal such differences by using an integral design for our impact objectives so they become a constructive and life-affirming source for unity, identity, belonging, and mutual growth. Activating the life-affirming potential of a specific culture requires continuous evolutionary processes within and between cultures, spearheaded by local agents and agencies. Any impact organisation or community can become such an agency if its development processes are sufficiently embedded within nature and culture – and if the active co-evolution with other cultures is facilitated.  

Our purpose, then, is to obtain impetus and reinforcement from inside each integral realm. If we can be allowed to make what is invisible inside our organisations visible, and feel engaged, charged and activated, we are fortunate indeed and will have a strong sense of connection and belonging. The converse requires an enormous amount of energy as we will be employing our mental strategies with no inner support systems that are truly engaged.  

We often spend more time inside our organisations than we do with our nearest and dearest; if we can’t ask (and converse about) the great questions of life, what it means to be fully human, then these organisations inevitably become too small for us. This is one of the greatest management challenges of our time. As Marcus Aurelius\cite{aurelius_meditations_2020} reminds us in his \emph{Meditations}:
\begin{quote}
	Accept the things to which fate binds you, and love the people with whom fate brings you together, but do so with all your heart.
\end{quote}

Few areas have more impact on our lives than our relationship and attitude towards money. In the next chapter, I will, however, share and expand on my own experiences and insights in order to stimulate further thoughts. 