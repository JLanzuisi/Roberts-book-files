\chapter{The five A's of relationship}

\epigraph{If you are here unfaithfully with us / you’re causing terrible damage.}{Rumi\cite{mitchell_enlightened_1989} (p.56).}

As the old saying goes, do you want to be right or do you want to be in a relationship? Looking through the four Rs, we see that this is a function of levels i.e., we are in relationship with everything but it is a question of quality.  
Another driver for a person’s inner calling into relationship is what the psychologist and Buddhist teacher, David Richo refers to as the necessary 5As of healthy adult relationships: \textsc{\lc{ATTENTION}}, \textsc{\lc{ACCEPTANCE}}, \textsc{\lc{APPRECIATION}}, \textsc{\lc{AFFECTION}} and 
\textsc{\lc{ALLOWING}}\cite[p. 55]{richo_how_2002}. These can be seen metaphorically as five fuel tanks that are full at birth but over time can get depleted as we experience situations that use up and modify them. We can probably all think of examples and areas where we need to replenish our levels. Richo suggests that during our lives these components make up our internal love which we have for ourselves and others.  

\textsc{\lc{ATTENTION}} refers to our capacity to be aware of others and ourselves, whilst being the focus of someone’s loving attention. This aspect is fundamental to our impact as it is the same as being present in seeking real Impact in the dialectic for governance.  

So in our interactions, how much of ourselves do we bring into the room? And our capacity to give attention to ourselves, which is a function of the level of self-identity we have acquired.   

\textsc{\lc{ACCEPTANCE}} means being seen with mercy, love, respect and understanding by those whom we believe are important to us. In order to create intimacy and real relationships, we have to feel safe, accepted, relaxed and worthy. In our context, this aspect is fundamental to our capacity to understand perspectives and to remain open to new learning. Creating a non-judgemental space is important to generate creativity and clear decision making.  

\textsc{\lc{APPRECIATION}} for self and others is essential to our feeling loved and accepted and to maintaining good relations. This includes contentment, gratitude and being humble enough to see the gifts that come our way. Benjamin Franklin stated succinctly: ‘Content makes poor men rich; discontent makes rich men poor\cite{quote2_forbes_nodate}.’

Situations of relative and unearned wealth may need work in order for us to value our contribution and see the results that Impact adds to capital. Here our ego is the trickster, as we often judge others as lesser or greater than ourselves; in each case, this will dramatically narrow our capacity to appreciate others and their contribution. C.G. Jung says it clearly: ‘Thinking is difficult, that’s why most people judge\cite{quote4_carl_nodate}.’ This resonates with Tal Ben-Shahar’s wisdom, 

\begin{quote}
The word appreciate has two meanings. The first is to be thankful—the opposite of taking something for granted—and the second is to increase in value, the way we say that assets appreciate when their value rises. When it comes to the role that appreciation plays in our life, both these meanings are relevant. Psychological research has repeatedly shown that when we are thankful for the good in our life, the good grows and we have more of it. The opposite, sadly, is also true: When we fail to appreciate the good, when we take it for granted, the good depreciates.	\cite[p. 144]{ben-shahar_choose_2014}
\end{quote} 

\textsc{\lc{AFFECTION}} is derived from the word ‘affect’, meaning impact or change in physicality, emotions and feelings. As humans, we need emotional, spiritual and physical affection; infants who do not receive these may die as a result. Affection includes the three keys of attention, acceptance and appreciation, but it also requires direct behaviours that demonstrate proof. This aspect can be quite confusing, particularly when we are confronted by relational challenges.  

\textsc{\lc{ALLOWING}} means letting ourselves and others be fully who we are meant to be. As directed by our upbringing, norms and culture provide a framework for our rules, requirements and expectations which direct our behaviours. Allowing also determines our capacity to let in and receive the four previous As. To what extent do we, and others, deserve the good things that come our way? This aspect also contains the understanding and need for control, and it is through such filters that we shape our environment and relationships. If we don’t allow much to enter for ourselves, it is unlikely that we will feel allowing towards others. In our governance structures this is an important aspect for allowing others to be heard, which we will address later. However, collective culture again trumps individual needs and requirements, with the need for conformity being a main driver.  

As Herbert Marcuse mentions, ‘The way in which a society organises the life of its members involves an initial choice between historical alternatives which are determined by the inherited level of the material and intellectual culture’ \cite{marcuse_one-dimensional_1991}.  That is: I am aware that my personal history colours and fragments the way the industry has developed, and my role will be to understand and map the historical choices and pathways of development.  

Claire Graves, who developed Spiral Dynamics, saw clearly that our development is neither assured nor self-evident. This step often requires individuals to venture from conformity to find solutions that actually work, and then attempt to re-admit themselves into some form of influence.  Said Dawlabani in his book \textsc{\lc{MEME}}nomics, much based on Graves work, 

\begin{quote}
Our problems today can no longer be solved from a first-tier subsistence toolbox that has been exhausted and corrupted. That system is in decay and the final stages of entropy are at hand. Will humanity decide to take the momentous leap forward and begin to design from a systemic perspective that is informed by the lessons learned from the subsistence value systems and past human behaviour or would we continue to squander our human potential by providing Band-Aid solutions and hope for the best? If we choose the former, we will evolve to the ‘being’ level of existence on our upward journey of human emergence. If we choose the latter, we will condemn ourselves to becoming a footnote in the universe’s cosmic reality’ (p. 172).\cite{dawlabani_memenomics:_2013}
\end{quote}

Having now looked at the background, and some of the needs for taking a more Integral Impact approach, we will now delve deeper into how we can use questions to gain further insight for our Impact approach.